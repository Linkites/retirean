/**
 * PhotoSorterView.java
 * 
 * (c) Luke Hutchison (luke.hutch@mit.edu)
 * 
 * TODO: Add OpenGL acceleration.
 * 
 * --
 * 
 * Released under the MIT license (but please notify me if you use this code, so that I can give your project credit at
 * http://code.google.com/p/android-multitouch-controller ).
 * 
 * MIT license: http://www.opensource.org/licenses/MIT
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package com.linkites.retire.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.linkites.retire.utility.MultiTouchController.MultiTouchObjectCanvas;
import com.linkites.retire.utility.MultiTouchController.PointInfo;
import com.linkites.retire.utility.MultiTouchController.PositionAndScale;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.BitmapFactory.Options;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class PhotoSortrView extends View implements
		MultiTouchObjectCanvas<PhotoSortrView.Img>  {

	private float minX, minY, maxX, maxY;
	private static String[] IMAGES_FILENAME;
	private static Bitmap[] IMAGES_BITMAP;
	private static Bitmap CLIP_BITMAP;
	private static Canvas canvas;
	private static Img framedImage;
	//private static GestureDetector gestureDetector;
	private static Context mContext;
	private static final int REQ_FRAME_IMAGE = 0x04;
	private static boolean framedFlag = false;
	private static boolean clipFlag = false;
	private static boolean textFlag = false;
	private static boolean firstDownload;
	private boolean isFirstLoadWithFrame = false;
	private static int framePhotoIndex = -1;
	private MotionEvent event;
	private boolean isPSVFocus = false;
	private boolean isPaintView = false;
	private boolean isArtView = false;
	private boolean isPhotoSelected = false;
	private float old_centerX, old_centerY, old_scaleX, old_scaleY, old_angle;
	private static ArrayList<Drawable> framedDrawables = new ArrayList<Drawable>();
	private static ArrayList<Drawable> clipDrawables = new ArrayList<Drawable>();
	private static ArrayList<Drawable> textDrawables = new ArrayList<Drawable>();
	// private static final int[] IMAGES = { R.drawable.m74hubble,
	// R.drawable.catarina, R.drawable.tahiti, R.drawable.sunset,
	// R.drawable.lake };
	// private static final int[] IMAGES = { 0, 1, 2, 3, 4 };
	private static ArrayList<Img> mImages = new ArrayList<Img>();

	public int getImagesLength() {
		int i = mImages.size();
		return i;
	}

	// --

	public MultiTouchController<Img> multiTouchController = new MultiTouchController<Img>(
			this);

	// --

	private PointInfo currTouchPoint = new PointInfo();

	private boolean mShowDebugInfo = true;

	private static final int UI_MODE_ROTATE = 1, UI_MODE_ANISOTROPIC_SCALE = 2;

	private int mUIMode = UI_MODE_ROTATE;

	// --

	private Paint mLinePaintTouchPointCircle = new Paint();

	// ---------------------------------------------------------------------------------------------------

	public PhotoSortrView(Context context) {
		this(context, null);
	}

	public PhotoSortrView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PhotoSortrView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
		//gestureDetector = new GestureDetector(context, new GestureListener());
		mContext = context;
	}

	private void init(Context context) {
		// getBitmaps(context);
		mLinePaintTouchPointCircle.setColor(Color.YELLOW);
		mLinePaintTouchPointCircle.setStrokeWidth(5);
		mLinePaintTouchPointCircle.setStyle(Style.STROKE);
		mLinePaintTouchPointCircle.setAntiAlias(true);
		setBackgroundColor(Color.BLACK);
	}

	public void getBitmaps(Context context) {
		// TODO Auto-generated method stub
//		Resources res = context.getResources();
//		File sdcards = new File("/sdcard/PhotoCollage/Camera");
//		IMAGES_FILENAME = sdcards.list();
//		IMAGES_BITMAP = new Bitmap[IMAGES_FILENAME.length];
//		for (int i = 0; i < IMAGES_FILENAME.length; i++) {
//			IMAGES_BITMAP[i] = ShrinkBitmap("/sdcard/PhotoCollage/Camera/"
//					+ IMAGES_FILENAME[i], 500, 500);
//			mImages.add(new Img(IMAGES_BITMAP[i], res));
//			int n = mImages.size();
//			mImages.get(n - 1).load(res);
//			mImages.get(n - 1).draw(new Canvas());
//		}
//		IMAGES_BITMAP = null;
//		System.gc();
	}

	public void selectedImages(Context context, ArrayList<String> sImages) {
		// TODO Auto-generated method stub
		
//		Resources res = context.getResources();
//		IMAGES_BITMAP = new Bitmap[sImages.size()];
//		for (int i = 0; i < sImages.size(); i++) {
//
//			IMAGES_BITMAP[i] = ShrinkBitmap(sImages.get(i), 500, 500);
//			mImages.add(new Img(IMAGES_BITMAP[i], res));
//			// loadImages(context);
//			int n = mImages.size();
//			mImages.get(n - 1).load(res);
//			// for (int j = 0; j < n; j++){
//			mImages.get(n - 1).draw(new Canvas());
//			// }
//
//		}
//		IMAGES_BITMAP = null;
//		System.gc();
//		invalidate();
	}

	public void addFrameImage(Context context) {
		// mImages.remove(framedImage);
//		old_centerX = framedImage.getCenterX();
//		old_centerY = framedImage.getCenterY();
//		old_scaleX = framedImage.getScaleX();
//		old_scaleY = framedImage.getScaleY();
//		old_angle = framedImage.getAngle();
//
//		isFirstLoadWithFrame = true;
//		int index = mImages.indexOf(framedImage);
//		Resources res = context.getResources();
//		File sdcards = new File("/sdcard/PhotoCollage/EditImages/Applied");
//		IMAGES_FILENAME = sdcards.list();
//		IMAGES_BITMAP = new Bitmap[IMAGES_FILENAME.length];
//		Img img = null;
//		for (int i = 0; i < IMAGES_FILENAME.length; i++) {
//			IMAGES_BITMAP[0] = ShrinkBitmap(
//					"/sdcard/PhotoCollage/EditImages/Applied/"
//							+ IMAGES_FILENAME[0], 500, 500);
//			img = new Img(IMAGES_BITMAP[0], res);
//			
//			mImages.set(index, img);
//			// loadImages(context);
//			int n = mImages.size();
//
//			mImages.get(n - 1).load(res);
//			// for (int j = 0; j < n; j++)
//			mImages.get(n - 1).draw(new Canvas());
//
//		}
//		if (framedFlag) {
//			framedDrawables.set(framePhotoIndex, img.getDrawable());
//		} else {
//			framedDrawables.add(img.getDrawable());
//		}
//		isFirstLoadWithFrame = false;
//		
//		IMAGES_BITMAP = null;
//		System.gc();
//		invalidate();
	}

	public void addImage(Context context) {
		Resources res = context.getResources();
		File sdcards = new File("/sdcard/PhotoCollage/Text");
		IMAGES_FILENAME = sdcards.list();
		IMAGES_BITMAP = new Bitmap[IMAGES_FILENAME.length];
		Img img = null;
		for (int i = 0; i < IMAGES_FILENAME.length; i++) {
			IMAGES_BITMAP[i] = ShrinkBitmap("/sdcard/PhotoCollage/Text/"
					+ IMAGES_FILENAME[i], 500, 500);
			img = new Img(IMAGES_BITMAP[i], res);
			mImages.add(img);
			int n = mImages.size();
			mImages.get(n - 1).load(res);
			mImages.get(n - 1).draw(new Canvas());

		}
		invalidate();
		textDrawables.add(img.getDrawable());
		IMAGES_BITMAP = null;
		System.gc();

	}

	public void addClipArt(Context context, int resID) {
		//String newFilePath = filePath.replace("thumbs", "hd");
		
		Resources res = context.getResources();
		Img img = null;
		CLIP_BITMAP= ShrinkBitmapClip(resID, 150, 150);
		img = new Img(CLIP_BITMAP, res);
		mImages.add(img);
		int n = mImages.size();
		mImages.get(n - 1).load(res);
		mImages.get(n - 1).draw(new Canvas());
		invalidate();
		clipDrawables.add(img.getDrawable());
		CLIP_BITMAP= null;
		System.gc();
	}


	private Bitmap ShrinkBitmapClip(int res, int width, int height) {
		// Log.e("ShrinkBitmap....", "called....");
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), res, bmpFactoryOptions);
		// Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
		// bmpFactoryOptions);
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);

		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}

		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeResource(getResources(), res, bmpFactoryOptions);
		// bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
		// bmpFactoryOptions);
		return bitmap;
	}
	
	
	private Bitmap ShrinkBitmap(String string, int width, int height) {
		// Log.e("ShrinkBitmap....", "called....");
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
		// Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
		// bmpFactoryOptions);
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);

		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}

		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(string, bmpFactoryOptions);
		// bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
		// bmpFactoryOptions);
		return bitmap;
	}

	/** Called by activity's onResume() method to load the images */
	public void loadImages(Context context) {
		Resources res = context.getResources();
		int n = mImages.size();
		for (int i = 0; i < n; i++)
			mImages.get(i).load(res);
	}

	/**
	 * Called by activity's onPause() method to free memory used for loading the
	 * images
	 */
	public void unloadImages() {
		int n = mImages.size();
		for (int i = 0; i < n; i++)
			mImages.get(i).unload();
		
		mImages.removeAll(mImages);
		
	}

	// ---------------------------------------------------------------------------------------------------

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		this.canvas = canvas;
		if (isPaintView || isArtView) {
			
			canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
	        canvas.drawPath(mPath, mPaint);
	        
		}
		int n = mImages.size();
		for (int i = 0; i < n; i++)
			mImages.get(i).draw(canvas);
		if (mShowDebugInfo)
			drawMultitouchDebugMarks(canvas);
	}

	// ---------------------------------------------------------------------------------------------------

	public void trackballClicked() {
		mUIMode = (mUIMode + 1) % 3;
		invalidate();
	}

	private void drawMultitouchDebugMarks(Canvas canvas) {
		if (currTouchPoint.isDown()) {
			float[] xs = currTouchPoint.getXs();
			float[] ys = currTouchPoint.getYs();
			float[] pressures = currTouchPoint.getPressures();
			int numPoints = Math.min(currTouchPoint.getNumTouchPoints(), 2);
			if (isPhotoSelected) {
				Paint paint = new Paint();
				paint.setColor(Color.WHITE);
				paint.setStyle(Style.STROKE);
				paint.setStrokeWidth(2);
				paint.setAntiAlias(true);
				Path path = new Path();
				path.addRect(new RectF(minX, minY, maxX, maxY), Path.Direction.CCW);
				canvas.drawPath(path, paint);
			}
			/*for (int i = 0; i < numPoints; i++)
			canvas.drawCircle(xs[i], ys[i], 50 + pressures[i] * 80,
			mLinePaintTouchPointCircle);
			if (numPoints == 2)
			canvas.drawLine(xs[0], ys[0], xs[1], ys[1],
			mLinePaintTouchPointCircle);*/
		}
	}
	
	private float mX, mY;
	private float prvX, prvY;
	private ArrayList<Float> listX = new ArrayList<Float>();
	private ArrayList<Float> listY = new ArrayList<Float>();
	private float paintedMinX, paintedMinY, paintedMaxX, paintedMaxY;
    private static final float TOUCH_TOLERANCE = 4;
    
    private Bitmap  mBitmap;
    private Canvas  mCanvas;
    private Path    mPath;
    private Paint   mBitmapPaint;
    private Paint   mPaint;
    private MaskFilter  mEmboss;
    private MaskFilter  mBlur;
    private Bitmap mBMP;
    private int strockWidth;
    
    public void initPaintView(){
    	
    	mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        
        mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 },
                0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
        
        int x = (mContext.getResources().getDisplayMetrics().widthPixels) - 50;
		int y = (mContext.getResources().getDisplayMetrics().heightPixels) - 100;
        mBitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
        
        mCanvas = new Canvas(mBitmap);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        //isPaintView = true;
    }
    
    public void initArtView(String filePath){
    	
    	mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        
        mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 },
                0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
        
        int x = (mContext.getResources().getDisplayMetrics().widthPixels) - 50;
		int y = (mContext.getResources().getDisplayMetrics().heightPixels) - 100;
        mBitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
       	mBMP = BitmapFactory.decodeFile(new File(filePath).toString());
        mCanvas = new Canvas(mBitmap);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        //isArtView = true;
    }
    
    public int getColor(){
    	return mPaint.getColor();
    }
    
    public void setArtView(boolean flag){
    	isArtView = flag;
    }
    
    public void setPaintView(boolean flag){
    	isPaintView = flag;
    }
    
    public float getStrockWidth(){
    	return mPaint.getStrokeWidth();
    }
    
    public void setStrockWidth(float width){
    	strockWidth = (int) width;
    	mPaint.setStrokeWidth(width);
    }
    
    public MaskFilter getMaskFilter(){
		return mPaint.getMaskFilter();
    }
    
    public void setMaskFilter(MaskFilter mFilter){
    	mPaint.setMaskFilter(mFilter);
    }
    
    public void setBrushColor(int color){
    	mPaint.setColor(color);
    }
    
    private void touch_start(float x, float y) {
        //mPath.reset();
    	if (!isArtView) {
    		mPath.moveTo(x, y);
		}
        
        mX = x;
        mY = y;
        prvX = x;
        prvY = y;
        listX.add(mX);
        listY.add(mY);
        if (isArtView) {
        	mCanvas.drawBitmap(mBMP, x - (mBMP.getWidth()/2), y - (mBMP.getHeight()/2), mPaint);
		}
        
    }
    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
        	
            if (!isArtView) {
            	mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
			}
        	
            mX = x;
            mY = y;
        }
        listX.add(x);
        listY.add(y);
        
        // Image Draw.....
        if (isArtView) {
        	int BMP_SIZE = mBMP.getWidth();
        	if ((int)(x - prvX) > BMP_SIZE || (int)(y - prvY) > BMP_SIZE || (int)(x - prvX) < -BMP_SIZE || (int)(y - prvY) < -BMP_SIZE) {
            	mCanvas.drawBitmap(mBMP, x - (BMP_SIZE/2), y - (BMP_SIZE/2), mPaint);
            	prvX = x;
            	prvY = y;
    		}
		}
        
        
    }
    private void touch_up() {
        if (!isArtView) {
        	mPath.lineTo(mX, mY);
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
		}
    	//mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        //mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
        //mPath.reset();
        
        createMinMax();
    }
    
    
    private void createMinMax(){
    	paintedMinX = Collections.min(listX);
    	paintedMinY = Collections.min(listY);
    	
    	paintedMaxX = Collections.max(listX);
    	paintedMaxY = Collections.max(listY);
    	
    	
    }
    
    

	// ---------------------------------------------------------------------------------------------------

	/** Pass touch events to the MT controller */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		this.event = event;
		if (isPSVFocus) {
			return multiTouchController.onTouchEvent(event);
		}else if (isPaintView || isArtView){
		//gestureDetector.onTouchEvent(event);
			float x = event.getX();
	        float y = event.getY();
	        
	        switch (event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	                touch_start(x, y);
	                invalidate();
	                break;
	            case MotionEvent.ACTION_MOVE:
	                touch_move(x, y);
	                invalidate();
	                break;
	            case MotionEvent.ACTION_UP:
	                touch_up();
	                invalidate();
	                copyPaintedImage();
	                break;
	        }
			return true;
		}else{
			return false;
		}
		
        
	}
	
	public void setPSVFocus(boolean flag){
		
		isPSVFocus = flag;
		/*if (isPSVFocus) {
			isPSVFocus = false;
			isPaintView = false;
			isArtView = false;
			listX.removeAll(listX);
			listY.removeAll(listY);
		}else{
			isPSVFocus = true;
			isPaintView = false;
		}*/
		//setClickable(false);
	}
	
	private void copyPaintedImage(){
		old_scaleX = 1;
		old_scaleY = 1;
		old_angle = 0;
		
		if (isArtView) {
			strockWidth = mBMP.getWidth();
		}

		isFirstLoadWithFrame = true;
		int posX = (int) (paintedMinX - strockWidth);
		int posY = (int) (paintedMinY - strockWidth);
		int width = (int) (paintedMaxX - paintedMinX) + (strockWidth * 2);
		int height = (int)(paintedMaxY - paintedMinY) + (strockWidth * 2);
		
		if (posX < 0) {
			posX = 0;
		}
		if ((width + posX) > mBitmap.getWidth()) {
			width = width - ((width + posX) - mBitmap.getWidth());
		}
		if (posY < 0) {
			posY = 0;
		}
		if ((height + posY) > mBitmap.getHeight()) {
			height = height - ((height + posY) - mBitmap.getHeight());
		}
		Bitmap cropBMP = Bitmap.createBitmap(mBitmap, posX, posY,  
				width, height, null, true);

		
		Img img = new Img(cropBMP, mContext.getResources());
		old_centerX = (paintedMinX - strockWidth) + (cropBMP.getWidth()/2);
		old_centerY = (paintedMinY - strockWidth) + (cropBMP.getHeight()/2);
		
		//old_centerX = mContext.getResources().getDisplayMetrics().widthPixels / 2;
		//old_centerY = mContext.getResources().getDisplayMetrics().heightPixels / 2;
		
		
		mImages.add(img);
		int n = mImages.size();
		mImages.get(n - 1).load(mContext.getResources());
		mImages.get(n - 1).draw(new Canvas());
		invalidate();
		clearPaint();
		isFirstLoadWithFrame = false;
	}

	public void clearPaint() {
		mBitmap = null;
		mPath = null;
		mCanvas = null;
		int x = (mContext.getResources().getDisplayMetrics().widthPixels) - 50;
		int y = (mContext.getResources().getDisplayMetrics().heightPixels) - 100;
		mBitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
		mCanvas = new Canvas(mBitmap);
		mPath = new Path();
		//isPaintView = true;
		listX.removeAll(listX);
		listY.removeAll(listY);
	}

	private void deleteFrames() {
		File sdcard = Environment.getExternalStorageDirectory();
		File pictureDir = new File(sdcard, "/PhotoCollage/EditImages/Orignal");
		if (pictureDir.isDirectory()) {
			String[] child = pictureDir.list();
			for (String str : child) {
				new File(pictureDir, str).delete();
			}
		}
	}

	private void deleteFrames(int index) {
		File sdcard = Environment.getExternalStorageDirectory();
		File pictureDir = new File(sdcard, "/PhotoCollage/EditImages/Orignal");
		new File(pictureDir, "orignal" + index + ".jpg").delete();
		if (pictureDir.isDirectory()) {
			String[] child = pictureDir.list();
			if (child.length == 0) {
				framePhotoIndex = -1;
				framedFlag = false;
			}
			for (int i = 0; i <= child.length; i++) {
				if (i > index) {
					File from = new File(pictureDir, "orignal" + i + ".jpg");
					int j = i - 1;
					File to = new File(pictureDir, "orignal" + j + ".jpg");
					from.renameTo(to);
				}
			}
		}
	}

	public void removeAll() {
		if (mImages != null) {
			unloadImages();
			mImages.removeAll(mImages);
			deleteFrames();
			framedDrawables.removeAll(framedDrawables);
			clipDrawables.removeAll(clipDrawables);
			textDrawables.removeAll(textDrawables);
			framePhotoIndex = -1;
			framedFlag = false;
			clipFlag = false;
			textFlag = false;
			invalidate();
			bitmapdata = null;
			IMAGES_BITMAP = null;
			IMAGES_FILENAME = null;
			System.gc();
		}
	}

	/*private class GestureListener extends
			GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		// event when double tap occurs
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			float x = e.getX();
			float y = e.getY();

			// Log.d("Double Tap", "Tapped at: (" + x + "," + y + ")");
			String[] configValues = { "Edit image", "Remove current image",
					"Create New Collage" };
			AlertDialog.Builder configBuilder = new AlertDialog.Builder(
					mContext);
			configBuilder.setTitle("Edit:");
			configBuilder.setNegativeButton("Cancel", null);
			configBuilder.setItems(configValues,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							switch (which) {
							case 2:
								AlertDialog.Builder builder = new AlertDialog.Builder(
										mContext);
								builder.setMessage(
										"Are you sure you want to create new collage?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														if (mImages != null) {
															unloadImages();
															mImages.removeAll(mImages);
															PhotoSortrActivity psa = new PhotoSortrActivity();
															psa.backgroundPosition = 0;
															psa.galleryView.setSelection(0);
															setBackgroundColor(Color.BLACK);
															deleteFrames();
															framedDrawables
																	.removeAll(framedDrawables);
															clipDrawables
																	.removeAll(clipDrawables);
															textDrawables
																	.removeAll(textDrawables);
															framePhotoIndex = -1;
															framedFlag = false;
															clipFlag = false;
															textFlag = false;
															invalidate();
															bitmapdata = null;
															IMAGES_BITMAP = null;
															IMAGES_FILENAME = null;
															System.gc();
														}
														Toast.makeText(
																mContext,
																"All images are removed successfully.",
																Toast.LENGTH_SHORT)
																.show();

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														dialog.cancel();
													}
												});
								AlertDialog alert = builder.create();
								alert.show();
								break;
							case 1:
								AlertDialog.Builder builder1 = new AlertDialog.Builder(
										mContext);
								builder1.setMessage(
										"Are you sure you want to remove current image?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														if (bitmapdata != null) {
															mImages.remove(framedImage);
															if (framedFlag) {
																framedDrawables
																		.remove(framePhotoIndex);
																deleteFrames(framePhotoIndex);
															}
															invalidate();
															bitmapdata = null;
															System.gc();
														} else {
															Toast.makeText(
																	mContext,
																	"Please select a image to Remove.",
																	Toast.LENGTH_SHORT)
																	.show();
														}
													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														dialog.cancel();
													}
												});
								AlertDialog alert1 = builder1.create();
								alert1.show();

								break;
							case 0:
								final File sdcards = new File("/sdcard/PhotoCollage/Frames/hd");
								if (sdcards.exists()) {
									IMAGES_FILENAME = sdcards.list();
									if (IMAGES_FILENAME.length != 0) {
										CopyAssetsToSdcard cats = new CopyAssetsToSdcard(
												mContext);
										cats.CopyAssets("noeffect");
										cats.CopyAssets("noframe");
										if (clipFlag) {
											Toast.makeText(
													mContext,
													"You cannot edit on ClipArts...",
													Toast.LENGTH_SHORT).show();
										} else if (textFlag) {
											Toast.makeText(mContext,
													"You cannot edit on Text...",
													Toast.LENGTH_SHORT).show();
										} else {
											byte[] data = getBitmapData();
											if (data != null) {
												Intent frameingIntent = new Intent(
														mContext,
														ImageFraming.class);
												frameingIntent.putExtra(
														"selectedImage", data);
												frameingIntent.putExtra("isFramed",
														framedFlag);
												frameingIntent.putExtra("index",
														framePhotoIndex);
												((Activity) mContext)
														.startActivityForResult(
																frameingIntent,
																REQ_FRAME_IMAGE);
											} else {
												Toast.makeText(
														mContext,
														"Please select a image for framing.",
														Toast.LENGTH_SHORT).show();
											}
											bitmapdata = null;
										}

									}else{
										AlertDialog.Builder builder11 = new AlertDialog.Builder(mContext);
										builder11.setTitle("Download:");
										builder11.setMessage("Collage has to download a set of frames from an internet server. " +
												"\n \nA netwrok connection is recommended for this, as the files are around 20 megabytes in size. " +
												"\n \nWould you like to proceed now?")
										       .setCancelable(false)
										       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
										           public void onClick(DialogInterface dialog, int id) {
										        	   new Download(mContext, 2);
										           }
										       })
										       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
										           public void onClick(DialogInterface dialog, int id) {
										                dialog.cancel();
										           }
										       });
										AlertDialog alert11 = builder11.create();
										alert11.show();
										firstDownload = true;
									}
									
								} else {
									AlertDialog.Builder builder11 = new AlertDialog.Builder(mContext);
									builder11.setTitle("Download:");
									builder11.setMessage("Collage has to download a set of frames from an internet server. " +
											"\n \nA netwrok connection is recommended for this, as the files are around 20 megabytes in size. " +
											"\n \nWould you like to proceed now?")
									       .setCancelable(false)
									       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									           public void onClick(DialogInterface dialog, int id) {
									        	   new Download(mContext, 2);
									           }
									       })
									       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									           public void onClick(DialogInterface dialog, int id) {
									                dialog.cancel();
									           }
									       });
									AlertDialog alert11 = builder11.create();
									alert11.show();
									firstDownload = true;
								}

								break;

							default:
								break;
							}
						}
					});
			String[] textValues = { "Remove current image", "Create New Collage" };
			AlertDialog.Builder textBuilder = new AlertDialog.Builder(mContext);
			textBuilder.setTitle("Edit:");
			textBuilder.setNegativeButton("Cancel", null);
			textBuilder.setItems(textValues,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								AlertDialog.Builder builder1 = new AlertDialog.Builder(
										mContext);
								builder1.setMessage(
										"Are you sure you want to remove current image?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														if (bitmapdata != null) {
															mImages.remove(framedImage);
															if (framedFlag) {
																framedDrawables
																		.remove(framePhotoIndex);
																deleteFrames(framePhotoIndex);
															}
															invalidate();
															bitmapdata = null;
															System.gc();
														} else {
															Toast.makeText(
																	mContext,
																	"Please select a image to Remove.",
																	Toast.LENGTH_SHORT)
																	.show();
														}
													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														dialog.cancel();
													}
												});
								AlertDialog alert1 = builder1.create();
								alert1.show();
								break;
							case 1:
								AlertDialog.Builder builder = new AlertDialog.Builder(
										mContext);
								builder.setMessage(
										"Are you sure you want to create new collage?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														if (mImages != null) {
															unloadImages();
															mImages.removeAll(mImages);
															PhotoSortrActivity psa = new PhotoSortrActivity();
															psa.backgroundPosition = 0;
															psa.galleryView.setSelection(0);
															setBackgroundColor(Color.BLACK);
															deleteFrames();
															framedDrawables
																	.removeAll(framedDrawables);
															clipDrawables
																	.removeAll(clipDrawables);
															textDrawables
																	.removeAll(textDrawables);
															framePhotoIndex = -1;
															framedFlag = false;
															clipFlag = false;
															textFlag = false;
															invalidate();
															bitmapdata = null;
															IMAGES_BITMAP = null;
															IMAGES_FILENAME = null;
															System.gc();
														}
														Toast.makeText(
																mContext,
																"All images are removed successfully.",
																Toast.LENGTH_SHORT)
																.show();

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														dialog.cancel();
													}
												});
								AlertDialog alert = builder.create();
								alert.show();

								break;
							default:
								break;
							}
						}
					});

			AlertDialog myDialog = configBuilder.create();
			AlertDialog textDialog = textBuilder.create();
			if (clipFlag || textFlag) {
				textDialog.show();
			} else {
				myDialog.show();
			}

			return true;
		}
	}*/

	/**
	 * Get the image that is under the single-touch point, or return null
	 * (canceling the drag op) if none
	 */
	public Img getDraggableObjectAtPoint(PointInfo pt) {
		float x = pt.getX(), y = pt.getY();
		int n = mImages.size();
		for (int i = n - 1; i >= 0; i--) {
			Img im = mImages.get(i);
			if (im.containsPoint(x, y)) {

				return im;
			}
		}
		return null;
	}

	/**
	 * Select an object for dragging. Called whenever an object is found to be
	 * under the point (non-null is returned by getDraggableObjectAtPoint()) and
	 * a drag operation is starting. Called with null when drag op ends.
	 */
	public void selectObject(Img img, PointInfo touchPoint) {
		currTouchPoint.set(touchPoint);

		if (img != null) {
			// Move image to the top of the stack when selected
			framedImage = img;
			mImages.remove(img);
			mImages.add(img);
			
			Drawable d = img.getDrawable();
			for (int i = 0; i < framedDrawables.size(); i++) {
				if (framedDrawables.get(i).equals(d)) {
					// Toast.makeText(mContext, "true " +
					// framedDrawables.indexOf(d), Toast.LENGTH_SHORT).show();
					framePhotoIndex = framedDrawables.indexOf(d);
					framedFlag = true;
					break;
				} else {
					framePhotoIndex = -1;
					framedFlag = false;
				}

			}
			for (int i = 0; i < clipDrawables.size(); i++) {
				if (clipDrawables.get(i).equals(d)) {
					clipFlag = true;
					break;
				} else {
					clipFlag = false;
				}
			}
			for (int i = 0; i < textDrawables.size(); i++) {
				if (textDrawables.get(i).equals(d)) {
					textFlag = true;
					break;
				} else {
					textFlag = false;
				}
			}
			Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			
			byte[] bitmapdata = stream.toByteArray();
			
			setBitmapData(bitmapdata);// /
			isPhotoSelected = true;
		} else {
			//isPhotoSelected = false;
			// Called with img == null when drag stops.
		}
		//invalidate();
	}
	
	public void removeCurrentImage(){
		mImages.remove(framedImage);
		invalidate();
		bitmapdata = null;
		System.gc();
	}
	
	
	public byte[] bitmapdata;

	public void setBitmapData(byte[] bitmapdata) {
		// TODO Auto-generated method stub
		this.bitmapdata = bitmapdata;
	}

	public byte[] getBitmapData() {
		return this.bitmapdata;
	}

	/**
	 * Get the current position and scale of the selected image. Called whenever
	 * a drag starts or is reset.
	 */
	public void getPositionAndScale(Img img, PositionAndScale objPosAndScaleOut) {
		// FIXME affine-izem (and fix the fact that the anisotropic_scale part
		// requires averaging the two scale factors)
		objPosAndScaleOut.set(img.getCenterX(), img.getCenterY(),
				(mUIMode & UI_MODE_ANISOTROPIC_SCALE) == 0,
				(img.getScaleX() + img.getScaleY()) / 2,
				(mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0, img.getScaleX(),
				img.getScaleY(), (mUIMode & UI_MODE_ROTATE) != 0,
				img.getAngle());
		
	}

	/** Set the position and scale of the dragged/stretched image. */
	public boolean setPositionAndScale(Img img,
			PositionAndScale newImgPosAndScale, PointInfo touchPoint) {
		currTouchPoint.set(touchPoint);
		boolean ok = img.setPos(newImgPosAndScale);
		if (ok)
			invalidate();
		
		minX = img.getMinX();
		minY = img.getMinY();
		maxX = img.getMaxX();
		maxY = img.getMaxY();
		return ok;
	}

	// ----------------------------------------------------------------------------------------------

	class Img {
		private Bitmap resId;

		private Drawable drawable;

		private boolean firstLoad;

		private int width, height, displayWidth, displayHeight;

		private float centerX, centerY, scaleX, scaleY, angle;

		private float minX, maxX, minY, maxY;

		private static final float SCREEN_MARGIN = 5;

		public Img(Bitmap resId, Resources res) {
			this.resId = resId;
			this.firstLoad = true;
			getMetrics(res);
			setFocusable(false);
		}

		private void getMetrics(Resources res) {
			DisplayMetrics metrics = res.getDisplayMetrics();
			// The DisplayMetrics don't seem to always be updated on screen
			// rotate, so we hard code a portrait
			// screen orientation for the non-rotated screen here...
			// this.displayWidth = metrics.widthPixels;
			// this.displayHeight = metrics.heightPixels;
			//this.displayWidth = 270;
			//this.displayHeight = 380;
			this.displayWidth = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Math
					.max(metrics.widthPixels, metrics.heightPixels) : Math.min(
					metrics.widthPixels, metrics.heightPixels);
			this.displayHeight = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Math
					.min(metrics.widthPixels, metrics.heightPixels) : Math.max(
					metrics.widthPixels, metrics.heightPixels);
			this.displayWidth = this.displayWidth - 50;
			this.displayHeight = this.displayHeight - 100;
		}

		/** Called by activity's onResume() method to load the images */
		public void load(Resources res) {
			getMetrics(res);
			// this.drawable = res.getDrawable(resId);
			// Log.e("ImageLoad....", "called....");
			this.drawable = new BitmapDrawable(res, resId);

			this.width = drawable.getIntrinsicWidth();
			this.height = drawable.getIntrinsicHeight();
			float cx, cy, sx, sy;
			if (firstLoad) {
				if (isFirstLoadWithFrame) {
					cx = old_centerX;
					cy = old_centerY;
					sx = old_scaleX;
					sy = old_scaleY;
					firstLoad = false;
					
					setPos(cx, cy, sx, sy, old_angle);
				} else {
					// Log.d("firstLoad", ">>>>>>>>>>>>>>>>>>>>>>>>" +
					// firstLoad);
					cx = SCREEN_MARGIN
							+ (float) (Math.random() * (displayWidth - 2 * SCREEN_MARGIN));
					cy = SCREEN_MARGIN
							+ (float) (Math.random() * (displayHeight - 2 * SCREEN_MARGIN));
					float sc = (float) (Math.max(displayWidth, displayHeight)
							/ (float) Math.max(width, height) * Math.random()
							* 0.3 + 0.2);
					sx = sy = sc;
					firstLoad = false;
					
					setPos(cx, cy, sx, sy, 0.0f);
				}

			} else {
				// Log.d("firstLoad", ">>>>>>>>>>>>>>>>>>>>>>>>" + firstLoad);
				// Reuse position and scale information if it is available
				// FIXME this doesn't actually work because the whole activity
				// is torn down and re-created on rotate
				cx = this.centerX;
				cy = this.centerY;
				sx = this.scaleX;
				sy = this.scaleY;
				// Make sure the image is not off the screen after a screen
				// rotation
				if (this.maxX < SCREEN_MARGIN)
					cx = SCREEN_MARGIN;
				else if (this.minX > displayWidth - SCREEN_MARGIN)
					cx = displayWidth - SCREEN_MARGIN;
				if (this.maxY > SCREEN_MARGIN)
					cy = SCREEN_MARGIN;
				else if (this.minY > displayHeight - SCREEN_MARGIN)
					cy = displayHeight - SCREEN_MARGIN;
			}

		}

		/**
		 * Called by activity's onPause() method to free memory used for loading
		 * the images
		 */
		public void unload() {
			this.drawable = null;
		}

		/** Set the position and scale of an image in screen coordinates */
		public boolean setPos(PositionAndScale newImgPosAndScale) {
			return setPos(
					newImgPosAndScale.getXOff(),
					newImgPosAndScale.getYOff(),
					(mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0 ? newImgPosAndScale
							.getScaleX() : newImgPosAndScale.getScale(),
					(mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0 ? newImgPosAndScale
							.getScaleY() : newImgPosAndScale.getScale(),
					newImgPosAndScale.getAngle());
			// FIXME: anisotropic scaling jumps when axis-snapping
			// FIXME: affine-ize
			// return setPos(newImgPosAndScale.getXOff(),
			// newImgPosAndScale.getYOff(),
			// newImgPosAndScale.getScaleAnisotropicX(),
			// newImgPosAndScale.getScaleAnisotropicY(), 0.0f);
		}

		/** Set the position and scale of an image in screen coordinates */
		private boolean setPos(float centerX, float centerY, float scaleX,
				float scaleY, float angle) {
			float ws = (width / 2) * scaleX, hs = (height / 2) * scaleY;
			float newMinX = centerX - ws, newMinY = centerY - hs, newMaxX = centerX
					+ ws, newMaxY = centerY + hs;
			if (newMinX > displayWidth - SCREEN_MARGIN
					|| newMaxX < SCREEN_MARGIN
					|| newMinY > displayHeight - SCREEN_MARGIN
					|| newMaxY < SCREEN_MARGIN)
				return false;
			this.centerX = centerX;
			this.centerY = centerY;
			this.scaleX = scaleX;
			this.scaleY = scaleY;
			this.angle = angle;
			this.minX = newMinX;
			this.minY = newMinY;
			this.maxX = newMaxX;
			this.maxY = newMaxY;
			return true;
		}

		/** Return whether or not the given screen coords are inside this image */
		public boolean containsPoint(float scrnX, float scrnY) {
			// FIXME: need to correctly account for image rotation
			return (scrnX >= minX && scrnX <= maxX && scrnY >= minY && scrnY <= maxY);
		}

		public void draw(Canvas canvas) {
			canvas.save();
			float dx = (maxX + minX) / 2;
			float dy = (maxY + minY) / 2;
			drawable.setBounds((int) minX, (int) minY, (int) maxX, (int) maxY);
			canvas.translate(dx, dy);
			canvas.rotate(angle * 180.0f / (float) Math.PI);
			canvas.translate(-dx, -dy);
			/*if (isPhotoSelected) {
				Paint paint = new Paint();
				paint.setColor(Color.WHITE);
				paint.setStyle(Style.STROKE);
				paint.setStrokeWidth(2);
				paint.setAntiAlias(true);
				Path path = new Path();
				path.addRect(new RectF(minX, minY, maxX, maxY), Path.Direction.CCW);
				canvas.drawPath(path, paint);
			}
			*/
			drawable.draw(canvas);
			
			canvas.restore();
		}

		public Drawable getDrawable() {
			return drawable;
		}

		public int getWidth() {
			return width;
		}

		public int getHeight() {
			return height;
		}

		public float getCenterX() {
			return centerX;
		}

		public float getCenterY() {
			return centerY;
		}

		public float getScaleX() {
			return scaleX;
		}

		public float getScaleY() {
			return scaleY;
		}

		public float getAngle() {
			return angle;
		}

		// FIXME: these need to be updated for rotation
		public float getMinX() {
			return minX;
		}

		public float getMaxX() {
			return maxX;
		}

		public float getMinY() {
			return minY;
		}

		public float getMaxY() {
			return maxY;
		}
	}
}
