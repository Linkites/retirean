package com.linkites.retire.utility;

import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class PhotoStackImageView extends ImageView {

	private int mHeight;
    private int mWidth;
    private float mRotate;
    
	public PhotoStackImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		initRotate();
	}
	public PhotoStackImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initRotate();
	}
	public PhotoStackImageView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		initRotate();
	}

	private void initRotate(){
        mRotate = (new Random().nextFloat() - 0.5f) * 30;
    }
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		// TODO Auto-generated method stub
		super.onLayout(changed, left, top, right, bottom);
		 mWidth = bottom - top;
	        mHeight = right - left;
	}
	
	 @SuppressLint("DrawAllocation")
	@Override
	    protected void onDraw(Canvas canvas) {
	        int borderWidth = 2;
	        canvas.save();
	        canvas.rotate(mRotate, mWidth / 2, mHeight / 2);
	        Paint paint = new Paint();
	        paint.setAntiAlias(true);
	        paint.setColor(0xffffffff);
	        canvas.drawRect(getPaddingLeft() - borderWidth, getPaddingTop() - borderWidth, mWidth - (getPaddingRight() - borderWidth), mHeight - (getPaddingBottom() - borderWidth), paint);
	        super.onDraw(canvas);
	        canvas.restore();
	    }
}
