package com.linkites.retire.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewCustom extends TextView {

	public TextViewCustom(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init();
	}
	
	
	public TextViewCustom(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}
	
	public TextViewCustom(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init();
	}

	private void init()
	{
		if(!isInEditMode())
		{
			 Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GeosansLight.ttf");
	            setTypeface(tf);

		}
	}
}
