package com.linkites.retire.util;



import com.Intlfaces.retire.ly.R;

import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
public class ZoomableViewGroup extends LinearLayout implements OnGestureListener{

	private static final int INVALID_POINTER_ID = 1;
    private int mActivePointerId = INVALID_POINTER_ID;
    private RelativeLayout relativeLayout;
    private float mScaleFactor = 1;
    private ScaleGestureDetector mScaleDetector;
    private Matrix mScaleMatrix = new Matrix();
    private Matrix mScaleMatrixInverse = new Matrix();
    private PanGestureRecognizer mPanGestureRecognizer;
    private float mPosX;
    private float mPosY;
    private Matrix mTranslateMatrix = new Matrix();
    private Matrix mTranslateMatrixInverse = new Matrix();
    private float mIntialx, mintialy;
    private float mLastTouchX;
    private float mLastTouchY;
    public ImageButton btnDislike;
    Animation show, hide;
   // private GestureDetector detectore;
    private float mFocusY;
    private boolean isLeftAnimationRunnning,isRightAnimationRunnning;
    private float mFocusX;

    private float[] mInvalidateWorkingArray = new float[6];
    private float[] mDispatchTouchEventWorkingArray = new float[2];
    private float[] mOnTouchEventWorkingArray = new float[2];


    public ZoomableViewGroup(Context context) {
        super(context);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        mTranslateMatrix.setTranslate(0, 0);
        mScaleMatrix.setScale(1, 1);
        //detectore = new GestureDetector(getContext(), this);
        relativeLayout = (RelativeLayout)getChildAt(0);
        Animation hide = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_hide);
		Animation show = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_show);
		
		//LocalBroadcastManager.getInstance(getContext()).registerReceiver(arg0, arg1)
		   
    }
    
    public ZoomableViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        mPanGestureRecognizer = new PanGestureRecognizer(this, panListener);
        relativeLayout = (RelativeLayout)getChildAt(0);
        //detectore = new GestureDetector(getContext(), this);
        Animation hide = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_hide);
		   Animation show = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_show);
		   
    }

    public ZoomableViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
       mPanGestureRecognizer = new PanGestureRecognizer(this, panListener);
       relativeLayout = (RelativeLayout)getChildAt(0);
       //detectore = new GestureDetector(getContext(), this);
	   
       
    }
    
    @Override 
    public boolean hasWindowFocus() {
            return isEnabled() && getVisibility() == VISIBLE ? true : false;
        }
    
    public void swapRigh()
	{
    	
    	 if(show==null)
			   show = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_show);
    	 if(relativeLayout==null)
			   relativeLayout = (RelativeLayout)getChildAt(0);
			   
		   ImageView image1 = (ImageView)relativeLayout.getChildAt(3);
		   image1.startAnimation(show);
		   AnimationSet set = new AnimationSet(true);
		   set.setFillAfter(true);
		 	TranslateAnimation moveLefttoRight = new TranslateAnimation(0, 1000, 0, 0);
		    moveLefttoRight.setStartOffset(100);
		    moveLefttoRight.setDuration(1000);
		    set.addAnimation(moveLefttoRight);
		    set.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					setVisibility(View.GONE);
				
				}
			});
		    startAnimation(set);
		  //
	}
	public void swapLeft()
	{
		
		 if(show==null)
			   show = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_show);
		 
		 if(relativeLayout==null)
			   relativeLayout = (RelativeLayout)getChildAt(0);
		   ImageView image2 = (ImageView)relativeLayout.getChildAt(4);
	
		   image2.startAnimation(show);
		   
		AnimationSet set = new AnimationSet(true);
		 set.setFillAfter(true);
		 TranslateAnimation moveLefttoRight = new TranslateAnimation(0, -1000, 0, 0);
		    moveLefttoRight.setStartOffset(100);
		    moveLefttoRight.setDuration(1000);
		    set.addAnimation(moveLefttoRight);
		    startAnimation(set);
		    set.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					setVisibility(View.GONE);
				
				}
			});
	}
    
    private BroadcastReceiver leftSwip = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			swapRigh();
		}
	};

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                child.layout(l, t, l+child.getMeasuredWidth(), t + child.getMeasuredHeight());
            }
        }
    }

    private PanGestureListener panListener = new PanGestureListener() {
		
		@SuppressWarnings("deprecation")
		@Override
		public void onPan(View v, float deltax, float deltaY) {
			// TODO Auto-generated method stub
			   Log.d("DEBUG", "on panningx "+mPosX +"detlay  "+deltax);
			  if(hide==null)
			   hide = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_hide);
			  if(show==null)
			   show = AnimationUtils.loadAnimation(getContext(),R.anim.alpha_animation_show);
			  
			   if(relativeLayout==null)
				   relativeLayout = (RelativeLayout)getChildAt(0);
				   
			   ImageView image1 = (ImageView)relativeLayout.getChildAt(3);
			   ImageView image2 = (ImageView)relativeLayout.getChildAt(4);
			
			if (mPosX<0) {
				
				if(isRightAnimationRunnning&&!isLeftAnimationRunnning)
				{
					isRightAnimationRunnning = false;
					isLeftAnimationRunnning = true;
					image2.startAnimation(show);
					image1.startAnimation(hide);
				}
				
				if(!isRightAnimationRunnning&&!isLeftAnimationRunnning)
				{
					isRightAnimationRunnning = false;
					isLeftAnimationRunnning = true;
					image2.startAnimation(show);
					image1.startAnimation(hide);
				}
				
				//
				image2.setVisibility(View.VISIBLE);
				image1.setVisibility(View.INVISIBLE);
				//
			}else   if (mPosX>0) {
				if(!isRightAnimationRunnning&&isLeftAnimationRunnning){
					isRightAnimationRunnning = true;
					isLeftAnimationRunnning = false;
					image1.startAnimation(show);
					image2.startAnimation(hide);
				}
				if(!isRightAnimationRunnning&&!isLeftAnimationRunnning)
				{
					isRightAnimationRunnning = true;
					isLeftAnimationRunnning = false;
					image1.startAnimation(show);
					image2.startAnimation(hide);
				}
				image1.setVisibility(View.VISIBLE);
				image2.setVisibility(View.INVISIBLE);
				
			}
		
		}	
		
		
		@Override
		public void onLift(View v, float velocityX, float velocityY) {
			// TODO Auto-generated method stub
			   Log.d("DEBUG", "on lifting");
			   Log.d("DEBUG", "on velocityx "+mPosX +"velocity  "+velocityY);
			
			   if(relativeLayout==null)
				   relativeLayout = (RelativeLayout)getChildAt(0);
			   
			  if (mPosX>-100&&mPosX<200) {
				 mPosX = mIntialx;
				 mPosY = mintialy;
				 ImageView image1 = (ImageView)relativeLayout.getChildAt(3);
				 ImageView image = (ImageView)relativeLayout.getChildAt(4);
				 if(image1!=null && image!=null){
				 image1.setVisibility(View.INVISIBLE);
				 image.setVisibility(View.INVISIBLE);
				 if(hide!=null){
				  image.startAnimation(hide);
				  image1.startAnimation(hide);
				 }
				 
				 invalidate();
				 }
			}else
			{
				 Log.d("DEBUG", "on lifting 2 ");
				
				 setVisibility(View.GONE);
				 if (mPosX<0)
				 {
					 Intent in = new Intent("nope");
					 in.putExtra("tag",(Integer)getTag());
					 LocalBroadcastManager.getInstance(getContext()).sendBroadcast(in);
				 }else if (mPosX>0)
				 {
					 Intent in = new Intent("liked");
						in.putExtra("tag",(Integer)getTag());
						LocalBroadcastManager.getInstance(getContext()).sendBroadcast(in);
					
				 }
			}
			  
		}
	};
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
            }
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.save();
        canvas.translate(mPosX, mPosY);
        canvas.scale(mScaleFactor, mScaleFactor, mFocusX, mFocusY);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        mDispatchTouchEventWorkingArray[0] = ev.getX();
        mDispatchTouchEventWorkingArray[1] = ev.getY();
        mDispatchTouchEventWorkingArray = screenPointsToScaledPoints(mDispatchTouchEventWorkingArray);
        ev.setLocation(mDispatchTouchEventWorkingArray[0],
                mDispatchTouchEventWorkingArray[1]);
        if(btnDislike!=null)
        {
        	int[] pos = new int[2];
        	btnDislike.getLocationOnScreen(pos);
            if(ev.getY() >360) //location button event
                return btnDislike.onTouchEvent(ev);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
    	// TODO Auto-generated method stub
    	return super.onInterceptTouchEvent(ev);
    }
    /**
     * Although the docs say that you shouldn't override this, I decided to do
     * so because it offers me an easy way to change the invalidated area to my
     * likening.
     */
    @Override
    public ViewParent invalidateChildInParent(int[] location, Rect dirty) {

        mInvalidateWorkingArray[0] = dirty.left;
        mInvalidateWorkingArray[1] = dirty.top;
        mInvalidateWorkingArray[2] = dirty.right;
        mInvalidateWorkingArray[3] = dirty.bottom;


        mInvalidateWorkingArray = scaledPointsToScreenPoints(mInvalidateWorkingArray);
        dirty.set(Math.round(mInvalidateWorkingArray[0]), Math.round(mInvalidateWorkingArray[1]),
                Math.round(mInvalidateWorkingArray[2]), Math.round(mInvalidateWorkingArray[3]));

        location[0] *= mScaleFactor;
        location[1] *= mScaleFactor;
        return super.invalidateChildInParent(location, dirty);
    }

    private float[] scaledPointsToScreenPoints(float[] a) {
        mScaleMatrix.mapPoints(a);
        mTranslateMatrix.mapPoints(a);
        return a;
    }

    private float[] screenPointsToScaledPoints(float[] a){
        mTranslateMatrixInverse.mapPoints(a);
        mScaleMatrixInverse.mapPoints(a);
        return a;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mOnTouchEventWorkingArray[0] = ev.getX();
        mOnTouchEventWorkingArray[1] = ev.getY();

        mOnTouchEventWorkingArray = scaledPointsToScreenPoints(mOnTouchEventWorkingArray);
        ev.setLocation(mOnTouchEventWorkingArray[0], mOnTouchEventWorkingArray[1]);
        mScaleDetector.onTouchEvent(ev);
      //  detectore.onTouchEvent(ev);
        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();

                mLastTouchX = x;
                mLastTouchY = y;
                Log.d("Debug", "y  "+y + " and x "+x);
              
                mActivePointerId = ev.getPointerId(0);
                
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                // Find the index of the active pointer and fetch its position
                final int pointerIndex = ev.findPointerIndex(mActivePointerId);
                final float x = ev.getX(pointerIndex);
                final float y = ev.getY(pointerIndex);

                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                mPosX += dx;
                mPosY += dy;
                mTranslateMatrix.preTranslate(dx, dy);
                mTranslateMatrix.invert(mTranslateMatrixInverse);

                mLastTouchX = x;
                mLastTouchY = y;

                invalidate();
                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                // Extract the index of the pointer that left the touch sensor
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = ev.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = ev.getX(newPointerIndex);
                    mLastTouchY = ev.getY(newPointerIndex);
                    mActivePointerId = ev.getPointerId(newPointerIndex);
                }
                break;
            }
        }
//        if(mLastTouchY>350)
//        {
//        
//        	return false;
//        }else
        {
        	 // Save the ID of this pointer
          return true;
          
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
//            mScaleFactor *= detector.getScaleFactor();
//            if (detector.isInProgress()) {
//                mFocusX = detector.getFocusX();
//                mFocusY = detector.getFocusY();
//            }
//            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));
//            mScaleMatrix.setScale(mScaleFactor, mScaleFactor,
//                    mFocusX, mFocusY);
//            mScaleMatrix.invert(mScaleMatrixInverse);
//            invalidate();
//            requestLayout();


            return true;
        }
    }

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		
		Toast.makeText(getContext(), "Single tap", Toast.LENGTH_LONG).show();
		return true;
	}
}
