package com.Intlfaces.retire.ly;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.linkites.retire.utility.Utility;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class LocationService extends Service {

	private int _interval = 5;
	Context appContext;

	boolean gps_enabled = false;
	boolean isServiceRunning, isBackground;
	boolean network_enabled = false;
	
	private NotificationManager nm;
	Notification notification;
	PendingIntent pendingIntent;
	double changelat, changelng;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		appContext = this;
		Toast.makeText(this, "Running In silent mode ", Toast.LENGTH_LONG)
				.show();

		final LocationManager mlocmag = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		final LocationListener mlocList = new MyLocationList();
		nm = (NotificationManager) appContext
				.getSystemService(NOTIFICATION_SERVICE);
		// exceptions will be thrown if provider is not permitted.
		try {
			gps_enabled = mlocmag
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = mlocmag
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		// don't start listeners if no provider is enabled
		if (!gps_enabled && !network_enabled) {
		}

		_interval = 10 * 1000;
		Location loc=null;
		// final Location loc;
		if (network_enabled) {
			loc = mlocmag
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			mlocmag.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
					_interval, 20, mlocList);
		} else if (gps_enabled) {
			loc = mlocmag.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			mlocmag.requestLocationUpdates(LocationManager.GPS_PROVIDER,
					_interval, 20, mlocList);
		}else
		{
			Utility.ShowAlertWithMessage(appContext, "Alert", "Please enable you Location network service.");
		}

		if(loc!=null)
		UpdateWithNewLocation(loc); // This method is used to get updated
									// location.

		mlocmag.requestLocationUpdates(LocationManager.GPS_PROVIDER, _interval,
				0, mlocList);

	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	private void UpdateWithNewLocation(final Location loc) {
		// TODO Auto-generated method stub

		try {
			if (loc != null) {
				Utility.setUserIdPrefernce(appContext, "latd",(long)loc.getLatitude());
				Utility.setUserIdPrefernce(appContext, "long",(long)loc.getLongitude());
				Geocoder gcd = new Geocoder(appContext, Locale.getDefault());
				List<Address> addresses = gcd.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
				if (addresses.size() > 0) 
				    System.out.println(addresses.get(0).getLocality());
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	public void showNotification(String msg, String title) {
		// notification = new Notification(R.drawable.launcher_icon, title,
		// System.currentTimeMillis());
		// Intent notificationIntent = new Intent(LocationService.this,
		// LoginActivity.class);
		// pendingIntent = PendingIntent.getActivity(this, 0,
		// notificationIntent,
		// 0);
		//
		// notification.setLatestEventInfo(this, "AbcSearch Player", msg,
		// pendingIntent);
		// nm.notify(1, notification);
	}

	public class MyLocationList implements LocationListener {

		public void onLocationChanged(Location arg0) {
			// TODO Auto-generated method stub
			UpdateWithNewLocation(arg0);
		}

		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			// Toast.makeText(getApplicationContext(), "GPS Disable ",
			// Toast.LENGTH_LONG).show();
		}

		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			// Toast.makeText(getApplicationContext(), "GPS enabled",
			// Toast.LENGTH_LONG).show();
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

	}

}