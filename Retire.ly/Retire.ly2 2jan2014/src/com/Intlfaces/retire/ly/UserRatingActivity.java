package com.Intlfaces.retire.ly;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class UserRatingActivity extends Activity {

	private int userrating;
	private Context mContext;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_write_review);
		mContext = this;
		final String advisorid = this.getIntent().getStringExtra("advisorid");
		RatingBar ratingbar = (RatingBar) findViewById(R.id.ratingBar);
		ratingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				// TODO Auto-generated method stub
				userrating = (int) rating;
			}
		});
		
		((ImageButton)findViewById(R.id.btn_BackReview)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		((Button)findViewById(R.id.buttonSubmit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String comment = ((EditText)findViewById(R.id.editTextReview)).getText().toString().trim();
				if(comment.length()>0&&userrating>0)
				{
					new PostRating().execute(advisorid,Utility.getUserPrefernce(mContext, "id"),comment);
				}
			}
		});

	}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
}
	private class PostRating extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mProgressDialog = ProgressDialog.show(mContext, "", "Please wait...", true, true, new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					PostRating.this.cancel(true);
				}
			});
			mProgressDialog.setCanceledOnTouchOutside(false);
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String serverUrl = Utility.SERVERURL + "AddReviews";
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			listParams.add(new BasicNameValuePair("advisorid", params[0]));
			listParams.add(new BasicNameValuePair("userid", params[1]));
			listParams.add(new BasicNameValuePair("comment", params[2]));
			listParams.add(new BasicNameValuePair("rating", "" + userrating));

			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);

			return response;
		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if(mProgressDialog!=null)
				mProgressDialog.cancel();
			
			if (result != null) {
					setResult(Activity.RESULT_OK);
					finish();
				}

		}

	}
}
