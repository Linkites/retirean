package com.Intlfaces.retire.ly;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MembershipActivity extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_member_ship);
		
		((Button)findViewById(R.id.buttonInApp)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		((Button)findViewById(R.id.buttonSubmit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String promoCode = ((EditText)findViewById(R.id.editTextPromoid)).getText().toString();
				if(promoCode==null||promoCode.trim().length()==0)
				{
					Utility.showAlert(MembershipActivity.this, "Alert", "Please enter Promo Id.");
					return;
				}
				new GetMemberShip().execute(promoCode);
			}
		});
		
	}
	
	private class GetMemberShip extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String _url = Utility.SERVERURL+"UpdateAdvisorStatus";
			ArrayList<NameValuePair> listNameValuePair = new ArrayList<NameValuePair>();
			if(params!=null)
			{
				listNameValuePair.add(new BasicNameValuePair("promocode", params[0]));
			}
			listNameValuePair.add(new BasicNameValuePair("advisorid", Utility.getUserPrefernce(MembershipActivity.this, "id")));
			String response = Utility.postParamsAndfindJSON(_url, listNameValuePair);
			
			if(response==null)
			{
				if(params!=null)
					return "-1";
			}
			
			return response;
		}
		
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
		
			if(result!=null)
			{
				
				if(result.contains("-"))
				{
					Utility.showAlert(MembershipActivity.this, "Alert", "Please enter correct promot code.");
					return;
				}
				
				if(result.contains("1"))
				{
					Intent in = new Intent();
					in.putExtra("buy", true);
					setResult(Activity.RESULT_OK, in);
					return;
				}
				if(result.contains("0"))
				{
					return;
				}
			}
		
		}
	}
}
