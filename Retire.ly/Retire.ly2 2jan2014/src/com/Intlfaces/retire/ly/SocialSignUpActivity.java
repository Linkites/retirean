package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.SignUpStep2UserActivity.SignUpUser;

import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;

public class SocialSignUpActivity extends Activity {
	private ProgressDialog mBProgressBar;
	private Context mContext;
	private Button btnSubmit;
	private RadioButton radiobtn_Agreement;
	private String userType = "", gender = "", Fname = "", Lname = "",
			Email = "", Password = "", Phone = "", photo = "", age = "",
			occupation, university, file = "", amount, Firm = "",
			designation = "", proffBio = "", webAdd = "", officeAdd = "";
	private EditText editTextOccupation, editTextUniversity, editTextAmount,
			editTextFirm, editTextDesignation, editTextAdvisorUniversity,
			editTextProffBio, editWebAddress, editOfficeAddress, editPhone;
	private ImageView btnCheck;
	byte[] imageByte = {};
	HashMap<String, String> infoMap;
	private int requestFor;
	private final int BUY = 101;
	private double userLat, userLng;
	private static final int LOGOUT = 3;
	private Button btnMale;
	private Button btnFemale;
	private TextViewCustom textUserAgreement;

	@SuppressWarnings("unchecked")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social_sign_up);
		mContext = this;
		
		infoMap = (HashMap<String, String>) this.getIntent().getSerializableExtra("map");
		
		Log.e("hashmap", infoMap.toString());
		Fname = infoMap.get("fname");
		Lname = infoMap.get("lname");
		Email = infoMap.get("email");
		
		textUserAgreement = (TextViewCustom)findViewById(R.id.textUserAgreement);
		btnMale = (Button) findViewById(R.id.btn_male);
		btnFemale = (Button) findViewById(R.id.btn_female);
		btnFemale.setOnClickListener(onClickListener);
		btnMale.setOnClickListener(onClickListener);
		
		Password = "";
		age = infoMap.get("age");
		gender = "Male";
		
		userType = infoMap.get("usertype");
		file = infoMap.get("photo");
		if (infoMap.get("usertype").equalsIgnoreCase("user")) {
			((View) findViewById(R.id.user)).setVisibility(View.VISIBLE);
			((View) findViewById(R.id.advisor)).setVisibility(View.GONE);
		}
		if (infoMap.get("usertype").equalsIgnoreCase("advisor")) {
			((View) findViewById(R.id.advisor)).setVisibility(View.VISIBLE);
			((View) findViewById(R.id.user)).setVisibility(View.GONE);
		}
		btnSubmit = (Button) findViewById(R.id.btn_Submit);
		btnSubmit.setOnClickListener(onClickListener);

		((ImageButton) findViewById(R.id.btn_BackSignup))
				.setOnClickListener(onClickListener);
		textUserAgreement.setOnClickListener(onClickListener);
		
		editTextOccupation = (EditText) findViewById(R.id.txtSOccupation);
		occupation = editTextOccupation.getText().toString();

		editTextUniversity = (EditText) findViewById(R.id.txtSUniversity);
		university = editTextUniversity.getText().toString();

		editTextAmount = (EditText) findViewById(R.id.txtSAmount);
		amount = editTextAmount.getText().toString();

		editPhone = (EditText) findViewById(R.id.txtSPhone);
		Phone = editPhone.getText().toString();

		btnCheck = (ImageView) findViewById(R.id.radiobtn_Agreement);
		btnCheck.setOnClickListener(onClickListener);

		editTextFirm = (EditText) findViewById(R.id.txtSFirm);
		Firm = editTextFirm.getText().toString();

		editTextDesignation = (EditText) findViewById(R.id.txtSDesignation);
		designation = editTextDesignation.getText().toString();

		editTextProffBio = (EditText) findViewById(R.id.txtSProffBio);
		proffBio = editTextProffBio.getText().toString();

		editWebAddress = (EditText) findViewById(R.id.txtSWebAAddress);
		webAdd = editWebAddress.getText().toString();

		editOfficeAddress = (EditText) findViewById(R.id.txtSOfficeAddress);
		officeAdd = editOfficeAddress.getText().toString();

	}

	private boolean isValidateField() {
		boolean isValidate = true;

		if (editTextOccupation.getText().toString().length() == 0) {
			isValidate = false;

		}

		if (editTextUniversity.getText().toString().length() == 0) {
			isValidate = false;
		}
		if (editTextAmount.getText().toString().length() == 0) {
			isValidate = false;
		}
		return isValidate;

	}

	@SuppressLint("NewApi")
	private OnClickListener onClickListener = new OnClickListener() {

		@SuppressLint("NewApi")
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackSignup: {
				Intent intent = new Intent(SocialSignUpActivity.this,
						SignInActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
			}
			case R.id.radiobtn_Agreement: {

				if (btnCheck.isSelected() == false) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.check_icon));
					btnCheck.setSelected(true);
					break;
				}
				if (btnCheck.isSelected()) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.uncheck_icon));
					btnCheck.setSelected(false);
					break;
				}

			}
			case R.id.textUserAgreement:
			{
				if (btnCheck.isSelected() == false) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.check_icon));
					btnCheck.setSelected(true);
					break;
				}
				if (btnCheck.isSelected()) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.uncheck_icon));
					btnCheck.setSelected(false);
					break;
				}
			}

			case R.id.btn_female: {
				btnMale.setSelected(false);
				btnFemale.setSelected(true);
				// btnMale.setBackgroundResource(R.drawable.male);
				gender = "Female";
				if (btnFemale.isSelected()) {
					btnFemale.setTextColor(getResources().getColor(
							R.color.White));
					btnMale.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
				}
				// btnFemale.setBackgroundResource(R.drawable.female_selected);
			}
				break;
			case R.id.btn_male: {

				btnMale.setSelected(true);
				btnFemale.setSelected(false);
				// btnMale.setBackgroundResource(R.drawable.male_sel);
				gender = "Male";
				if (btnMale.isSelected()) {
					btnMale.setTextColor(getResources().getColor(R.color.White));
					btnFemale.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
				}
				// btnFemale.setBackgroundResource(R.drawable.female);

			}
				break;
			case R.id.btn_Submit: {

				if (editPhone.getText().toString().trim().length() == 0) {
					Utility.ShowAlertWithMessage(mContext, "Empty Phone Number",
							"Please enter the Phone number");
					return;
				}
				

//				if (editTextUniversity.getText().toString().trim().length() == 0) {
//					Utility.ShowAlertWithMessage(mContext, "Empty University",
//							"Please enter the University");
//					return;
//				}
				if (editTextAmount.getText().toString().trim().length() == 0) {
					Utility.ShowAlertWithMessage(mContext, "Empty Amount",
							"Please enter the Amount");
					return;
				}
				if (userType.toString().equalsIgnoreCase("advisor")) {
					if (editTextFirm.getText().toString().trim().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Firm Name", "Please enter the firm name");
						return;
					}
//					if (editTextDesignation.getText().toString().trim()
//							.length() == 0) {
//						Utility.ShowAlertWithMessage(mContext,
//								"Empty Designation",
//								"Please enter the designation");
//						return;
//					}
//					if (editTextProffBio.getText().toString().trim().length() == 0) {
//						Utility.ShowAlertWithMessage(mContext,
//								"Empty Professional Bio",
//								"Please enter the professional bio");
//						return;
//					}
//					if (editWebAddress.getText().toString().trim().length() == 0) {
//						Utility.ShowAlertWithMessage(mContext,
//								"Empty Website Name",
//								"Please enter the website name");
//						return;
//					}
//					if (editOfficeAddress.getText().toString().trim().length() == 0) {
//						Utility.ShowAlertWithMessage(mContext,
//								"Empty Office Address",
//								"Please enter the current office address");
//						return;
//					}
				}
				if (userType.equalsIgnoreCase("user")) {
					occupation = "";//editTextOccupation.getText().toString();
					infoMap.put("occupation", occupation);
					Phone =editPhone.getText().toString();
					
					infoMap.put("phone", Phone);
					university = "";//editTextUniversity.getText().toString();
					
					infoMap.put("university", university);
					amount = editTextAmount.getText().toString();
					
					infoMap.put("investableamout", amount);
					
					infoMap.put("lat", ""+userLat);
					infoMap.put("lng", ""+userLng);
					Log.e("infoMapUser", infoMap.toString());
				}
				if (userType.equalsIgnoreCase("advisor")) {
					Firm = editTextFirm.getText().toString();
					infoMap.put("firm", Firm);
					Phone = editPhone.getText().toString();
					infoMap.put("phone", Phone);
					designation = "";//editTextDesignation.getText().toString();
					infoMap.put("title", designation);
					university = "";//editTextUniversity.getText().toString();
					infoMap.put("university", university);
					proffBio = "";//ditTextProffBio.getText().toString();
					infoMap.put("professionalbio", proffBio);
					webAdd = "";//editWebAddress.getText().toString();
					infoMap.put("website", webAdd);
					officeAdd = "";//editOfficeAddress.getText().toString();
					infoMap.put("currentoffice", officeAdd);
					infoMap.put("lat", "" + Utility.getUserIdPrefernce(mContext, "latd"));
					infoMap.put("lng",  "" + Utility.getUserIdPrefernce(mContext, "long"));
					Log.e("infoMapAdvisor", infoMap.toString());
				}
				startSignUp();

			}
				break;
			}
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
	}

	private void startSignUp() {
		// Start to get current location

		if (userType.toString().equalsIgnoreCase("user")) {
			new SignUpUser().execute("UpdateUserProfile");
		} else {
			new SignUpUser()
					.execute("UpdateAdvisorProfile");
		}
	}

	public class SignUpUser extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				if (mBProgressBar != null)
					mBProgressBar.cancel();

				mBProgressBar = ProgressDialog.show(SocialSignUpActivity.this,
						"Sign Up", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								SignUpUser.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("In Lead", "execute asynctask");
			
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			if (userType.equalsIgnoreCase("user")) {
				listParams.add(new BasicNameValuePair("fname", Fname));
				listParams.add(new BasicNameValuePair("lname", Lname));
				listParams.add(new BasicNameValuePair("email", Email));
				listParams.add(new BasicNameValuePair("pwd", Password));
				listParams.add(new BasicNameValuePair("phone", Phone));
				listParams.add(new BasicNameValuePair("age", age));
				listParams
						.add(new BasicNameValuePair("university", university));
				listParams.add(new BasicNameValuePair("photo", file));
				listParams
						.add(new BasicNameValuePair("occupation", occupation));
				listParams.add(new BasicNameValuePair("amount", amount));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				listParams.add(new BasicNameValuePair("gender", gender));
				listParams.add(new BasicNameValuePair("userid", infoMap.get("id")));
			
			} else {
				listParams.add(new BasicNameValuePair("advisorid", infoMap.get("id")));
				listParams.add(new BasicNameValuePair("fname", Fname));
				listParams.add(new BasicNameValuePair("lname", Lname));
				listParams.add(new BasicNameValuePair("email", Email));
				listParams.add(new BasicNameValuePair("pwd", Password));
				listParams.add(new BasicNameValuePair("phone", Phone));
				listParams.add(new BasicNameValuePair("age", age));
				listParams
						.add(new BasicNameValuePair("university", university));
				listParams.add(new BasicNameValuePair("photo", file));
				listParams.add(new BasicNameValuePair("firm", Firm));
				listParams.add(new BasicNameValuePair("title",
						designation));
				listParams.add(new BasicNameValuePair("professionalbio",
						proffBio));
				listParams.add(new BasicNameValuePair("website", webAdd));
				listParams.add(new BasicNameValuePair("currentoffice",
						officeAdd));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				listParams.add(new BasicNameValuePair("gender", gender));
			}
			listParams.add(new BasicNameValuePair("devicetype", "android"));
			
			if (Utility.getUserPrefernce(mContext, "token") != null)
				listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));
			String serverUrl = Utility.SERVERURL + params[0];
			Log.e("Fields into API", listParams.toString());
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);
			Log.e("Response User step 2", response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("-")) {
					Utility.showAlert(mContext, "Alert", "Email Already exists");
					return;
				}
				if (result.contains("1")) {
					String json = new JSONObject(infoMap).toString();
					Utility.setUserPrefernce(mContext, "userprofileinfo",json
							);
					if(userType.equalsIgnoreCase("user"))
						Utility.setUserPrefernce(mContext, "id", infoMap.get("id"));
					else
						Utility.setUserPrefernce(mContext, "id", infoMap.get("id"));
					
					Utility.setBooleanPreferences(mContext, "login", true);
					Utility.setUserPrefernce(mContext, "usertype", userType);
					setResult(Activity.RESULT_OK);
					finish();
				}
				if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert",
							"some error occured, please try again later");
					return;
				}
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == BUY) {
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivity(intent);
				finish();
			}
		}

	}

}
