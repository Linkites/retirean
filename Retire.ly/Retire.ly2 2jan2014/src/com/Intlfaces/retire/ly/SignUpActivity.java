package com.Intlfaces.retire.ly;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.util.Base64;

import com.linkites.retire.utility.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.*;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class SignUpActivity extends Activity {
	private static int RESULT_LOAD_IMAGE = 1;
	private static final int PICK_FROM_GALLERY = 2;
	static final int DATE_DIALOG_ID = 0;
	private ProgressDialog mBProgressBar;
	private Context mContext;
	private String gender;
	private Button btnMale, btnSubmit;
	private Button btnFemale;
	private Button btnLead, btnAdvisor;
	private String userType;
	private EditText editTextFname, editTextLname, editTextEmail, editTextPass,
			editTextPhone, editTextComman, editTextFirm, editTextAmount;
	private TextView TextdateofBirth;
	ImageView imgSignup, btnCheck;
	private int age = 0;
	public int year, month, day;
	private int mYear, mMonth, mDay;
	String Firmname = "";
	String file;

	String editFname = "", editLname = "", editEmail = "", editPassword = "",
			editPhone = "", editDob = "", editUniversity = "", editFirm = "",
			editAmount = "";
	Bitmap thumbnail = null;
	byte[] imageByte = {};
	private double userLat, userLng;
	private final int BUY = 101;
	private static final int LOGOUT = 3;
	private HashMap<String,String>infoMap=new HashMap<String,String>();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up);
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		mContext = this;
		btnMale = (Button) findViewById(R.id.btn_male);
		btnFemale = (Button) findViewById(R.id.btn_female);
		gender = "Male";
		userType = "Lead";
		btnCheck = (ImageView) findViewById(R.id.btn_agree);
		btnCheck.setOnClickListener(onClickListener);
		btnLead = (Button) findViewById(R.id.btn_Lead);
		btnAdvisor = (Button) findViewById(R.id.btn_Advisor);
		btnLead.setOnClickListener(onClickListener);
		btnAdvisor.setOnClickListener(onClickListener);
		btnFemale.setOnClickListener(onClickListener);
		btnMale.setOnClickListener(onClickListener);
		btnSubmit = (Button) findViewById(R.id.btn_Next);
		btnSubmit.setOnClickListener(onClickListener);

		((ImageButton) findViewById(R.id.btn_BackSignup))
				.setOnClickListener(onClickListener);
		editTextFname = (EditText) findViewById(R.id.txtSFName);
		editFname=editTextFname.getText().toString();

		editTextLname = (EditText) findViewById(R.id.txtSLName);
        editLname=editTextLname.getText().toString();

		editTextEmail = (EditText) findViewById(R.id.txtSEmail);
        editEmail=editTextEmail.getText().toString();

		editTextPass = (EditText) findViewById(R.id.txtSPass);
        editPassword=editTextPass.getText().toString();
		editTextPhone = (EditText) findViewById(R.id.txtSPhone);
        editPhone=editTextPhone.getText().toString();
		TextdateofBirth = (TextView) findViewById(R.id.txtSDateOfBirth);
		// editTextFirm.addTextChangedListener(txtWather);
        TextdateofBirth.setOnClickListener(onClickListener);

		editTextFirm = (EditText) findViewById(R.id.txtSFirm);
		// editTextFname.addTextChangedListener(txtWather);
		editFirm = editTextFirm.getText().toString();
		editTextAmount = (EditText) findViewById(R.id.txtSAmount);
		// editTextFname.addTextChangedListener(txtWather);
		editAmount = editTextFirm.getText().toString();

		imgSignup = (ImageView) findViewById(R.id.img_signUpProfile);
		imgSignup.setOnClickListener(onClickListener);

		btnMale.setSelected(true);
		if (btnMale.isSelected()) {
			btnMale.setTextColor(getResources().getColor(R.color.White));
		}
		btnLead.setSelected(true);
		if (btnLead.isSelected()) {
			btnLead.setTextColor(getResources().getColor(R.color.White));
		}
		
		
		
		
		
	}
	

	@SuppressLint("NewApi")
	private OnClickListener onClickListener = new OnClickListener() {

		@SuppressLint("NewApi")
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackSignup: {

				onBackPressed();
				break;
			}
			case R.id.btn_agree: {

				if (btnCheck.isSelected() == false) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.check_icon));
					btnCheck.setSelected(true);
					break;
				}
				if (btnCheck.isSelected()) {
					btnCheck.setBackground(getResources().getDrawable(
							R.drawable.uncheck_icon));
					btnCheck.setSelected(false);
					break;
				}

			}

			case R.id.btn_female: {
				btnMale.setSelected(false);
				btnFemale.setSelected(true);
				// btnMale.setBackgroundResource(R.drawable.male);
				gender = "Female";
				if (btnFemale.isSelected()) {
					btnFemale.setTextColor(getResources().getColor(
							R.color.White));
					btnMale.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
				}
				// btnFemale.setBackgroundResource(R.drawable.female_selected);
			}
				break;
			case R.id.btn_male: {

				btnMale.setSelected(true);
				btnFemale.setSelected(false);
				// btnMale.setBackgroundResource(R.drawable.male_sel);
				gender = "Male";
				if (btnMale.isSelected()) {
					btnMale.setTextColor(getResources().getColor(R.color.White));
					btnFemale.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
				}
				// btnFemale.setBackgroundResource(R.drawable.female);

			}
				break;

			case R.id.btn_Lead: {
				btnAdvisor.setSelected(false);
				btnLead.setSelected(true);
				editTextAmount.setVisibility(View.VISIBLE);
				editTextFirm.setVisibility(View.GONE);
				// btnMale.setBackgroundResource(R.drawable.male);

				if (btnLead.isSelected()) {
					btnLead.setTextColor(getResources().getColor(R.color.White));
					btnAdvisor.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
					userType = "Lead";
				}
			}
				break;

			case R.id.txtSDateOfBirth: {

				showDialog(DATE_DIALOG_ID);

			}
				break;
			case R.id.btn_Advisor: {
				btnAdvisor.setSelected(true);
				btnLead.setSelected(false);
				// btnMale.setBackgroundResource(R.drawable.male_sel);
				editTextAmount.setVisibility(View.GONE);
				editTextFirm.setVisibility(View.VISIBLE);
				if (btnAdvisor.isSelected()) {
					btnAdvisor.setTextColor(getResources().getColor(
							R.color.White));
					btnLead.setTextColor(getResources().getColor(
							R.color.retirely_theme_color));
					userType = "Advisor";
				}
				// btnFemale.setBackgroundResource(R.drawable.female);
			}
				break;

			case R.id.img_signUpProfile: {
				Intent in = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(in, RESULT_LOAD_IMAGE);
			}
				break;

			case R.id.btn_Next: {
			
					if (editTextFname.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty First Name",
								"Please check the First Name field");
						return;
					}
					if (editTextLname.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Last Name",
								"Please check the Last Name field");
						return;
					}
					if (!Utility.isValidEmail(editTextEmail.getText()
							.toString())
							|| editTextEmail.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext, "Invalid Email",
								"Please check the email address");
						return;
					}
					if (editTextPass.getText().toString().length() < 6) {
						Utility.ShowAlertWithMessage(mContext,
								"Password too short",
								"Password should be greater then 6 digits");
						return;
					}
					if (editTextPhone.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Phone Field",
								"Please check the phone field");
						return;
					}
					if (editDob.contains("Date Of Birth")) {
						Utility.ShowAlertWithMessage(mContext, "Invalid date",
								"Please select date of birth");
						return;
					}
					if(btnCheck.isSelected()==false){
						Utility.ShowAlertWithMessage(mContext, "Alert", "Please accept the terms and conditions of retire.ly");
						return;
					}

				
					if (userType == "Advisor") {
												
						
						
						infoMap.put("fname", editTextFname.getText().toString());
						infoMap.put("lname", editTextLname.getText().toString());
						infoMap.put("email", editTextEmail.getText().toString());
						infoMap.put("pwd", editTextPass.getText().toString());
						infoMap.put("phone", editTextPhone.getText().toString());
						infoMap.put("age", "" + age);
						infoMap.put("photo", file);
						infoMap.put("firm", editTextFirm.getText().toString());
						infoMap.put("lat", "" + Utility.getUserIdPrefernce(mContext, "latd"));
						infoMap.put("lng", "" + Utility.getUserIdPrefernce(mContext, "long"));
						infoMap.put("gender", gender);
						
						editFirm = editTextFirm.getText().toString();
						editFname = editTextFname.getText().toString();
						editLname = editTextLname.getText().toString();
						editEmail = editTextEmail.getText().toString();
						editPassword = editTextPass.getText().toString();
						editPhone = editTextPhone.getText().toString();
						
						startSignUp();
						
					}else{
					//amount\//investableamout							
						editAmount = editTextAmount.getText().toString();
						editFname = editTextFname.getText().toString();
						editLname = editTextLname.getText().toString();
						editEmail = editTextEmail.getText().toString();
						editPassword = editTextPass.getText().toString();
						editPhone = editTextPhone.getText().toString();
						infoMap.put("fname", editTextFname.getText().toString());
						infoMap.put("lname", editTextLname.getText().toString());
						infoMap.put("email", editTextEmail.getText().toString());
						infoMap.put("pwd", editTextPass.getText().toString());
						infoMap.put("phone", editTextPhone.getText().toString());
						infoMap.put("age", "" + age);
						infoMap.put("photo", file);
						infoMap.put("amount", editTextAmount.getText().toString());
						infoMap.put("lat", "" + Utility.getUserIdPrefernce(mContext, "latd"));
						infoMap.put("lng", "" + Utility.getUserIdPrefernce(mContext, "long"));
						infoMap.put("gender", gender);
						startSignUp();
						
						}
			}					
				break;
			}
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// create a new DatePickerDialog with values you want to show
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		// the callback received when the user "sets" the Date in the
		// DatePickerDialog
		public void onDateSet(DatePicker view, int yearSelected,
				int monthOfYear, int dayOfMonth) {
			year = yearSelected;
			month = monthOfYear + 1;
			day = dayOfMonth;
			age = mYear - year;
			// Set the Selected Date in Select date Button
			TextdateofBirth.setText(day + "-" + month + "-" + year);
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
	}

	private void startSignUp() {
		// Start to get current location

//		MyLocation location = new MyLocation();
//
//		location.getLocation(getApplicationContext(), new LocationResult() {
//
//			@Override
//			public void gotLocation(Location location) {
//				// TODO Auto-generated method stub
//				if (location != null) {
//					userLat = location.getLatitude();
//					userLng = location.getLongitude();
//					Utility.setFloatPrefrence(mContext, "latitude",
//							(float) userLat);
//					Utility.setFloatPrefrence(mContext, "longitude",
//							(float) userLng);
//					runOnUiThread(new Runnable() {
//
//						@Override
//						public void run() {
//							// TODO Auto-generated method stub
//
//							userLat = 22.70450;// 34.197311;
//							userLng = 75.87373;// -118.643982;
//
//							if(userType=="Advisor"){
//							new SignUpUser().execute("AdvisorSignUp");
//							}else{
//								new SignUpUser().execute("UserSignUp");
//							}
//
//						}
//					});
//				}
//			}
//		});
		if(userType=="Advisor"){
			new SignUpUser().execute("AdvisorSignUp");
			}else{
				new SignUpUser().execute("UserSignUp");
			}
	}

	public class SignUpUser extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
			
				mBProgressBar = ProgressDialog.show(SignUpActivity.this,
						"Sign Up", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								SignUpUser.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("In Lead", "execute asynctask");
			
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			if (userType.equalsIgnoreCase("Advisor")) {
				listParams.add(new BasicNameValuePair("fname", editFname));
				listParams.add(new BasicNameValuePair("lname", editLname));
				listParams.add(new BasicNameValuePair("email", editEmail));
				listParams.add(new BasicNameValuePair("pwd", editPassword));
				listParams.add(new BasicNameValuePair("phone", editPhone));
				listParams.add(new BasicNameValuePair("age", "" + age));
				listParams.add(new BasicNameValuePair("photo", file));
				listParams.add(new BasicNameValuePair("firm", editFirm));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				listParams.add(new BasicNameValuePair("gender", gender));
			}else{
				listParams.add(new BasicNameValuePair("fname", editFname));
				listParams.add(new BasicNameValuePair("lname", editLname));
				listParams.add(new BasicNameValuePair("email", editEmail));
				listParams.add(new BasicNameValuePair("pwd", editPassword));
				listParams.add(new BasicNameValuePair("phone", editPhone));
				listParams.add(new BasicNameValuePair("age", "" + age));
				listParams.add(new BasicNameValuePair("photo", file));
				listParams.add(new BasicNameValuePair("amount", editAmount));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				listParams.add(new BasicNameValuePair("gender", gender));
			}
			
			listParams.add(new BasicNameValuePair("devicetype", "android"));
			
			if (Utility.getUserPrefernce(mContext, "token") != null)
				listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));
                    
			String serverUrl = Utility.SERVERURL + params[0];
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);
			Log.e("Response in signup activity", response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("{") && result.length() > 5) {
					HashMap<String, String> map = null;
					;
					try {
						map = Utility.toMap(new JSONObject(result));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (userType == "Lead") {
						Utility.setUserPrefernce(mContext, "id",
								map.get("userid"));
						Utility.setUserPrefernce(mContext, "usertype", "user");
					} else {
						Utility.setUserPrefernce(mContext, "id",
								map.get("advisorid"));
						Utility.setUserPrefernce(mContext, "usertype",
								"advisor");
					}

					map.put("gender",
							Utility.getUserPrefernce(mContext, "gender"));
					result = new JSONObject(map).toString();
					Utility.setBooleanPreferences(mContext, "login", true);
					Utility.setUserPrefernce(mContext, "userinfo", result);
					
					Utility.setUserPrefernce(mContext, "userprofileinfo",
							result);
					
					setResult(Activity.RESULT_OK);
					finish();

				} else if (result.contains("-")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.emailexist));
				} else if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.noexist));
				}
			} else {
				Utility.showAlert(mContext, "Alert",
						getResources().getString(R.string.nonetwork));
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == BUY) {
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivity(intent);
				finish();
			}
		}
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
				&& null != data) {

			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

			String picturePath = cursor.getString(columnIndex);
			Log.e("Picture_path", picturePath);
			cursor.close();
			thumbnail = (BitmapFactory.decodeFile(picturePath));

			runOnUiThread(new Thread() {
				public void run() {
					imgSignup.setImageBitmap(thumbnail);
				}
			});
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			imageByte = baos.toByteArray();
			file = Base64.encode(imageByte);
//			thumbnail = null;
//			imageByte = null;
	
		}
	}
}
