package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.linkites.retire.util.ImageDownloader;
import com.linkites.retire.util.ImageDownloader.Mode;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OtherUserProfileActivityView extends FragmentActivity {
	private int requestFor, fromView;
	@SuppressWarnings("unused")
	private TextView txtvFname, txtvLname, txtvAge, txtvLocation, txtvFirm,
			txtvTitle, txtReviewCount,txtvKeyword,txtFProfessionalBio;
	private EditText txtvEmail, txtvPhone,editTextFirm,
			txtvWebsitee, txtvOccupation, txtvAmount, txtvUniversity,txtCurrentOffice;
	private ImageView backIcon;
	@SuppressWarnings("unused")
	private ImageView imageRating, imageOnline, imageViewPic, imageFav,
			imageMenu;
	private ImageButton btnuserMenuProfile;
	private Bitmap bitmap;
	private boolean isAdvisor;
	private Context mContext;
	private static final int REVIEWUPDATE = 101;
	private GoogleMap map;
	HashMap<String, String> Infomap = new HashMap<String, String>();
	private int Friendid;
	private LinearLayout menuLayout;
	LayoutInflater inflater;
	private View parentView;
	private String strbroadcast;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_profile_user_new);

		// bitmap = Utility.getBitmap();
		requestFor = this.getIntent().getIntExtra("requestFor", 0);
		mContext = this;
		strbroadcast = this.getIntent().getStringExtra("broadcast");
		
	
		
		Infomap = (HashMap<String, String>) this.getIntent()
				.getSerializableExtra("detail");
	
		Log.e("This is other activity problem", Infomap.toString());
		
		
		
		
		fromView = this.getIntent().getIntExtra("from", 0);
	
		if (requestFor == Constants.ADVISOR || Infomap.get("usertype").contains("advisor")) {
			initadvisor();
		} else {
			initUser();
		}

		String userType = Utility.getUserPrefernce(mContext, "usertype");
		if (userType.equalsIgnoreCase("user"))
			isAdvisor = false;
		else
			isAdvisor = true;

		// hide default image of navigation bar
				((ImageView)findViewById(R.id.ImageViewNavTitle)).setVisibility(View.INVISIBLE);
				
		Friendid = Integer.parseInt(Infomap.get("favoriteid"));
		int status = Integer.parseInt(Infomap.get("favoriteid"));
		menuLayout = (LinearLayout) findViewById(R.id.MenuLayout);
		
		if (!isAdvisor) {
			String[] menuTitle = { "Block Advisor", "Add to Favorite",
					"Write a review", (status == 0) ? "" :"Send Message" };
			String[] menuTitle1 = { "Block Advisor", "Write a review",
					(status == 0) ? "" : "Send Message" };

			int[] res = { R.drawable.btn_block_advisor,
					R.drawable.btn_add_favourite, R.drawable.btn_write_review,
					R.drawable.btn_send_message };
			int[] res2 = { R.drawable.btn_block_advisor,
					R.drawable.btn_write_review, R.drawable.btn_send_message };
			
			int menuscount = (Friendid == 0) ? 4 : 3;
			LayoutInflater inflater = LayoutInflater.from(this);

			for (int i = 0; i < menuscount; i++) {
				View v = inflater.inflate(R.layout.pop_up_menu, null);
				TextViewCustom txtView = (TextViewCustom) v
						.findViewById(R.id.txtMenuTitle);
				ImageView imageIcon = (ImageView) v
						.findViewById(R.id.imgMenuIcon);
				if (Friendid == 0) {
					txtView.setText(menuTitle[i]);
					imageIcon.setImageResource(res[i]);
					if (menuTitle[i].trim().length() > 0) {
					menuLayout.addView(v);
					v.setTag(i + 1);
					v.setOnClickListener(onMenuClick);
					}
				} else {

					txtView.setText(menuTitle1[i]);
					imageIcon.setImageResource(res2[i]);
					if (menuTitle1[i].trim().length() > 0) {
						menuLayout.addView(v);
						v.setTag(i + 1);
						v.setOnClickListener(onMenuClick);
					}
				}

			}

		} else {
			String[] menuTitle = { "Block User", "Add to Favorite",
					(status == 0) ? "" :"Send Message" };
			String[] menuTitle1 = { "Block User", (status == 0) ? "" : "Send Message" };
			int[] res = { R.drawable.btn_block_advisor,
					R.drawable.btn_add_favourite, R.drawable.btn_send_message };
			int[] res2 = { R.drawable.btn_block_advisor,
					R.drawable.btn_send_message };

			int menuscount = (Friendid == 0) ? 3 : 2;
			LayoutInflater inflater = LayoutInflater.from(this);
			for (int i = 0; i < menuscount; i++) {
				View v = inflater.inflate(R.layout.pop_up_menu, null);
				TextViewCustom txtView = (TextViewCustom) v
						.findViewById(R.id.txtMenuTitle);
				ImageView imageIcon = (ImageView) v
						.findViewById(R.id.imgMenuIcon);
				if (Friendid == 0) {
					txtView.setText(menuTitle[i]);
					imageIcon.setImageResource(res[i]);
					if (menuTitle[i].trim().length() > 0) {
						menuLayout.addView(v);
						v.setTag(i + 1);
						v.setOnClickListener(onMenuClick);
						}
				} else {
					txtView.setText(menuTitle1[i]);
					imageIcon.setImageResource(res2[i]);
					if (menuTitle1[i].trim().length() > 0){
					menuLayout.addView(v);
					v.setTag(i + 1);
					v.setOnClickListener(onMenuClick);
					}
				}

		
			}
		}

		backIcon = (ImageView) findViewById(R.id.btn_BackHome);
		backIcon.setOnClickListener(onClickListener);

		btnuserMenuProfile = (ImageButton) findViewById(R.id.userMenuProfile);
		btnuserMenuProfile.setOnClickListener(onClickListener);

	}

	private OnClickListener onMenuClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int tag = (Integer) v.getTag();
			menuLayout.setVisibility(View.GONE);
			switch (tag) {
			case 1:
				new MarkAndUnmarkAsFavorite().execute(false);
				break;
			case 2:
				if (Friendid == 0) {
					new MarkAndUnmarkAsFavorite().execute(true);
				} else {
					if (!isAdvisor) {
						// Write review
						Intent intent = new Intent(mContext,
								UserRatingActivity.class);
						intent.putExtra("advisorid", Infomap.get("advisorid"));
						startActivityForResult(intent, REVIEWUPDATE);
					} else {
						// Profile Detail
						if (fromView == Constants.FAVOURITE) {
							finish();
						} else {
							Intent intent = new Intent(mContext,
									ChatActivity.class);
							requestFor = (Infomap.get("usertype")
									.equalsIgnoreCase("user")) ? Constants.USER
									: Constants.ADVISOR;
							intent.putExtra("requestFor", requestFor);
							intent.putExtra("detail", Infomap);
							startActivity(intent);
						}
					}
				}

				break;
			case 3:
				if (Friendid == 0) {
					if (!isAdvisor) {
						// Write review
						Intent intent = new Intent(mContext,
								UserRatingActivity.class);
						intent.putExtra("advisorid", Infomap.get("advisorid"));
						startActivityForResult(intent, REVIEWUPDATE);
					} else {
						// Profile Details
						if (fromView == Constants.FAVOURITE) {
							finish();
						} else {
							Intent intent = new Intent(mContext,
									ChatActivity.class);
							requestFor = (Infomap.get("usertype")
									.equalsIgnoreCase("user")) ? Constants.USER
									: Constants.ADVISOR;
							intent.putExtra("requestFor", requestFor);
							intent.putExtra("detail", Infomap);
							startActivity(intent);
						}
					}
				} else {
					if (!isAdvisor) {
						// Profile detail
						if (fromView == Constants.FAVOURITE) {
							finish();
						} else {
							Intent intent = new Intent(mContext,
									ChatActivity.class);
							requestFor = (Infomap.get("usertype")
									.equalsIgnoreCase("user")) ? Constants.USER
									: Constants.ADVISOR;
							intent.putExtra("requestFor", requestFor);
							intent.putExtra("detail", Infomap);
							startActivity(intent);
						}
					}
				}
				break;
			case 4:
				if (!isAdvisor) {
					// Profile detail
					if (fromView == Constants.FAVOURITE) {
						finish();
					} else {
						Intent intent = new Intent(mContext, ChatActivity.class);
						requestFor = (Infomap.get("usertype")
								.equalsIgnoreCase("user")) ? Constants.USER
								: Constants.ADVISOR;
						intent.putExtra("requestFor", requestFor);
						intent.putExtra("detail", Infomap);
						startActivity(intent);
					}
				}
				break;

			default:
				break;
			}
		}
	};

	private void initadvisor()
	{
		inflater = LayoutInflater.from(this);
		parentView = inflater.inflate(R.layout.activity_profile_advisor, null);
		((LinearLayout)findViewById(R.id.layoutProfile)).addView(parentView);
		txtvFname = (TextView) parentView.findViewById(R.id.txtNameAdvisor);
		
		
		txtFProfessionalBio = (EditText)parentView.findViewById(R.id.editTextAdvisorProffInfo);
		txtFProfessionalBio.setFocusable(false);
		txtFProfessionalBio.setClickable(false);
		txtvUniversity = (EditText)parentView.findViewById(R.id.editTextAdvisorUniversity);
		txtvUniversity.setFocusable(false);
		txtvUniversity.setClickable(false);
		txtvOccupation = (EditText)parentView.findViewById(R.id.editTextAdvisorDesignation);
		txtvOccupation.setFocusable(false);
		txtvOccupation.setClickable(false);
		txtvLocation = (TextView) parentView.findViewById(R.id.txtLocationAdvisor);
		txtvAge = (TextView) parentView.findViewById(R.id.txtAgeAdvisor);
		txtReviewCount = (TextView) parentView.findViewById(R.id.txtReviewCount);
		txtvEmail = (EditText) parentView.findViewById(R.id.editTextAdvisorMail);
		txtvEmail.setFocusable(false);
		txtvEmail.setClickable(false);
		txtvPhone = (EditText) parentView.findViewById(R.id.editTextAdvisorPhone);
		txtvPhone.setFocusable(false);
		txtvPhone.setClickable(false);
		txtvWebsitee = (EditText) parentView.findViewById(R.id.editTextAdvisorWebsite);
		txtvWebsitee.setFocusable(false);
		txtvWebsitee.setClickable(false);
		txtCurrentOffice = (EditText)parentView.findViewById(R.id.editTextAdvisorOfficeAddress);
		txtCurrentOffice.setFocusable(false);
		txtCurrentOffice.setClickable(false);
		imageRating = (ImageView) findViewById(R.id.userMenuProfileRating);
		imageViewPic = (ImageView) parentView.findViewById(R.id.ImageHeaderPic);
		editTextFirm = (EditText)parentView.findViewById(R.id.editTextAdvisorFIRM);
		setupAdvisor(Infomap);
		
	}
	private void setupAdvisor(final HashMap<String, String> info) {
		txtFProfessionalBio.setText((Infomap.get("professionalbio").trim().length()==0)?"Not Available":Infomap.get("professionalbio"));
		txtvUniversity.setText((Infomap.get("university").trim().length()==0)?"Not Available":Infomap.get("university"));
		txtvOccupation.setText(info.get("registeredtitle"));
		txtCurrentOffice.setText((Infomap.get("currentoffice").trim().length()==0)?"Not Available":Infomap.get("currentoffice"));
		// if(map!=null)
		if (info != null && info.get("lname") != null) {
			txtvFname
					.setText(info.get("fname") + " " + info.get("lname"));
		} else {
			txtvFname.setText(info.get("fname"));
		}
		
	
		editTextFirm.setText((Infomap.get("firm").trim().length()==0)?"Not Available":Infomap.get("firm"));
		editTextFirm.setFocusable(false);
		editTextFirm.setClickable(false);
		
		txtvLocation.setText((Infomap.get("location").trim().length()==0)?"Not Available":Infomap.get("location"));
		txtvAge.setText(info.get("age"));
		txtReviewCount.setText("Reviews(" + info.get("reviewCount") + ")");
		// Advisor Professional info
		txtReviewCount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Integer.parseInt(info.get("reviewCount")) > 0) {
					Intent intent = new Intent(mContext,
							GetAdvisorReviewActivity.class);
					intent.putExtra("advisorid", info.get("advisorid"));
					startActivity(intent);
				}
			}
		});
		
		txtvEmail.setText((Infomap.get("email").trim().length()==0)?"Not Available":Infomap.get("email"));
		txtvEmail.setOnClickListener(onClickListener);
		txtvPhone.setText((info.get("phone") != null) ? info.get("phone")
						: "Not Available");
		txtvPhone.setOnClickListener(onClickListener);
		txtvWebsitee.setText((info.get("website") != null ? info
						.get("website") : ""));
		txtvWebsitee.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String website=txtvWebsitee.getText().toString();
				if (Patterns.WEB_URL.matcher(website).matches()){
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
			    startActivity(intent);
				}
			}
		});
		
		int rate = Math.round(Float.valueOf(info.get("overallrating")));
		//Integer.parseInt(info.get("overallrating"));
		int resid = R.drawable.review_blank_fill;
		if (rate > 0){
			resid = getResources().getIdentifier("review_" + rate + "_fill",
					"drawable", getPackageName());
			imageRating.setImageResource(resid);
		}
		String url = null;
		if (Infomap.get("photopath") != null)
			if (info.get("photopath").contains("http")
					|| info.get("photopath").contains("https")) {
				url = info.get("photopath");
			} else {
				url = Utility.IMAGEURL + info.get("photopath");
			}

		if (url != null) {
			ImageDownloader imageLoaaderr = new ImageDownloader();
			imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
			imageLoaaderr.download(url, imageViewPic, BitmapFactory
					.decodeResource(getResources(), R.drawable.dummy));
		}
	}

	private void setLocationOnMap() {
		map.addMarker(new MarkerOptions().position(
				new LatLng(Double.parseDouble(Infomap.get("lat")), Double
						.parseDouble(Infomap.get("lng")))).title(
				Infomap.get("fname")));
	}

	private void initUser()
	{
		inflater = LayoutInflater.from(this);
		parentView = inflater.inflate(R.layout.activity_user_detail, null);
		((LinearLayout)findViewById(R.id.layoutProfile)).addView(parentView);
		txtvFname = (TextView) parentView.findViewById(R.id.txtNameUser);
		txtvLocation = (TextView) parentView.findViewById(R.id.txtLocationUser);
		txtvAge = (TextView) parentView.findViewById(R.id.txtAgeUser);
		txtvOccupation = (EditText)parentView.findViewById(R.id.editTextUserOccupation);
		txtvOccupation.setFocusable(false);
		txtvOccupation.setClickable(false);
		txtvAmount = (EditText) parentView.findViewById(R.id.editTextUserAmount);
		txtvAmount.setFocusable(false);
		txtvAmount.setClickable(false);
		txtvUniversity = (EditText) parentView.findViewById(R.id.editTextUserUniversity);
		txtvUniversity.setFocusable(false);
		txtvUniversity.setClickable(false);
		txtvEmail = (EditText) parentView.findViewById(R.id.editTextUserMailuser);
		txtvPhone = (EditText) parentView.findViewById(R.id.editTextUserPhoneuser);
		txtvEmail.setFocusable(false);
		txtvEmail.setClickable(false);
		txtvPhone.setFocusable(false);
		txtvPhone.setClickable(false);
		imageViewPic = (ImageView) parentView.findViewById(R.id.ImageHeaderPicuser);
		setupUser(Infomap);
	}
	private void setupUser(HashMap<String, String> info) {
		if (info.get("lname") != null) {
			txtvFname
					.setText(Infomap.get("fname") + " " + Infomap.get("lname"));
		} else {
			txtvFname.setText(Infomap.get("fname"));
		}
		txtvLocation.setText((Infomap.get("location").trim().length()==0)?"Not Available":Infomap.get("location"));
		txtvAge.setText(Infomap.get("age"));
		txtvOccupation.setText((Infomap.get("occupation").trim().length()==0)?"Not Available":Infomap.get("occupation"));
		txtvAmount.setText((Infomap.get("investableamout").trim().length()==0)?"Not Available":"$"+Infomap.get("investableamout"));
		txtvUniversity.setText((Infomap.get("university").trim().length()==0)?"Not Available":Infomap.get("university"));
		// User Contact info
		txtvEmail.setText(Infomap.get("email"));
		txtvEmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String email= txtvEmail.getText().toString();
				Log.e("userEmail", email);
				if(Utility.isValidEmail(email)){
					Intent intent = new Intent(android.content.Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.putExtra(android.content.Intent.EXTRA_EMAIL, email);
					startActivity(intent);
				}
			}
		});
		txtvPhone.setText((Infomap.get("phone").trim().length()==0)?"Not Available":Infomap.get("phone"));
		txtvPhone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String posted_by = txtvPhone.getText().toString();
				 String uri = "tel:" + posted_by.trim() ;
				 if(!isEmulator()){
				 Intent intent = new Intent(Intent.ACTION_CALL);
				 intent.setData(Uri.parse(uri));
				 startActivity(intent);
				 }
			}
		});
		if (bitmap != null) {
			imageViewPic.setImageBitmap(bitmap);
		} else {
			new Thread(downloadpic).start();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	// Click listener for favorite image
	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackHome: {
				finish();
			}
				break;
			case R.id.userMenuProfile: {
				if (menuLayout.getVisibility() == View.VISIBLE) {
					menuLayout.setVisibility(View.GONE);
				} else {
					menuLayout.setVisibility(View.VISIBLE);
				}
			}
				break;
			
			case R.id.editTextUserMailuser:{
				
			}
			break;
			case R.id.editTextAdvisorWebsite:{
				
			}
			break;
			}
		}
	};

	@Override
	public void onBackPressed() {
		super.onBackPressed();

	};

	// Download user image
	private Runnable downloadpic = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String url = null;
			if (Infomap.get("photopath") != null
					&& Infomap.get("photopath").contains("http")
					|| Infomap.get("photopath").contains("https")) {
				url = Infomap.get("photopath");
			} else if (Infomap.get("photopath") == null) {
				url = Utility.IMAGEURL + "";
			} else {
				url = Utility.IMAGEURL + Infomap.get("photopath");
			}
			bitmap = Utility.getBitmap(url);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (bitmap != null)
						imageViewPic.setImageBitmap(bitmap);
					else
						imageViewPic.setImageResource(R.drawable.dummy);
				}
			});
		}
	};

	// Mark as favorite

	Runnable updateProfile = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String serverUrl = Utility.SERVERURL + "GetAdvisorDetail";
			serverUrl = serverUrl + "&advisorid=" + Infomap.get("advisorid");
			final String response = Utility.getJSONFromUrl(serverUrl);
			if (response != null) {
				JSONObject json;
				try {
					json = new JSONObject(response);
					JSONArray jarray = json.getJSONArray("advisors");
					json = jarray.getJSONObject(0);
					json = json.getJSONObject("advisor");
					int rating = Math.round(Float.valueOf(json.getString("overallrating")));
					Infomap.put("overallrating",
							""+rating);
					Infomap.put("reviewCount", json.getString("reviewCount"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (response != null) {
						
						
						int rate = Integer.parseInt(Infomap
								.get("overallrating"));

						int resid = R.drawable.review_blank_fill;
						if (rate > 0)
							resid = getResources().getIdentifier(
									"review_" + rate + "_fill", "drawable",
									getPackageName());
						imageRating.setImageResource(resid);
						txtReviewCount.setText("Reviews(" + Infomap.get("reviewCount") + ")");
						LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(strbroadcast).putExtra("type", "detail").putExtra("details", Infomap));

					}
				}
			});
		}
	};

	// MarkAndUnmark as favorite
	private class MarkAndUnmarkAsFavorite extends
			AsyncTask<Boolean, Void, String> {
		private boolean isBlock;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Boolean... isFavorite) {
			// TODO Auto-generated method stub
			String response = null;
			isBlock = !isFavorite[0];
			//String userinfo = Utility.getUserPrefernce(mContext, "userprofileinfo");
			String userid = "";
			userid = Utility.getUserPrefernce(mContext, "id");
			
			if (isFavorite[0]) {
				String url = Utility.SERVERURL + "MarkAsFavorites";
				
				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					listParams.add(new BasicNameValuePair("userid", Infomap
							.get("userid")));
					listParams.add(new BasicNameValuePair("advisorid", userid));
				} else {
					listParams.add(new BasicNameValuePair("advisorid", Infomap
							.get("advisorid")));
					listParams.add(new BasicNameValuePair("userid", userid));
				}
				listParams.add(new BasicNameValuePair("usertype", Utility
						.getUserPrefernce(mContext, "usertype")));
				response = Utility.postParamsAndfindJSON(url, listParams);

			} else {
				String url = Utility.SERVERURL + "MarkAsBlocked";
				// url = url + "&senderid="
				// + Utility.getUserPrefernce(mContext, "id");

				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
				listParams.add(new BasicNameValuePair("senderid", Utility
						.getUserPrefernce(mContext, "id")));
				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					listParams.add(new BasicNameValuePair("senderusertype",
							"advisor"));
					listParams.add(new BasicNameValuePair("blockeduserid",
							Infomap.get("userid")));
				} else {

					// url = url + "&senderusertype=user&blockeduserid="
					// + hashUser.get("advisorid");

					listParams.add(new BasicNameValuePair("senderusertype",
							"user"));
					listParams.add(new BasicNameValuePair("blockeduserid",
							Infomap.get("advisorid")));
				}
				response = Utility.postParamsAndfindJSON(url, listParams);
				;
			}
			return response;// http://projects.linkites.com/retirely/api/v1/api.php?method=MarkAsBlocked&senderid=124&senderusertype=user&blockeduserid=76
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result != null) {
				
				if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert",
							"Unable to process, please try again.");
				} else {
					if (fromView == Constants.FAVOURITE)
						LocalBroadcastManager.getInstance(mContext)
								.sendBroadcast(new Intent("updateFav"));
					else {
						String userid = null;

						if (Utility.getUserPrefernce(mContext, "usertype")
								.equalsIgnoreCase("advisor")) {
							userid = Infomap.get("userid");
						} else {
							userid = Infomap.get("advisorid");
						}
						if (isBlock) {
							
							LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(strbroadcast).putExtra("type", "block"));
						}else
						{
							LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(strbroadcast).putExtra("type", "fav"));
						}
//						LocalBroadcastManager.getInstance(mContext)
//								.sendBroadcast(
//										new Intent("updateuser").putExtra("id",
//												userid).putExtra("block",
//												isBlock));
					}

				}
			} else {
				Utility.showAlert(mContext, "Alert",
						"Please check your internet connection.");
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REVIEWUPDATE) {
			if (resultCode == Activity.RESULT_OK) {
				new Thread(updateProfile).start();
			}
		}
	}
	
	public boolean isEmulator() {
		return Build.MANUFACTURER.equals("unknown");
	}

}
