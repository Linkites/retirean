package com.Intlfaces.retire.ly;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.util.ImageDownloader;
import com.linkites.retire.util.RoundedImageView;
import com.linkites.retire.util.ImageDownloader.Mode;
import com.linkites.retire.utility.Base64Coder;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.ChatListAdapter;
import com.retirely.adapters.RetirelyDbAdapter;




import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.TextView.OnEditorActionListener;

public class ChatActivity extends Activity {

	private Context mContext;
	String id;
	private ArrayList<HashMap<String, String>> arrayListDemo;
	private ArrayList<String> arrayListChat;
	private ProgressDialog mProgressDialog;
	private boolean isLoading, isbackground, isServiceRuning, isAdvisor;
	private HashMap<String, Bitmap> hashBitmap;
	private HashMap<String, String> hashUser;
	private RelativeLayout chatListLayout;
	private ScrollView scrollChats;
	private ChatListAdapter adapter;
	private ProgressDialog applicationDialog;
	private EditText edtTxtMessage;
	private String message;
	private Thread thDownloadImage;
	private int previousCount = 0;
	private ListView chatList;
	private Intent intent;
	private Bitmap bitmap;
	private int requestFor;
	private RoundedImageView imageView;
	private RetirelyDbAdapter dbAdapter;
	private LinearLayout menuLayout;
	private int Friendid;
	private Bitmap leftUser, rightUser;
	private LinearLayout layoutChat;
//	1ΔmaxterΔ0ΔhiΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δuser
//	1ΔmaxterΔ2ΔhiΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δuser
//
//
//	2ΔtestΔ0ΔhelloΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δadvisor
//	2ΔtestΔ1ΔhelloΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δadvisor
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.chat_layout);
		mContext = this;
		((ImageView)findViewById(R.id.ImageViewNavTitle)).setVisibility(View.INVISIBLE);
		intent = getIntent();
		requestFor = this.getIntent().getIntExtra("requestFor", 0);
		chatListLayout = (RelativeLayout)findViewById(R.id.rlayoutChatList);

		dbAdapter = new RetirelyDbAdapter(this); 
		
		imageView = (RoundedImageView) findViewById(R.id.imageViewUser);
		imageView.setScaleType(ScaleType.CENTER_CROP);
		imageView.setCornerRadius(50);
		imageView.setBorderWidth(2);
		imageView.setBorderColor(Color.DKGRAY);
		imageView.setRoundBackground(true);
		
		hashUser = (HashMap<String, String>) intent
				.getSerializableExtra("detail");
		if(Utility.getUserPrefernce(mContext, "userimg")!=null)
			rightUser = Base64Coder.decodeBase64(Utility.getUserPrefernce(mContext, "userimg"));
		
		
		
		String userType = Utility.getUserPrefernce(mContext, "usertype");//112
		
		
		if(userType.equalsIgnoreCase("user"))
			isAdvisor = false;
		else
			isAdvisor = true;
		layoutChat = (LinearLayout)findViewById(R.id.layoutChat);
		scrollChats = (ScrollView)findViewById(R.id.chatlayoutscroll);
		Friendid = Integer.parseInt(hashUser.get("favoriteid"));
		menuLayout  = (LinearLayout)findViewById(R.id.MenuLayout);
		if (!isAdvisor) {
			String[] menuTitle = {"Block Advisor","Add to Favorite","Write a review","Profile Details"};
			String[] menuTitle1 = {"Block Advisor","Write a review","Profile Details"};
			
			int [] res = {R.drawable.btn_block_advisor,R.drawable.btn_add_favourite,R.drawable.btn_write_review,R.drawable.btn_send_message};
			int [] res2 = {R.drawable.btn_block_advisor,R.drawable.btn_write_review,R.drawable.btn_send_message};
			int menuscount = (Friendid==0)?4:3;
			LayoutInflater inflater = LayoutInflater.from(this);
			
			for (int i = 0; i < menuscount; i++) {
				View v = inflater.inflate(R.layout.pop_up_menu, null);
				TextViewCustom txtView = (TextViewCustom)v.findViewById(R.id.txtMenuTitle);
				ImageView imageIcon = (ImageView)v.findViewById(R.id.imgMenuIcon);
				if(Friendid==0)
				{
				txtView.setText(menuTitle[i]);
				imageIcon.setImageResource(res[i]);
				}
				else {
					txtView.setText(menuTitle1[i]);
					imageIcon.setImageResource(res2[i]);
				}
				menuLayout.addView(v);
				v.setTag(i+1);
				v.setOnClickListener(onMenuClick);
			}
			
			
		}else
		{
			String[] menuTitle ={"Block User","Add to Favorite","Profile Details"};
			String[] menuTitle1 = {"Block User","Profile Details"};
			int [] res = {R.drawable.btn_block_advisor,R.drawable.btn_add_favourite,R.drawable.btn_send_message};
			int [] res2 = {R.drawable.btn_block_advisor,R.drawable.btn_send_message};
			
			int menuscount = (Friendid==0)?3:2;
			LayoutInflater inflater = LayoutInflater.from(this);
			for (int i = 0; i < menuscount; i++) {
				View v = inflater.inflate(R.layout.pop_up_menu, null);
				TextViewCustom txtView = (TextViewCustom)v.findViewById(R.id.txtMenuTitle);
				ImageView imageIcon = (ImageView)v.findViewById(R.id.imgMenuIcon);
				if(Friendid==0)
				{
				txtView.setText(menuTitle[i]);
				imageIcon.setImageResource(res[i]);
				}
				else 
				{
					txtView.setText(menuTitle1[i]);
					imageIcon.setImageResource(res2[i]);
				}
				
				
				menuLayout.addView(v);
				v.setTag(i+1);
				v.setOnClickListener(onMenuClick);
			}
		}
		
		
		
		inflater = LayoutInflater.from(this);
		id = (isAdvisor)?hashUser.get("userid"):hashUser.get("advisorid");
		Utility.setBooleanprefence(mContext, "chatwindow_"+id, true);
		dbAdapter.deleteChatsOfuser(id);
		LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("updateFav"));
		edtTxtMessage = (EditText) findViewById(R.id.editTexttest);
		
		((TextViewCustom)findViewById(R.id.textViewChattitle)).setText(hashUser.get("firstname"));
		
		edtTxtMessage.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
			
				if (hasFocus) {
					scrollChats.post(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							scrollChats.scrollTo(0, scrollChats.getTop());
						}
					});
				}
			}
		});
		arrayListChat = new ArrayList<String>();
		arrayListDemo = new ArrayList<HashMap<String, String>>();

		chatList = (ListView) findViewById(R.id.listViewtest);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub

//			
//				
//				if (hashUser.get("photopath") != null)
//					if (hashUser.get("photopath").contains("http")
//							|| hashUser.get("photopath").contains("https")) {
//						url = hashUser.get("photopath");
//					} else {
//						url = Utility.IMAGEURL + hashUser.get("photopath");
//					}
//				leftUser = Utility.getBitmap(url);
							
				String url = null;
				String Map = Utility.getUserPrefernce(mContext, "userprofileinfo");
				JSONObject json;
				HashMap<String, String> Infomap = null;
				try {
					json = new JSONObject(Map);
					Infomap = Utility.toMap(json);
					Log.e("Info Map data in profile view class", "" + Infomap);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (Infomap!=null) {
					
					if (Infomap.get("photopath") != null)
						if (Infomap.get("photopath").contains("http")
								|| Infomap.get("photopath").contains("https")) {
							url = Infomap.get("photopath");
						} else {
							url = Utility.IMAGEURL + Infomap.get("photopath");
						}

					rightUser = Utility.getBitmap(url);
				}
				
			}
		}).start();
		setDetail();
		
		new GetChatUsersList().execute();
		
		String userId=Utility.getUserPrefernce(mContext, "id");//124
		adapter = new ChatListAdapter(mContext, 0, R.layout.cell_match_user,
				arrayListChat, hashBitmap,userId);
		
		chatList.setAdapter(adapter);
	
		// Button Message send
		Button btnSend = (Button) findViewById(R.id.btn_test);
		btnSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				message = edtTxtMessage.getText().toString().trim();
				edtTxtMessage.setText("");
				if (message.length() == 0) {
					Utility.showAlert(mContext, "Alert",
							"Message can't be empty!!!");
					return;
				}
				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss +a");
		//		df.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"));
				String date  = df1.format(new Date());
				String formattedDate = df.format(new Date());
				String strMessage = ""+Utility.getUserPrefernce(mContext, "id")+"Δ";
				strMessage = strMessage + ""+Utility.getUserPrefernce(mContext, "fname")+"Δ";
				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
				if(isAdvisor){
					strMessage = strMessage+""+hashUser.get("userid")+"Δ";
				}
				else{
					strMessage = strMessage+""+hashUser.get("advisorid")+"Δ";
				}
				strMessage  = strMessage+""+message+"Δ";
				strMessage = strMessage+""+formattedDate;//Time
				strMessage = strMessage+"Δ"+date;// Date
				strMessage = strMessage+"Δ"+Utility.getUserPrefernce(mContext, "usertype");
				addMessageToController(strMessage);
			
				
				//arrayListChat.add(strMessage);
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("sendmsg").putExtra("msg", strMessage));
				
				//adapter.notifyDataSetChanged();
				scrollChats.post(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//scrollChats.scrollTo(0, scrollChats.getTop());
						scrollChats.fullScroll(View.FOCUS_DOWN);
					}
				});
				//chatList.setSelection(adapter.getCount() - 1);
				
			}
		});
		
	
		LocalBroadcastManager.getInstance(mContext).registerReceiver(msgreceiver, new IntentFilter("receivemsg"));
		
		LocalBroadcastManager.getInstance(this).registerReceiver(updatesearchChat,
				new IntentFilter("updatsearchChat"));
	}
	
private OnClickListener onMenuClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int tag = (Integer)v.getTag();
			menuLayout.setVisibility(View.GONE);
			switch (tag) {
			case 1:
				new MarkAndUnmarkAsFavorite().execute(false);
				break;
			case 2:
				if(Friendid==0)
				{
					new MarkAndUnmarkAsFavorite().execute(true);
				}else
				{
					if (!isAdvisor) {
						// Write review
						Intent intent = new Intent(mContext, UserRatingActivity.class);
						intent.putExtra("advisorid", hashUser.get("advisorid"));
						startActivity(intent);
					}else
					{
						// Profile Detail
						Intent intent = new Intent(mContext, OtherUserProfileActivityView.class);
						int requestfor = (hashUser.get("userid")!=null)?Constants.USER:Constants.ADVISOR;
						intent.putExtra("requestFor", requestfor);
						intent.putExtra("broadcast", "updatsearchChat");
						intent.putExtra("detail", hashUser);
						startActivity(intent);
					}
				}
					
				break;
			case 3:
				if(Friendid==0)
				{
					if (!isAdvisor) {
						// Write review
						Intent intent = new Intent(mContext, UserRatingActivity.class);
						intent.putExtra("advisorid", hashUser.get("advisorid"));
						startActivity(intent);
					}else
					{
						//  Profile Details
						Intent intent = new Intent(mContext, OtherUserProfileActivityView.class);
						int requestfor = (hashUser.get("userid")!=null)?Constants.USER:Constants.ADVISOR;
						intent.putExtra("requestFor", requestfor);
						intent.putExtra("broadcast", "updatsearchChat");
						intent.putExtra("from", Constants.FAVOURITE);
						intent.putExtra("detail", hashUser);
						startActivity(intent);
					}
				}else
				{
					if (!isAdvisor) {
						//Profile detail
						Intent intent = new Intent(mContext, OtherUserProfileActivityView.class);
						int requestfor = (hashUser.get("userid")!=null)?Constants.USER:Constants.ADVISOR;
						intent.putExtra("requestFor", requestfor);
						intent.putExtra("broadcast", "updatsearchChat");
						intent.putExtra("from", Constants.FAVOURITE);
						intent.putExtra("detail", hashUser);
						startActivity(intent);
					}
				}
				break;
			case 4:
				if (!isAdvisor) {
					//Profile detail
					Intent intent = new Intent(mContext, OtherUserProfileActivityView.class);
					int requestfor = (hashUser.get("userid")!=null)?Constants.USER:Constants.ADVISOR;
					intent.putExtra("requestFor", requestfor);
					intent.putExtra("broadcast", "updatsearchChat");
					intent.putExtra("from", Constants.FAVOURITE);
					intent.putExtra("detail", hashUser);
					startActivity(intent);
				}
				break;
	

			default:
				break;
			}
		}
	};
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		
	};
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Utility.setBooleanprefence(mContext, "chatwindow_"+id, false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		Utility.setBooleanprefence(mContext, "chatloaded", true);
	}
	private void setDetail()
	{
		bitmap = Utility.getBitmap();//(Bitmap) this.getIntent().getParcelableExtra("image");
		if(rightUser==null)
		rightUser = BitmapFactory.decodeResource(getResources(), R.drawable.dummy);
		if(bitmap==null)
			bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dummy);
		TextView txtTitel = (TextView) findViewById(R.id.textViewChattitle);
		txtTitel.setText(hashUser.get("fname"));
	
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			new Thread(downloadpic).start();
		}
		((ImageButton) findViewById(R.id.btnInfo))
				.setOnClickListener(onclickListner);
		((ImageButton) findViewById(R.id.btn_BackProfile))
				.setOnClickListener(onclickListner);
	}
	
	BroadcastReceiver msgreceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			final String msg = intent.getStringExtra("msg");
			runOnUiThread(new Runnable() {
				public void run() {
					Log.d("Chat", ""+msg);
					String[] msgchat = msg.split("said");
					addMessageToController(msgchat[1]);
					scrollChats.post(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							scrollChats.fullScroll(View.FOCUS_DOWN);
						}
					});
//					arrayListChat.add(msgchat[1]);
//					adapter.notifyDataSetChanged();
//					chatList.setSelection(adapter.getCount() - 1);
				}
			});
		}
	};
	
	private LayoutInflater inflater;
	
private void addMessageToController(String Message)
{
	
	String [] seprated = Message.split("Δ");
	String userid = Utility.getUserPrefernce(mContext, "id");
 	View convertView = inflater.inflate(R.layout.cell_chats_user, null);
 	TextViewCustom txtViewChat, txtViewChatRight;
	RoundedImageView imageUserLeft;
	RelativeLayout layoutBgChat,layoutBgChatRight, layoutRightChat, layoutLeftChat;
	ImageView imageChaticon, imageChaticonRight;
	
 	layoutLeftChat = (RelativeLayout)convertView.findViewById(R.id.layoutLeftbubble);
	layoutRightChat = (RelativeLayout)convertView.findViewById(R.id.layoutRighttbubble);
	txtViewChatRight = (TextViewCustom) convertView
			.findViewById(R.id.textViewNameRight);
	imageUserLeft = (RoundedImageView)convertView.findViewById(R.id.imageViewPic);
	RoundedImageView rightImageView = (RoundedImageView)convertView.findViewById(R.id.imageViewPicRight);
	
	imageUserLeft.setScaleType(ScaleType.CENTER_CROP);
	imageUserLeft.setCornerRadius(20);
	imageUserLeft.setBorderWidth(2);
	imageUserLeft.setBorderColor(Color.DKGRAY);
	imageUserLeft.setRoundBackground(true);
	imageUserLeft.setImageBitmap(bitmap);
	
	rightImageView.setScaleType(ScaleType.CENTER_CROP);
	rightImageView.setCornerRadius(20);
	rightImageView.setBorderWidth(2);
	rightImageView.setBorderColor(Color.DKGRAY);
	rightImageView.setRoundBackground(true);
	if(rightUser==null){
		
		rightImageView.setImageResource(R.drawable.dummy);
	}else
	{
		rightImageView.setImageBitmap(rightUser);
	}

	
	
	

	//holder.txtViewSenderinfo = (TextViewCustom) convertView
			//.findViewById(R.id.textViewtimeRight);
	layoutBgChatRight = (RelativeLayout) convertView
			.findViewById(R.id.chatbglayoutRight);
	imageChaticonRight = (ImageView) convertView
			.findViewById(R.id.imageViewPicRight);
	txtViewChat = (TextViewCustom) convertView
				.findViewById(R.id.textViewName);
   layoutBgChat = (RelativeLayout) convertView
				.findViewById(R.id.chatbglayout);
   imageChaticon = (ImageView) convertView
				.findViewById(R.id.imageViewPic);
   
   if(seprated[0].equalsIgnoreCase(userid))
	{
		layoutLeftChat.setVisibility(View.GONE);
		layoutRightChat.setVisibility(View.VISIBLE);
		if (seprated[3] != null)
			txtViewChatRight.setText(seprated[3]);
		else
			txtViewChatRight.setText("Annonymous");
		
	}else
	{
		layoutLeftChat.setVisibility(View.VISIBLE);
		layoutRightChat.setVisibility(View.GONE);
		if (seprated[3] != null)
			txtViewChat.setText(seprated[3]);
		else
			txtViewChat.setText("Annonymous");
	}



layoutChat.addView(convertView);

}
	

class ViewHolder {

	TextViewCustom txtViewChat, txtViewChatRight;
	RoundedImageView imageUserLeft, imageUserRight;
	RelativeLayout layoutBgChat,layoutBgChatRight, layoutRightChat, layoutLeftChat;
	ImageView imageChaticon, imageChaticonRight;

}
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		// TODO Auto-generated method stub
		int targetWidth = 250;
		int targetHeight = 250;
		 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
		                        targetHeight,Bitmap.Config.ARGB_8888);

		            Canvas canvas = new Canvas(targetBitmap);
		 Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
		((float) targetHeight - 1) / 2,
		(Math.min(((float) targetWidth), 
		            ((float) targetHeight)) / 2),
		Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, 
		                            new Rect(0, 0, sourceBitmap.getWidth(),
		sourceBitmap.getHeight()), 
		                            new Rect(0, 0, targetWidth,
		targetHeight), null);
		return targetBitmap;
		}
	
private BroadcastReceiver updatesearchChat = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String type  = intent.getStringExtra("type");
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) intent.getSerializableExtra("details");
			if (type.equalsIgnoreCase("detail")) {
				
				hashUser.putAll(map);
			}
			if(type.equalsIgnoreCase("block")){
				finish();
			}
			
			
		}
	};
	

	private OnClickListener onclickListner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			if (id == R.id.btnInfo) {
				if(menuLayout.getVisibility()==View.VISIBLE)
				{
					Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.hide_menu);
				//	menuLayout.startAnimation(anim);
					menuLayout.setVisibility(View.GONE);
					
				}else
				{
					Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.show_menu);
					//menuLayout.startAnimation(anim);
					menuLayout.setVisibility(View.VISIBLE);
				}
			}
			if (id == R.id.btn_BackProfile) {
				onBackPressed();
			}
			
		}
	};
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};

	// Download user image
		private Runnable downloadpic = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				String url = null;
				if (hashUser.get("photopath").contains("http")
						|| hashUser.get("photopath").contains("https")) {
					url = hashUser.get("photopath");
				} else {
					url = Utility.IMAGEURL + hashUser.get("photopath");
				}
				bitmap = Utility.getBitmap(url);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if(bitmap!=null)
							imageView.setImageBitmap(getRoundedShape(bitmap));
					}
				});
			}
		};
		
		
		// Get User Chat History
	public class GetChatUsersList extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			isLoading = true;

			if (!isbackground) {
				
				mProgressDialog = ProgressDialog.show(mContext, "Getting chat history", "Please wait...", true, true, new DialogInterface.OnCancelListener() {
					
					@Override
					public void onCancel(DialogInterface dialog) {
						// TODO Auto-generated method stub
						GetChatUsersList.this.cancel(true);
					}
				});
			}
			
		
			
			arrayListChat.clear();
			
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			String serverUrl = Utility.SERVERURL + "GetChatHistory";
			
			if(isAdvisor)
			{
				serverUrl = serverUrl + "&advisorid="+Utility.getUserPrefernce(mContext, "id")+"&userid="+hashUser.get("userid")+"&usertype=advisor";
			}else
			{
				serverUrl = serverUrl + "&userid="+Utility.getUserPrefernce(mContext, "id")+"&advisorid="+hashUser.get("advisorid")+"&usertype=user";	
			}
			String response = Utility.getJSONFromUrl(serverUrl);
			if (response != null) {

				try {
					JSONObject jsonObject = new JSONObject(response);
					JSONArray jsonArray = jsonObject.getJSONArray("chats");
					
					
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject objectJson = (JSONObject) jsonArray.get(i);
						objectJson = objectJson.getJSONObject("chat");
						HashMap<String, String> map = Utility.toMap(objectJson);
						arrayListDemo.add(map);

					}

					response = "Success";
				} catch (Exception e) {
					response = "Failed";
					e.printStackTrace();
					return response;
				}

			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			if (mProgressDialog != null)
				mProgressDialog.cancel();
				isServiceRuning = false;

			if (!isbackground) {
				//startTimer();
				isbackground = true;
			}
			
			if (result != null) {
				if (result.contains("Failed")) {

				}
				if (result.contains("Success")) {
					
					if (arrayListDemo.size() > 0
							&& arrayListDemo.size() > previousCount) {
						arrayListChat.clear();
						for (HashMap<String, String> map : arrayListDemo) {
							String messages = map.get("message");
							String [] message = messages.split("\n");
							for(int i=0;i<message.length;i++)
							{
								addMessageToController(message[i]);
							}
								//arrayListChat.add(message[i]);
						}
						scrollChats.post(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								//scrollChats.scrollTo(0, scrollChats.getTop());
								scrollChats.fullScroll(View.FOCUS_DOWN);
							}
						});
						adapter.notifyDataSetChanged();
						chatList.setSelection(adapter.getCount() - 1);// Scroll at last index
						previousCount = arrayListDemo.size();
						//chatListLayout.setVisibility(View.VISIBLE);
						adapter.notifyDataSetChanged();
						
					} else {
						arrayListChat.clear();
						//if (!isbackground)
							//Utility.showAlert(mContext, "Alert", "No record found.");
					}
				}
			} else {
				if (!isbackground)
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.nonetwork));
			}

		}
	}

		
	// MarkAndUnmark as favorite
	private class MarkAndUnmarkAsFavorite extends AsyncTask<Boolean, Void, String>
	{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(Boolean... isFavorite) {
			// TODO Auto-generated method stub
			String response = null;
			if(isFavorite[0])
			{
				String url = Utility.SERVERURL + "MarkAsFavorites";
				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					listParams.add(new BasicNameValuePair("userid", hashUser.get("userid")));
					listParams.add(new BasicNameValuePair("advisorid", Utility
							.getUserPrefernce(mContext, "id")));
				} else {
					listParams.add(new BasicNameValuePair("advisorid",
							hashUser.get("advisorid")));
					listParams.add(new BasicNameValuePair("userid", Utility
							.getUserPrefernce(mContext, "id")));
				}

				response = Utility
						.postParamsAndfindJSON(url, listParams);
				
			}else
			{
				String url = Utility.SERVERURL + "MarkAsBlocked";
				//url = url + "&senderid="
					//	+ Utility.getUserPrefernce(mContext, "id");
				
				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
				listParams.add(new BasicNameValuePair("senderid", Utility.getUserPrefernce(mContext, "id")));
				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					listParams.add(new BasicNameValuePair("senderusertype", "advisor"));
					listParams.add(new BasicNameValuePair("blockeduserid", hashUser.get("userid")));
				} else {
					
					//url = url + "&senderusertype=user&blockeduserid="
						//	+ hashUser.get("advisorid");
					
					listParams.add(new BasicNameValuePair("senderusertype", "user"));
					listParams.add(new BasicNameValuePair("blockeduserid",hashUser.get("advisorid")));
				}
				response = Utility
						.postParamsAndfindJSON(url, listParams);;
			}
			return response;//http://projects.linkites.com/retirely/api/v1/api.php?method=MarkAsBlocked&senderid=124&senderusertype=user&blockeduserid=76
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result!=null) {
				Toast.makeText(mContext, ""+result, Toast.LENGTH_LONG).show();
				if(result.contains("0"))
				{
					Utility.showAlert(mContext, "Alert", "Unable to process, please try again.");
				}else
				{
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("updateFav"));
				}
			}else
			{
				Utility.showAlert(mContext, "Alert", "Please check your internet connection.");
			}
		}
	}

	

	
}
