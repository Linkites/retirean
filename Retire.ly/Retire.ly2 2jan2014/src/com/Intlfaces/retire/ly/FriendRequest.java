package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.google.code.linkedinapi.schema.Connections;
import com.google.code.linkedinapi.schema.Person;
import com.linkedinn.Config;
import com.linkedinn.LinkedinDialog;
import com.linkedinn.LinkedinDialog.OnVerifyListener;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.ContactDetails;
import com.retirely.adapters.FriendRequestAdapter;

import facebook.BaseRequestListener;
import facebook.SessionEvents;
import facebook.SessionEvents.AuthListener;

public class FriendRequest extends Activity {

	private AsyncFacebookRunner mAsyncRunner;
	private Facebook facebook;
	private static String APP_ID = "415089575266346";// Replace it with your app id
	JSONArray friends=null;
	JSONObject json;
	ArrayList<ContactDetails> contactList = new ArrayList<ContactDetails>();
	ArrayList<ContactDetails> fbFriendList = new ArrayList<ContactDetails>();
	ArrayList<ContactDetails> linkedInFriendList = new ArrayList<ContactDetails>();
	FriendRequestAdapter contactsAdapter;
	FriendRequestAdapter fbFriendsAdapter;
	FriendRequestAdapter linkedInFriendsAdapter;
	Button fbFriends; 
	Button linkedInFriends;
	ListView fbFriendsList;
	ListView linkedInFriendsList;
	SampleAuthListener fbauthlistener;
	ContactDetails contact;
    private TabHost tabHost;
    Context mContext;
    List<Person> connectionsList;
    boolean running = true;
    LinearLayout linearLayoutContact;
    LinearLayout contactUnderline;
    TextView contactTextView;
    LinearLayout linearLayoutFacebook;
    LinearLayout facebookUnderline;
    TextView facebookTextView;
    LinearLayout linearLayoutLinkedIn;
    LinearLayout linkedInUnderline;
    TextView linkedInTextView;
    HashMap<String, Bitmap> fbProfilepicMap;
    HashMap<String, Bitmap> linkedInProfilePicMap;
    
    final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
            .getInstance().createLinkedInOAuthService(
                    Config.LINKEDIN_CONSUMER_KEY,Config.LINKEDIN_CONSUMER_SECRET);
	final LinkedInApiClientFactory factory = LinkedInApiClientFactory
			.newInstance(Config.LINKEDIN_CONSUMER_KEY,
					Config.LINKEDIN_CONSUMER_SECRET);
	
	LinkedInRequestToken liToken;
	LinkedInApiClient client;
	LinkedInAccessToken accessToken = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_request);
		mContext=this;
		
		ImageButton backBtn = (ImageButton)findViewById(R.id.btn_BackHome);
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
		
		facebook = new Facebook(APP_ID);
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		fbauthlistener = new SampleAuthListener();
		SessionEvents.addAuthListener(fbauthlistener);
		
		//initialise the UI controls 
		tabHost=(TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();
        
        
        TabSpec spec1=tabHost.newTabSpec("Contacts");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Contacts");
        
        TabSpec spec2=tabHost.newTabSpec("Facebook Friends");
        spec2.setIndicator("Facebook");
        spec2.setContent(R.id.tab2);
        
        TabSpec spec3=tabHost.newTabSpec("LinkedIn Friends");
        spec3.setIndicator("LinkedIn");
        spec3.setContent(R.id.tab3);
        
        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.setBackgroundColor(Color.rgb(245, 243, 237));
        
        contactUnderline = (LinearLayout)findViewById(R.id.contactUnderline);
        contactTextView = (TextView)findViewById(R.id.contactTextView);
        facebookUnderline = (LinearLayout)findViewById(R.id.facebookUnderline);
        facebookTextView = (TextView)findViewById(R.id.facebookTextView);
        linkedInUnderline = (LinearLayout)findViewById(R.id.linkedInUnderline);
        linkedInTextView = (TextView)findViewById(R.id.linkedInTextView);
        linearLayoutContact = (LinearLayout)findViewById(R.id.contact);
        linearLayoutContact.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tabHost.setCurrentTab(0);
				contactUnderline.setBackgroundColor(Color.rgb(30, 144, 255));
				contactTextView.setTypeface(Typeface.DEFAULT_BOLD);
				
				facebookUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				facebookTextView.setTypeface(Typeface.DEFAULT);
				
				linkedInUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				linkedInTextView.setTypeface(Typeface.DEFAULT);
			}
		});
        linearLayoutFacebook = (LinearLayout)findViewById(R.id.facebook);
        linearLayoutFacebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tabHost.setCurrentTab(1);
				contactUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				contactTextView.setTypeface(Typeface.DEFAULT);
				
				facebookUnderline.setBackgroundColor(Color.rgb(30, 144, 255));
				facebookTextView.setTypeface(Typeface.DEFAULT_BOLD);
				
				linkedInUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				linkedInTextView.setTypeface(Typeface.DEFAULT);
			}
		});
        linearLayoutLinkedIn = (LinearLayout)findViewById(R.id.linkedin);
        linearLayoutLinkedIn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tabHost.setCurrentTab(2);
				contactUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				contactTextView.setTypeface(Typeface.DEFAULT);
				
				facebookUnderline.setBackgroundColor(Color.rgb(245, 243, 237));
				facebookTextView.setTypeface(Typeface.DEFAULT);
				
				linkedInUnderline.setBackgroundColor(Color.rgb(30, 144, 255));
				linkedInTextView.setTypeface(Typeface.DEFAULT_BOLD);
			}
		});
		        
	    contactsAdapter = new FriendRequestAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, contactList,1);
	    ListView contactsList = (ListView)findViewById(R.id.contactList);
	    contactsList.setAdapter(contactsAdapter);

	    fbProfilepicMap = new HashMap<String, Bitmap>();
	    linkedInProfilePicMap = new HashMap<String, Bitmap>();	
	    
	    fbFriendsList = (ListView)findViewById(R.id.facebookList);
		fbFriendsAdapter =  new FriendRequestAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, fbFriendList,2, fbProfilepicMap);
		fbFriendsList.setAdapter(fbFriendsAdapter);
	    linkedInFriendsList = (ListView) findViewById(R.id.linkedInList);
		linkedInFriendsAdapter =  new FriendRequestAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, linkedInFriendList,3, linkedInProfilePicMap);
		linkedInFriendsList.setAdapter(linkedInFriendsAdapter);
		
	    fbFriends = (Button)findViewById(R.id.fbFriends);
	    fbFriends.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginToFacebook();
			}
		});
	    linkedInFriends = (Button)findViewById(R.id.inFriends);
	    linkedInFriends.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				linkedInLogin();
				Log.e("linkedInLogin", "Logging in");
			}
		});
	    
	    Button btn_send = (Button) findViewById(R.id.btn_send);
	    btn_send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int currentTab = tabHost.getCurrentTab();
				if(currentTab==0){
					String id="";
					for(int i=0; i<contactList.size();i++){
						if(contactList.get(i).isChecked){
							if(id.equals("")){
								id=contactList.get(i).id;
							}
							else{
								id=id+","+contactList.get(i).id;
							}
						}
						if(!id.trim().equals("")){
							Log.e("ID", id);
							Intent sendemail = new Intent(Intent.ACTION_SEND);
							sendemail.putExtra(Intent.EXTRA_EMAIL, id.split(","));
							sendemail.putExtra(Intent.EXTRA_SUBJECT, "Invitation to join retire.ly");
							sendemail.putExtra(Intent.EXTRA_TEXT, "hi, Join Retire.ly");
							sendemail.setType("message/rfc822");
							startActivity(Intent.createChooser(sendemail, "Choose an Email client"));
						}
					}
				}
				else if(currentTab==1){
					String id="";
					for(int i=0; i<fbFriendList.size();i++){
						if(fbFriendList.get(i).isChecked){
							if(id.equals("")){
								id=fbFriendList.get(i).id;
							}
							else{
								id=id+","+fbFriendList.get(i).id;
							}
						}
					}
					if(!id.trim().equals("")){
						Log.e("ID", id);
						Bundle params = new Bundle();
						params.putString("message", "hi friend, join retire.ly");					
						params.putString("title", "Invite Friends");
						params.putString("to", id);
						facebook.dialog(mContext, "apprequests", params, new DialogListener() {
			                @Override
			                public void onComplete(Bundle values) {}
	
			                @Override
			                public void onFacebookError(FacebookError error) {}
	
			                @Override
			                public void onError(DialogError e) {}
	
			                @Override
			                public void onCancel() {}
	
			                public void onComplete(String response, Object state)
			                {
			                    try
			                    {
			                        JSONObject eventResponse = new JSONObject(response);
			                        //event_id = event.getString("id");
			                        Log.i("ee", "Event Response => "+eventResponse);
	
			                    }
			                    catch (Exception e)
			                    {
	
			                    }
			                }
			            });
					}
				}
				else {
					
					List<String> id = new LinkedList<String>();
					for(int i=0; i<linkedInFriendList.size();i++){
						if(linkedInFriendList.get(i).isChecked){
							Log.e("LinkedInSend", "sending messages to "+i);
							id.add(linkedInFriendList.get(i).id);							
						}
					}
					if(id.size()>0)
						client.sendMessage(id, "Subject", "sorry i am just checking the android implementation of linkedin api");
				}
			}
		});
	    LocalBroadcastManager.getInstance(this).registerReceiver(Notifier, new IntentFilter("doThisWork"));
	}
	
	BroadcastReceiver Notifier = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String RequestFor = intent.getStringExtra("RequestFor");			
			if(RequestFor.equals("contacts")){
				int position = Integer.parseInt(intent.getStringExtra("position").trim());			
				boolean isChecked = Boolean.parseBoolean(intent.getStringExtra("isChecked").trim());
				ContactDetails cd=contactList.get(position);
				cd.isChecked=isChecked;
//				contactList.remove(position);
//				contactList.add(position,cd);
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						contactsAdapter.notifyDataSetChanged();
					}
				});
			}
			else if(RequestFor.equals("facebook")){
				int position = Integer.parseInt(intent.getStringExtra("position").trim());			
				boolean isChecked = Boolean.parseBoolean(intent.getStringExtra("isChecked").trim());
				ContactDetails cd = fbFriendList.get(position);
				cd.isChecked=isChecked;				
//				fbFriendList.remove(position);
//				fbFriendList.add(position,cd);
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						fbFriendsAdapter.notifyDataSetChanged();
					}
				});
			}
			else if(RequestFor.equals("linkedin")){
				int position = Integer.parseInt(intent.getStringExtra("position").trim());			
				boolean isChecked = Boolean.parseBoolean(intent.getStringExtra("isChecked").trim());				
				ContactDetails cd = linkedInFriendList.get(position);
				cd.isChecked=isChecked;
//				linkedInFriendList.remove(position);
//				linkedInFriendList.add(position,cd);
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						linkedInFriendsAdapter.notifyDataSetChanged();
					}
				});
			}
		}
	};
	
	Runnable loadContacts = new Runnable() {
		public void run() {
			Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
			String photoUrl="";
		    String id;
		    c.moveToFirst();
		    for (int i = 0; i < c.getCount(); i++) {
		    	contact = new ContactDetails();
		        contact.name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		        id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));	        
		        photoUrl = c.getString(c.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
		        contact.photoUri = photoUrl;	        
		        Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[] { id }, null);
	            if (emailCur.moveToNext()) {
	                // This would allow you get several email <span class="IL_AD" id="IL_AD9">addresses</span> if the email addresses were stored in an array
	            	contact.id = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
	            }
	            emailCur.close();		  
		        c.moveToNext();
		        if(contact.id!=null && !contact.id.equals(""))
		        	contactList.add(contact);
	    	}
		    runOnUiThread(new Thread(){
		    	public void run(){
		    		contactsAdapter.notifyDataSetChanged();
		    	}
		    });
		}
	};
	
	public void loginToFacebook() {
		if (!facebook.isSessionValid()) {
			facebook.authorize(FriendRequest.this, new String[] {"email", "publish_stream","user_birthday","user_education_history", "user_work_history","user_location", "user_friends", "manage_friendlists"}, new DialogListener() {
				@Override
				public void onCancel() {
					// Function to handle cancel event
				}
				@Override
				public void onComplete(Bundle values) {
					// Function to handle complete event
					// Edit Preferences and update facebook acess_token
					SessionEvents.onLoginSuccess();
				}
				// Function to handle error
				@Override
				public void onError(DialogError error) {
					Log.e("Error", "Internet Connection Erro");
				}
				// Function to handle Facebook errors
				@Override
				public void onFacebookError(FacebookError fberror) {
					Log.e("Error", "FaceBook Erro");
				}
			});
		}
	}
	
	private class SampleAuthListener implements AuthListener {
		public void onAuthSucceed() {
			Bundle bundle = new Bundle();	
			bundle.putString("fields", "id,name,picture");
		    mAsyncRunner.request("me/friends", bundle, new FriendListRequestListener());	
		}

		public void onAuthFail(String error) {
			Log.e("Error", "Login Failed");
		}
	}
	
	class FriendListRequestListener extends BaseRequestListener {
		String _error="";
	    public void onComplete(String response) {
	        _error = null;
	        try {
	            json = Util.parseJson(response);
	            friends = json.getJSONArray("data");
	            System.out.println(json.toString());
	            FriendRequest.this.runOnUiThread(new Runnable() {
	                public void run() {
	                	ContactDetails fbcontact=null;
	                    for(int i=0;i<friends.length();i++){
	                    	if(i==0){
	                    		fbFriends.setVisibility(View.GONE);
	                    		fbFriendsList.setVisibility(View.VISIBLE);
	                    	}
	                    	try {
								json = friends.getJSONObject(i);
								fbcontact = new ContactDetails();
								fbcontact.name = json.getString("name");
								fbcontact.id = json.getString("id");
								fbcontact.photoUri = json.getJSONObject("picture").getJSONObject("data").getString("url");
								fbFriendList.add(fbcontact);
								if(i==friends.length()-1){
									fbFriendsAdapter.notifyDataSetChanged();
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
	                    }
	                    fbProfilePicsDownload.start();
	                }
	            });

	        } catch (JSONException e) {
	            _error = "JSON Error in response";
	        } catch (FacebookError e) {
	            _error = "Facebook Error: " + e.getMessage();
	        }

	        if (_error != null)
	        {
	            FriendRequest.this.runOnUiThread(new Runnable() {
	                public void run() {
	                	Log.e("Error", _error);
	                }
	            });
	        }
	    }
		@Override
		public void onComplete(String response, Object state) {			
			onComplete(response);
		}
	}

	
	Thread fbProfilePicsDownload = new Thread(){
		public void run(){
			ContactDetails cd;
			Bitmap bmp;
			for(int i=0;i<fbFriendList.size() && running;i++){
				cd	= fbFriendList.get(i);
				if(cd.photoUri!=null && !cd.photoUri.equals("")){
					bmp = Utility.getBitmap(cd.photoUri);
					fbProfilepicMap.put(cd.id, bmp);
				}
				runOnUiThread(new Thread(){
					public void run(){
						fbFriendsAdapter.notifyDataSetChanged();
					}
				});
			}
		}
	};
	
	
	Thread linkedInProfilePicsDownload = new Thread(){
		public void run(){
			ContactDetails cd;
			Bitmap bmp;
			for(int i=0;i<linkedInFriendList.size() && running;i++){
				cd	= linkedInFriendList.get(i);
				if(cd.photoUri!=null && !cd.photoUri.equals("")){
					bmp = Utility.getBitmap(cd.photoUri);
					linkedInProfilePicMap.put(cd.id, bmp);
				}
				runOnUiThread(new Thread(){
					public void run(){
						linkedInFriendsAdapter.notifyDataSetChanged();
					}
				});
			}
		}
	};

	private void linkedInLogin() {
		Log.e("linkedInLogin", "Login called");
		ProgressDialog progressDialog = new ProgressDialog(FriendRequest.this);
		LinkedinDialog d = new LinkedinDialog(mContext, progressDialog);
		d.show();
		Log.e("linkedInLogin", "dialog shown");
		// set call back listener to get oauth_verifier value
		d.setVerifierListener(new OnVerifyListener() {
			@Override
			public void onVerify(String verifier) {
				try {
					Log.i("LinkedinSample", "verifier: " + verifier);

					accessToken = LinkedinDialog.oAuthService
							.getOAuthAccessToken(LinkedinDialog.liToken,
									verifier);
					LinkedinDialog.factory.createLinkedInApiClient(accessToken);
					client = factory.createLinkedInApiClient(accessToken);
					// client.postNetworkUpdate("Testing by Mukesh!!! LinkedIn wall post from Android app");
					Log.i("LinkedinSample",
							"ln_access_token: " + accessToken.getToken());
					Log.i("LinkedinSample",
							"ln_access_token: " + accessToken.getTokenSecret());
				
					final Set<ProfileField> connectionFields = EnumSet.of(ProfileField.ID, ProfileField.MAIN_ADDRESS,
			                ProfileField.PHONE_NUMBERS, ProfileField.LOCATION,
			                ProfileField.LOCATION_COUNTRY, ProfileField.LOCATION_NAME,
			                ProfileField.FIRST_NAME, ProfileField.LAST_NAME, ProfileField.HEADLINE,
			                ProfileField.INDUSTRY, ProfileField.CURRENT_STATUS,
			                ProfileField.CURRENT_STATUS_TIMESTAMP, ProfileField.API_STANDARD_PROFILE_REQUEST,
			                ProfileField.EDUCATIONS, ProfileField.PUBLIC_PROFILE_URL, ProfileField.POSITIONS,
			                ProfileField.LOCATION, ProfileField.PICTURE_URL);
			        Connections connections = client.getConnectionsForCurrentUser(connectionFields);
			        
					connectionsList = connections.getPersonList();
					
					FriendRequest.this.runOnUiThread(new Runnable() {
		                public void run() {
		                	Person person;
							ContactDetails lnContact;               	
		                    for(int i=0;i<connectionsList.size();i++){
		                    	if(i==0){
		                    		linkedInFriends.setVisibility(View.GONE);
		                    		linkedInFriendsList.setVisibility(View.VISIBLE);
		                    	}
					        	lnContact = new ContactDetails();
					        	person = connectionsList.get(i);
					        	lnContact.id = person.getId();
					        	String name="";
					        	if(person.getFirstName()!=null)
					        		name+=person.getFirstName();
					        	if(person.getLastName()!=null){
					        		name = name+" "+person.getLastName();
					        	}
					        	lnContact.name=name;
					        	lnContact.photoUri = person.getPictureUrl();
					        	linkedInFriendList.add(lnContact);
		                    }
		                    linkedInFriendsAdapter.notifyDataSetChanged();
		                    //linkedInProfilePicsDownload.start();
		                }
		            });	
				} catch (Exception e) {
					Log.i("LinkedinSample", "error to get verifier");
					e.printStackTrace();
				}
			}
		});

		// set progress dialog
		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(true);
		progressDialog.show();
		progressDialog.setCanceledOnTouchOutside(false);
	}

	public void onDestroy(){
		running= false;
		LocalBroadcastManager.getInstance(this).unregisterReceiver(Notifier);
		SessionEvents.removeAuthListener(fbauthlistener);
		super.onDestroy();
	}
}

