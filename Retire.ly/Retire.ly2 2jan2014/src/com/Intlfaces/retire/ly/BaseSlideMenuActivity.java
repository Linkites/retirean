package com.Intlfaces.retire.ly;

import android.app.FragmentManager;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.aretha.slidemenu.SlideMenu;
import com.aretha.slidemenu.SlideMenu.LayoutParams;
import com.linkites.retire.utility.Utility;

public class BaseSlideMenuActivity extends FragmentActivity {
	private SlideMenu mSlideMenu;
	Context mContext;
	FragmentManager activityManager;
	@Override
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_menu);
		mContext = getApplicationContext();
		
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
		mSlideMenu = (SlideMenu) findViewById(R.id.slideMenu);
		
		
	}

	public void setSlideRole(int res) {
		if (null == mSlideMenu) {
			return;
		}

		getLayoutInflater().inflate(res, mSlideMenu, true);
	}

	public SlideMenu getSlideMenu() {
		return mSlideMenu;
	}
}
