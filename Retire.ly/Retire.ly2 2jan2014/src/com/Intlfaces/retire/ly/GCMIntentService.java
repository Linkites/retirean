package com.Intlfaces.retire.ly;



import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.RetirelyDbAdapter;



public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(Utility.SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        Utility.displayMessage(context, "Your device registred with GCM");
       // Log.d("NAME", MainActivity.name);
        //ServerUtilities.register(context, "Android","Android", registrationId);
        Utility.setUserPrefernce(context, "token", registrationId);
    }

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        //displayMessage(context, getString(R.string.gcm_unregistered));
       // ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
         String message = intent.getExtras().getString("message");
        
        Utility.displayMessage(context, message);
        // notifies user
        generateNotification(context, intent.getExtras());
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        //String message = getString(R.string.gcm_deleted, total);
        //displayMessage(context, message);
        // notifies user
        //generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        //displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        //displayMessage(context, getString(R.string.gcm_recoverable_error,
               // errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, Bundle extra) {
        int icon = R.drawable.app_icon;
        RetirelyDbAdapter dbAdapter = new RetirelyDbAdapter(context);
        String messagechat = extra.getString("custmsg");
     if(messagechat!=null)
       {
        	
			String [] strsubmg = messagechat.split("Δ");
	
			if(!Utility.getBooleanPrefernce(context, "chatwindow_"+strsubmg[0].replace("\"", "").trim()))
			{
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("userid", strsubmg[0].replace("\"", "").trim());
				map.put("message", messagechat);
				dbAdapter.insertChatRecord(map);
				LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("updateFav"));
			}
			
		}
     String message = extra.getString("message");
     
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        Notification notification = new Notification(icon, message, when);
       
        String title = context.getString(R.string.app_name);
        
        Intent notificationIntent = new Intent(context, SignInActivity.class);
        
       // notificationIntent.putExtra("requestFor", Constant.NOTIFICATION);
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put("category_name", json.getString("title"));
//        map.put("id", json.getString("CategoryID"));
        notificationIntent.putExtra("data", message);
        LocalBroadcastManager.getInstance(context)
		.sendBroadcast(new Intent("updateFav"));
       // notificationIntent.putExtra("id", json.getString("CategoryID"));
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        int id = Utility.getIntegerPrefrence(context, "notification");
        PendingIntent intent =
                PendingIntent.getActivity(context,id, notificationIntent, 0);
        id = id+1;
        Utility.setIntegerPrefrence(context, "notification", id);
        
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        
        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
        
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(id, notification);      
//        } catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }

}
