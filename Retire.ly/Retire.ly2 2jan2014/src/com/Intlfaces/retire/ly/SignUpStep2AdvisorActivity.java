package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.linkites.retire.utility.Utility;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class SignUpStep2AdvisorActivity extends Activity {
	private ProgressDialog mBProgressBar;
	private Context mContext;
	private Button btnSubmit;
	private RadioButton radiobtn_Agreement;
	private String userType = "", gender = "", Fname = "", Lname = "",
			Email = "", Password = "", Phone = "", photo = "", age = "",
			Firm = "", designation = "", university = "", proffBio = "",
			webAdd = "", officeAdd = "", file = "";
	private EditText editTextFirm, editTextDesignation, editTextUniversity,
			editTextProffBio, editWebAddress, editOfficeAddress;
	private ImageView btnCheck;
	byte[] imageByte = {};

	private int requestFor, advisorAge = 0;
	private final int BUY = 101;
	private double userLat, userLng;
	private static final int LOGOUT = 3;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up_step_2_advisor);
		mContext = this;

		Intent i = getIntent();

		btnCheck=(ImageView)findViewById(R.id.radiobtn_Agreement);
		btnCheck.setOnClickListener(onClickListener);
		btnSubmit = (Button) findViewById(R.id.btn_Submit);
		btnSubmit.setOnClickListener(onClickListener);
		((ImageButton) findViewById(R.id.btn_BackSignup))
				.setOnClickListener(onClickListener);
		((Button) findViewById(R.id.btnbck2))
				.setOnClickListener(onClickListener);
		editTextFirm = (EditText) findViewById(R.id.txtSFirm);
		// editTextFname.addTextChangedListener(txtWather);
		editTextFirm.setText(i.getStringExtra("firmname"));
		Firm = editTextFirm.getText().toString();

		editTextDesignation = (EditText) findViewById(R.id.txtSDesignation);
		// editTextLname.addTextChangedListener(txtWather);
		editTextDesignation.setText(i.getStringExtra("designation"));
		designation = editTextDesignation.getText().toString();

		editTextUniversity = (EditText) findViewById(R.id.txtSUniversity);
		// editTextEmail.addTextChangedListener(txtWather);
		editTextUniversity.setText(i.getStringExtra("university"));
		university = editTextUniversity.getText().toString();

		editTextProffBio = (EditText) findViewById(R.id.txtSProffBio);
		// editTextPass.addTextChangedListener(txtWather);
		editTextProffBio.setText(i.getStringExtra("proffBio"));
		proffBio = editTextProffBio.getText().toString();

		// editTextAge = (EditText) findViewById(R.id.txtSAge);
		// // editTextAge.addTextChangedListener(txtWather);
		// editTextAge.setOnFocusChangeListener(txtFocusListener);
		// editTextAge.setOnEditorActionListener(onEditActionListener);

		editWebAddress = (EditText) findViewById(R.id.txtSWebAAddress);
		// editTextFirm.addTextChangedListener(txtWather);
		editWebAddress.setText(i.getStringExtra("webAddress"));
		webAdd = editWebAddress.getText().toString();

		editOfficeAddress = (EditText) findViewById(R.id.txtSOfficeAddress);
		// editTextFirm.addTextChangedListener(txtWather);
		editOfficeAddress.setText(i.getStringExtra("officeAddress"));
		officeAdd = editOfficeAddress.getText().toString();

		Fname = i.getStringExtra("Fname");
		Lname = i.getStringExtra("Lname");
		Email = i.getStringExtra("Email");
		Password = i.getStringExtra("Password");
		Phone = i.getStringExtra("Phone");
		age = i.getStringExtra("Age");
		advisorAge = Integer.parseInt(age);
		Log.e("age", age);
		gender = i.getStringExtra("Gender");
		userType = i.getStringExtra("userType");
		imageByte = i.getByteArrayExtra("Photo");
		file = i.getStringExtra("file");

	}

	private boolean isValidateField() {
		boolean isValidate = true;

		if (editTextFirm.getText().toString().length() == 0) {
			isValidate = false;
		}

		if (editTextDesignation.getText().toString().length() == 0) {
			isValidate = false;
		}
		if (editTextUniversity.getText().toString().length() == 0) {
			isValidate = false;
		}
		if (editTextProffBio.getText().toString().length() == 0) {
			isValidate = false;
		}

		if (editOfficeAddress.getText().toString().length() == 0) {
			isValidate = false;
		}
		if (Utility.isValidEmail(editWebAddress.getText().toString())
				|| editWebAddress.getText().toString().length() == 0) {
			isValidate = false;
		}
		if(btnCheck.isSelected()==false){
			isValidate=false;
		}
		return isValidate;

	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackSignup: {

				Intent intent = new Intent(SignUpStep2AdvisorActivity.this,
						SignInActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;

			}

			case R.id.btnbck2: {
				onBackPressed();
				break;
			}
			
			case R.id.radiobtn_Agreement: {
				
				if(btnCheck.isSelected()==false){
					btnCheck.setBackground(getResources().getDrawable(R.drawable.check_icon));
					btnCheck.setSelected(true);
					break;
				}
				if(btnCheck.isSelected()){
					btnCheck.setBackground(getResources().getDrawable(R.drawable.uncheck_icon));
					btnCheck.setSelected(false);
					break;
				}
						
				
			}
			
			case R.id.btn_Submit: {

				if (!isValidateField()) {
					if (editTextFirm.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Firm Name",
								"Please insert name of the firm");
						return;
					}

					if (editTextDesignation.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Designation Name",
								"Please insert Designation");
						return;
					}
					if (editTextUniversity.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty University",
								"Please enter your University");
						return;
					}
					if (editTextProffBio.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty proffesional Bio",
								"Please enter your proffessional bio");
						return;
					}

					if (editOfficeAddress.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty office address",
								"Please enter office address");
						return;
					}
					if (Utility.isValidEmail(editWebAddress.getText()
							.toString())
							|| editWebAddress.getText().toString().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Invalid website Address",
								"Please insert/check name of the of your website");
						return;
					}
					if(btnCheck.isSelected()==false){
						Utility.ShowAlertWithMessage(mContext, "Alert", "Please accept the terms and conditions of retire.ly");
					}
				} else {
					startSignUp();
				}

			}
				break;
			}
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
	}

	private void startSignUp() {
		// Start to get current location

		

		new SignUpUser().execute("AdvisorSignUp");
		
	}

	public class SignUpUser extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				if (mBProgressBar != null)
					mBProgressBar.cancel();

				mBProgressBar = ProgressDialog.show(
						SignUpStep2AdvisorActivity.this, "Sign Up",
						"Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								SignUpUser.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("In Lead", "execute asynctask");
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			listParams.add(new BasicNameValuePair("fname", Fname));
			listParams.add(new BasicNameValuePair("lname", Lname));
			listParams.add(new BasicNameValuePair("email", Email));
			listParams.add(new BasicNameValuePair("pwd", Password));
			listParams.add(new BasicNameValuePair("phone", Phone));
			listParams.add(new BasicNameValuePair("age", "" + advisorAge));
			listParams.add(new BasicNameValuePair("university", university));
			listParams.add(new BasicNameValuePair("photo", file));

			listParams.add(new BasicNameValuePair("firm", Firm));
			listParams.add(new BasicNameValuePair("proffesionalbio", proffBio));
			listParams.add(new BasicNameValuePair("website", webAdd));
			listParams.add(new BasicNameValuePair("currentoffice", officeAdd));

			listParams.add(new BasicNameValuePair("lat", "" + userLat));
			listParams.add(new BasicNameValuePair("lng", "" + userLng));
			listParams.add(new BasicNameValuePair("devicetype", "android"));
			listParams.add(new BasicNameValuePair("gender", gender));

			if (Utility.getUserPrefernce(mContext, "token") != null)
				listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));

			String serverUrl = Utility.SERVERURL + params[0];
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);
			Log.e("Response", response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("{") && result.length() > 5) {
					HashMap<String, String> map = null;
					;
					try {
						map = Utility.toMap(new JSONObject(result));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (userType == "Lead") {
						Utility.setUserPrefernce(mContext, "id",
								map.get("userid"));
						Utility.setUserPrefernce(mContext, "usertype", "user");
					} else {
						Utility.setUserPrefernce(mContext, "id",
								map.get("advisorid"));
						Utility.setUserPrefernce(mContext, "usertype",
								"advisor");
					}

					map.put("gender",
							Utility.getUserPrefernce(mContext, "gender"));
					result = new JSONObject(map).toString();
					Utility.setBooleanPreferences(mContext, "login", true);
					Utility.setUserPrefernce(mContext, "userinfo", result);
					Intent intent = new Intent(mContext,
							SlideMenuActivityGroup.class);
					startActivityForResult(intent, LOGOUT);
					finish();

				} else if (result.contains("-")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.emailexist));
				} else if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.noexist));
				}
			} else {
				Utility.showAlert(mContext, "Alert",
						getResources().getString(R.string.nonetwork));
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == BUY) {
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivity(intent);
				finish();
			}
		}

	}

	public void finish() {
		// Prepare data intent
		Intent data = new Intent();
		data.putExtra("designation", editTextDesignation.getText().toString());
		data.putExtra("university", editTextUniversity.getText().toString());
		data.putExtra("Firmname", editTextFirm.getText().toString());
		data.putExtra("proffBio", editTextProffBio.getText().toString());
		data.putExtra("websiteAddress", editWebAddress.getText().toString());
		data.putExtra("officeAddress", editOfficeAddress.getText().toString());

		// Activity finished ok, return the data
		setResult(RESULT_OK, data);
		super.finish();
	}

}
