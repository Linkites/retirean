package com.Intlfaces.retire.ly;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.google.android.gms.internal.bm;
import com.linkites.retire.util.RoundedImageView;
import com.linkites.retire.utility.Base64Coder;
import com.linkites.retire.utility.Utility;

public class LeftUserAccountActivity extends Activity {

	private Context mContext;
	RoundedImageView profilePic;
	FrameLayout profile_ic;
	ImageView home_ic;
	ImageView myleads_ic;
	ImageView setting_ic;
	ImageView invitefriends_ic;
	TextView profile_txtv;
	TextView home_txtv;
	TextView myleads_txtv;
	TextView setting_txtv;
	TextView invitefriends_txtv;
	String selected = "";
	LinearLayout layoutProfile;
	LinearLayout layoutHome, layoutMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_left_layout);
		mContext = this;

		
		profilePic = (RoundedImageView) findViewById(R.id.profilePic);
		profilePic.setScaleType(ScaleType.CENTER_CROP);
		profilePic.setCornerRadius(50);
		profilePic.setBorderWidth(2);
		profilePic.setBorderColor(Color.WHITE);
		profilePic.setRoundBackground(true);
		
		profile_ic = (FrameLayout) findViewById(R.id.profile_ic);
		home_ic = (ImageView) findViewById(R.id.home_ic);
		myleads_ic = (ImageView) findViewById(R.id.myleads_ic);
		setting_ic = (ImageView) findViewById(R.id.setting_ic);
		invitefriends_ic = (ImageView) findViewById(R.id.invitefriends_ic);

		profile_txtv = (TextView) findViewById(R.id.profile_txtv);
		home_txtv = (TextView) findViewById(R.id.home_txtv);
		myleads_txtv = (TextView) findViewById(R.id.myleads_txtv);
		setting_txtv = (TextView) findViewById(R.id.setting_txtv);
		invitefriends_txtv = (TextView) findViewById(R.id.invitefriends_txtv);
		layoutProfile = (LinearLayout) findViewById(R.id.layoutProfile);
		layoutHome = (LinearLayout) findViewById(R.id.layoutHomeView);
		layoutMessage = (LinearLayout) findViewById(R.id.layoutmyMatched);

		// startAnimation();
		// profilePic.setImageBitmap(getRoundedShape(""));
		if(Utility.getUserPrefernce(mContext, "userimg")!=null)
			profilePic.setImageBitmap(Base64Coder.decodeBase64(Utility.getUserPrefernce(mContext, "userimg")));
		
		LocalBroadcastManager.getInstance(this).registerReceiver(updateImage, new IntentFilter("upateimage"));
		//startAnimation();
	}

	private BroadcastReceiver updateImage =  new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(Utility.getUserPrefernce(mContext, "userimg")!=null)
						profilePic.setImageBitmap(Base64Coder.decodeBase64(Utility.getUserPrefernce(mContext, "userimg")));
				}
			});
		}
	};
	private void startAnimation() {
		AnimationSet set = new AnimationSet(true);
		set.setFillAfter(true);
		TranslateAnimation moveLefttoRight = new TranslateAnimation(1000, 0, 0,
				0);
		moveLefttoRight.setStartOffset(100);
		moveLefttoRight.setDuration(2000);
		moveLefttoRight.setRepeatCount(-1);
		moveLefttoRight.setRepeatMode(Animation.RESTART);
		set.addAnimation(moveLefttoRight);
		layoutProfile.startAnimation(set);

		AnimationSet setHome = new AnimationSet(true);
		setHome.setFillAfter(true);
		TranslateAnimation moveLefttoRightHome = new TranslateAnimation(1000,
				0, 0, 0);
		moveLefttoRightHome.setStartOffset(101);
		moveLefttoRightHome.setDuration(2000);
		moveLefttoRightHome.setRepeatCount(-1);
		moveLefttoRightHome.setRepeatMode(Animation.RESTART);
		setHome.addAnimation(moveLefttoRightHome);
		layoutHome.startAnimation(setHome);

		AnimationSet setmessage = new AnimationSet(true);
		setmessage.setFillAfter(true);
		TranslateAnimation moveLefttoRightMessage = new TranslateAnimation(
				1000, 0, 0, 0);
		moveLefttoRightMessage.setStartOffset(102);
		moveLefttoRightMessage.setDuration(2000);
		moveLefttoRightMessage.setRepeatCount(-1);
		moveLefttoRightMessage.setRepeatMode(Animation.RESTART);
		setHome.addAnimation(moveLefttoRightMessage);
		layoutMessage.startAnimation(setmessage);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		
	}

	public void onClickMenu(View v) {
		int id = v.getId();
		Intent in = new Intent(Utility.SLIDEMENURBROADCAST);
		if (id < 0)
			id = R.id.layoutHomeView;

		switch (id) {
		case R.id.layoutHomeView:
			in.putExtra("request", Constants.HOME);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			selected = "home";
			runOnUiThread(setSelectedIcon);
			break;

		case R.id.layoutmyMatched:
			in.putExtra("request", Constants.MYMATCHED);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			selected = "myleads";
			runOnUiThread(setSelectedIcon);
			break;
		case R.id.layoutmatchPrefrence:
			in.putExtra("request", Constants.MATCHPREFRENCE);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			selected = "setting";
			runOnUiThread(setSelectedIcon);
			break;
		case R.id.layoutProfile:

			in.putExtra("request", Constants.USERPROFILE);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			selected = "profile";
			runOnUiThread(setSelectedIcon);
			break;
		case R.id.inviteFriends:

			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/html");
			intent.putExtra(Intent.EXTRA_SUBJECT, "Invite friends");
			intent.putExtra(Intent.EXTRA_TEXT,
					"Hello friends, Join Retirely to get connect with best advisors in the world"
							+ "http://retire.ly");
			startActivity(Intent.createChooser(intent, "Join Retirely"));
			selected = "invitefriends";
			runOnUiThread(setSelectedIcon);
			break;

		default:
			break;
		}
	}

	Runnable setSelectedIcon = new Runnable() {

		@Override
		public void run() {
			if (selected == "home") {
				setIcDefault();
				home_ic.setImageResource(R.drawable.icon_home_selected);
				home_txtv.setTextColor(Color.rgb(60, 156, 220));
			} else if (selected == "profile") {
				setIcDefault();
				profile_ic
						.setBackgroundResource(R.drawable.icon_profile_selected);
				profile_txtv.setTextColor(Color.rgb(60, 156, 220));
			} else if (selected == "myleads") {
				setIcDefault();
				myleads_ic.setImageResource(R.drawable.icon_message_selected);
				myleads_txtv.setTextColor(Color.rgb(60, 156, 220));
			} else if (selected == "setting") {
				setIcDefault();
				setting_ic.setImageResource(R.drawable.icon_settings_selected);
				setting_txtv.setTextColor(Color.rgb(60, 156, 220));
			} else if (selected == "invitefriends") {
				setIcDefault();
				invitefriends_ic
						.setImageResource(R.drawable.icon_invite_friends_selected);
				invitefriends_txtv.setTextColor(Color.rgb(60, 156, 220));
			}
		}
	};

	public void setIcDefault() {
		home_ic.setImageResource(R.drawable.icon_home);
		home_txtv.setTextColor(Color.WHITE);

		profile_ic.setBackgroundResource(R.drawable.icon_profile);
		profile_txtv.setTextColor(Color.WHITE);

		setting_ic.setImageResource(R.drawable.icon_settings);
		setting_txtv.setTextColor(Color.WHITE);

		myleads_ic.setImageResource(R.drawable.icon_message);
		myleads_txtv.setTextColor(Color.WHITE);

		invitefriends_ic.setImageResource(R.drawable.icon_invite_friends);
		invitefriends_txtv.setTextColor(Color.WHITE);
	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		// TODO Auto-generated method stub
		int targetWidth = 250;
		int targetHeight = 250;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}
}
