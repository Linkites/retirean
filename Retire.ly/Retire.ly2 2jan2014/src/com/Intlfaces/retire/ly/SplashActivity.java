package com.Intlfaces.retire.ly;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class SplashActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);


		//setContentView(R.layout.splash_screen);
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					sleep(5000);											
				} catch (Exception e) {

				} finally {
					finish();
					Intent i = new Intent(getApplicationContext(),
							SignInActivity.class);
					startActivity(i);
				}
			}
		};
		t.start();
	}
}
