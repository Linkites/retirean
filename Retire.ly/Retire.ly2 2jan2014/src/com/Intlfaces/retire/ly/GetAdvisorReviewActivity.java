package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.ApplicationAdapterForRating;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GetAdvisorReviewActivity extends Activity {

	private ApplicationAdapterForRating adapter;
	private Context mContext;
	private HashMap<String, Bitmap> mapBitmap;
	private boolean isLoaded, isAdvisor;
	private LinearLayout progressBar;
	private ArrayList<HashMap<String, String>> listMatchesRecord;
	private ListView listViewRecord;

	// "//"{"reviews":[{"review":{"reviewid":"3","advisorid":"4","userid":"23","comment":"Nice advice","rating":"2","date":"2013-09-24 01:09:53","status":"10","fname":"mark","lname":"luis","photopath":"upload\/photos\/user\/20130919011210630253674.jpg"}}]}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favourite_layout);

		mContext = this;
		String userType = Utility.getUserPrefernce(mContext, "usertype");
		if (userType.equalsIgnoreCase("user"))
			isAdvisor = false;
		else
			isAdvisor = true;
		// List for store users record
		listMatchesRecord = new ArrayList<HashMap<String, String>>();

		// list view to display record
		listViewRecord = (ListView) findViewById(R.id.listViewMatch);

		// Progressbar while loading data
		progressBar = (LinearLayout) findViewById(R.id.layoutProgress);

		// Map to store downloaded images
		mapBitmap = new HashMap<String, Bitmap>();

		adapter = new ApplicationAdapterForRating(mContext, 1, 0,
				listMatchesRecord, mapBitmap);
		listViewRecord.setAdapter(adapter);
		
		((ImageButton)findViewById(R.id.btn_BackHome)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!isLoaded) {
			isLoaded = true;
			final String advisorid = this.getIntent().getStringExtra("advisorid");
			Timer tm = new Timer();
			tm.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							new GetUserReview().execute(advisorid);
						}
					});
				}
			}, 100);

		}

	}

	private class GetUserReview extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			listViewRecord.setVisibility(View.GONE);
			listMatchesRecord.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String serverUrl = Utility.SERVERURL + "GetReviews";
			serverUrl = serverUrl + "&advisorid=" + params[0];

			String response = Utility.getJSONFromUrl(serverUrl);
			if (response != null) {
				try {
					JSONObject json = new JSONObject(response);
					if(json.has("status"))
					{
						return json.toString();
					}
					JSONArray jsonArray = json.getJSONArray("reviews");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject obj = jsonArray.getJSONObject(i);
						obj = obj.getJSONObject("review");
						HashMap<String, String> map = Utility.toMap(obj);
						listMatchesRecord.add(map);

					}

				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}
			}
			if (listMatchesRecord.size() > 0)
				response = "Success";
			else
				response = "No Record found";

			return response;
		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			progressBar.setVisibility(View.GONE);
			if (result != null) {
				if (result.contains("Success")) {
					listViewRecord.setVisibility(View.VISIBLE);
					adapter.notifyDataSetChanged();
					new Thread(downloadImage).start();
				} else if(result.contains("No Record found")) {
					((TextViewCustom) findViewById(R.id.lblProgressFav))
							.setText(result);
				}else if(result.contains("status"))
				{
					try{
						JSONObject json = new JSONObject(result);
						
						Utility.ShowPurchaseAlertWithMessage(mContext, "Membership", json.getString("message"), json.getString("renewurl"));
						
					}catch(JSONException e)
					{
						e.printStackTrace();
					}
				}
			} else {
				Utility.showAlert(getApplicationContext(), "Alert",
						"Please check your internet connection.");
				progressBar.setVisibility(View.GONE);
				listViewRecord.setVisibility(View.VISIBLE);
			}
		}
	}

	private Runnable downloadImage = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			for (int i = 0; i < listMatchesRecord.size(); i++) {
				HashMap<String, String> map = listMatchesRecord.get(i);
				String url = null;
				if (map.get("photopath").contains("http")
						|| map.get("photopath").contains("https")) {
					url = map.get("photopath");
				} else {
					url = Utility.IMAGEURL;
					url = url + map.get("photopath");
				}
				if (url != null) {
					Bitmap bm = Utility.getBitmap(url);
					mapBitmap.put(map.get("reviewid"), bm);
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						adapter.notifyDataSetChanged();
					}
				});
			}
		}
	};
}
