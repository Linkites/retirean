package com.Intlfaces.retire.ly;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.Intlfaces.retire.ly.R;
import com.aretha.slidemenu.SlideMenu;
import com.linkites.retire.utility.RangeSeekBar;
import com.linkites.retire.utility.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.linkites.retire.utility.Utility;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MatchPrefrenceActivity extends Activity implements OnSeekBarChangeListener, OnTouchListener{

	private TextView txtViewdistance, txtViewAge;
	private ToggleButton toggleBtnMale, toggleBtnFemale;
	Button btnMale,btnFemale;
	String userGender = "";
	Context mContext;
	SlideMenu mSlideMenu;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prefrence);
		mContext=this;
		SlideMenuActivityGroup slide = (SlideMenuActivityGroup) getParent();
		Log.d("Group", " "+slide);
		mSlideMenu = slide.getSlideMenu();
		txtViewAge = (TextView) findViewById(R.id.textViewAge);
		txtViewdistance = (TextView) findViewById(R.id.txtViewDistance);
		toggleBtnFemale = (ToggleButton)findViewById(R.id.women);
		toggleBtnMale = (ToggleButton)findViewById(R.id.men);

					
		btnMale = (Button)findViewById(R.id.btnMale);
		btnFemale = (Button)findViewById(R.id.btnFemale);
		
		
		toggleBtnMale.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(toggleBtnMale.isSelected())
				{
					Log.e("checked Male", "1");
					toggleBtnMale.setSelected(false);
					Utility.setUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "");
					Utility.setBooleanprefence(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), false);
					
				}else
				{
					Log.e("checked Male", "0");
					toggleBtnMale.setSelected(true);
					Utility.setUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "male");
					Utility.setBooleanprefence(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
				}
			}
		});
		
		toggleBtnFemale.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				 if(toggleBtnFemale.isSelected())
					{
					 Log.e("checked Female", "1");
						toggleBtnFemale.setSelected(false);
						Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "");
						Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), false);
					}else
					{
						Log.e("checked Female", "0");
						toggleBtnFemale.setSelected(true);
						Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "female");
						Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
					}
			}
		});
	
		((Button)findViewById(R.id.btnLogout)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("logout"));
				//new LogOut().execute(); 
			}
		});
		
		((Button)findViewById(R.id.btnContactus)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("contactus"));
			}
		});
		
		if(Utility.getUserPrefernce(mContext, "gender")==null)
		{
			userGender="male";
			btnMale.setBackgroundResource(R.drawable.blue_back_left_rounded_corner);
			btnMale.setTextColor(Color.WHITE);
			btnFemale.setBackgroundResource(R.drawable.gray_back_right_rounded_corner);
			btnFemale.setTextColor(Color.rgb(0, 122, 255));
			Utility.setUserPrefernce(mContext, "gender", "male");
		}else
		{
			if (Utility.getUserPrefernce(mContext, "gender").equalsIgnoreCase("male")) {
				
				userGender="male";
				btnMale.setBackgroundResource(R.drawable.blue_back_left_rounded_corner);
				btnMale.setTextColor(Color.WHITE);
				btnFemale.setBackgroundResource(R.drawable.gray_back_right_rounded_corner);
				btnFemale.setTextColor(Color.rgb(0, 122, 255));
				Utility.setUserPrefernce(mContext, "gender", "male");
			}else
			{
				userGender = "female";
				btnFemale.setBackgroundResource(R.drawable.blue_back_right_rounded_corner);
				btnFemale.setTextColor(Color.WHITE);
				btnMale.setBackgroundResource(R.drawable.gray_back_left_rounded_corner);
				btnMale.setTextColor(Color.rgb(0, 122, 255));
				Utility.setUserPrefernce(mContext, "gender", "female");
			}
		}
		
		btnMale.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userGender="male";
				btnMale.setBackgroundResource(R.drawable.blue_back_left_rounded_corner);
				btnMale.setTextColor(Color.WHITE);
				btnFemale.setBackgroundResource(R.drawable.gray_back_right_rounded_corner);
				btnFemale.setTextColor(Color.rgb(0, 122, 255));
				Utility.setUserPrefernce(mContext, "gender", "male");
			}
		});
		
		btnFemale.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				userGender = "female";
				btnFemale.setBackgroundResource(R.drawable.blue_back_right_rounded_corner);
				btnFemale.setTextColor(Color.WHITE);
				btnMale.setBackgroundResource(R.drawable.gray_back_left_rounded_corner);
				btnMale.setTextColor(Color.rgb(0, 122, 255));
				Utility.setUserPrefernce(mContext, "gender", "female");
			}
		});
		
		if(Utility.getUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"))==null)
		{
			Utility.setUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "male");
			Utility.setBooleanprefence(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
			if(Utility.getFloatPrefrence(MatchPrefrenceActivity.this,"distance_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"))==0)
			{
				Utility.setFloatPrefrence(getApplicationContext(), "distance_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), 100);
			}
			if(Utility.getIntegerPrefrence(MatchPrefrenceActivity.this,  "maxage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"))==0)
			 Utility.setIntegerPrefrence(getApplicationContext(), "maxage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), 50);
			if(Utility.getIntegerPrefrence(MatchPrefrenceActivity.this,  "maxage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"))==0)
             Utility.setIntegerPrefrence(getApplicationContext(), "minage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), 18);
		}
		if(Utility.getUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"))==null)
		{
			Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "female");
			Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
		}
		
		SeekBar distance = (SeekBar)findViewById(R.id.seekBardistance);
		distance.setOnTouchListener(this);
		distance.setMax(100);
		distance.setProgress((int)Utility.getFloatPrefrence(MatchPrefrenceActivity.this, "distance_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id")));
		txtViewdistance.setText(distance.getProgress()+" Miles");
		distance.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			
				
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
				
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				txtViewdistance.setText(progress+" Miles");
				Utility.setFloatPrefrence(getApplicationContext(), "distance_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), progress);
			}
		});
		
		// create RangeSeekBar as Integer range between 20 and 75
		RangeSeekBar<Integer> ageBar = new RangeSeekBar<Integer>(18, 75, this);
		ageBar.setSelectedMaxValue(Utility.getIntegerPrefrence(getApplicationContext(), "maxage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id")));
		ageBar.setSelectedMinValue(Utility.getIntegerPrefrence(getApplicationContext(), "minage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id")));
		
		android.view.ViewGroup.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT);
		ageBar.setLayoutParams(lp);
	
		((LinearLayout)findViewById(R.id.layoutPrefrenceAge)).addView(ageBar);
		txtViewAge.setText(ageBar.getSelectedMinValue()+" - "+ageBar.getSelectedMaxValue()+" +");
		ageBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
		        @Override
		        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
		                // handle changed range values
		               String value = minValue+" - "+maxValue+" +";
		               txtViewAge.setText(value);
		               Utility.setIntegerPrefrence(getApplicationContext(), "maxage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), maxValue);
		               Utility.setIntegerPrefrence(getApplicationContext(), "minage_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), minValue);
		        }
		});

		((ImageButton)findViewById(R.id.btn_BackPrefrence)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LocalBroadcastManager.getInstance(MatchPrefrenceActivity.this)
				.sendBroadcast(new Intent("slide").putExtra("slide","left"));
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e("OnResume", "In On Resume");
		if(Utility.getBooleanPrefernce(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id")))
		{
			toggleBtnMale.setChecked(true);
		}else
		{
			toggleBtnMale.setChecked(false);			
		}
		if(Utility.getBooleanPrefernce(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id")))
		{
			toggleBtnFemale.setChecked(true);			
		}else
		{
			toggleBtnFemale.setChecked(false);
		}
	}
	
	public void onClickPrefrence(View v) {
		int id = v.getId();
		switch (id) {
//		case R.id.layoutPrefrenceMale:
//			if(toggleBtnMale.isSelected())
//			{
//				Log.e("checked", "1");
//				toggleBtnMale.setSelected(false);
//				Utility.setUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "");
//				Utility.setBooleanprefence(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), false);
//				
//			}else
//			{
//				Log.e("checked", "0");
//				toggleBtnMale.setSelected(true);
//				Utility.setUserPrefernce(getApplicationContext(), "male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "male");
//				Utility.setBooleanprefence(getApplicationContext(), "Male_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
//			}
//			
//			 if(toggleBtnFemale.isSelected())
//			{
//				toggleBtnFemale.setSelected(false);
//				Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "");
//				Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), false);
//			}else
//			{
//				toggleBtnFemale.setSelected(true);
//				Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "female");
//				Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
//			}
//			break;
//		case R.id.layoutPrefrenceFemale:
//
//			if(toggleBtnFemale.isSelected())
//			{
//				toggleBtnFemale.setSelected(false);
//				Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "");
//				Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), false);
//			}else
//			{
//				toggleBtnFemale.setSelected(true);
//				Utility.setUserPrefernce(getApplicationContext(), "female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), "female");
//				Utility.setBooleanprefence(getApplicationContext(), "Female_"+Utility.getUserPrefernce(MatchPrefrenceActivity.this, "id"), true);
//			}
//			
//			break;

		default:
			break;
		}
	}
	
	private class LogOut extends AsyncTask<String, Void, String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			ProgressDialog.show(mContext, "Log Out", "Please wait...", true, true, new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					LogOut.this.cancel(true);
				}
			});
		}
		@Override
		protected String doInBackground(String... params) {
			String serverUrl = Utility.SERVERURL + "SignOut";
			String response = null;
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			try
			{
				listParams.add(new BasicNameValuePair("id", Utility.getUserPrefernce(getApplicationContext(), "id")));
				listParams.add(new BasicNameValuePair("usertype", Utility.getUserPrefernce(getApplicationContext(), "usertype")));
				response = Utility.postParamsAndfindJSON(serverUrl, listParams);
			
			}catch (Exception e)
			{
				e.printStackTrace();
				return null;
			}
			return response;
		}
		protected void onPostExecute(String result) {			
			if(result!=null)
			{
				if(result.contains("1"))
				{
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("logout"));
				}
			}
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v instanceof SeekBar)
		{
			mSlideMenu.setEdgeSlideEnable(true);
			mSlideMenu.setSlideMode(SlideMenu.STATE_CLOSE);
		}else
		{
			mSlideMenu.setEdgeSlideEnable(true);
			mSlideMenu.setSlideMode(SlideMenu.STATE_CLOSE);
		}
		return false;
	}
}
