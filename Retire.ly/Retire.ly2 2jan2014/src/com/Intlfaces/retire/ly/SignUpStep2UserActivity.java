package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.linkites.retire.utility.Utility;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class SignUpStep2UserActivity extends Activity {
	private ProgressDialog mBProgressBar;
	private Context mContext;
	private Button btnSubmit;
	private RadioButton radiobtn_Agreement;
	private String userType = "", gender = "", Fname = "", Lname = "",
			Email = "", Password = "", Phone = "", photo = "", age = "",
			occupation, university, file = "", amount;
	private EditText editTextOccupation, editTextUniversity, editTextAmount;

	byte[] imageByte = {};
	private HashMap<String, String> infoMap;
	private int requestFor;
	private final int BUY = 101;
	private double userLat, userLng;
	private static final int LOGOUT = 3;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up_step_2_user);
		mContext = this;

		Intent i = getIntent();
		Intent intent = getIntent();
		infoMap = (HashMap<String, String>) intent.getSerializableExtra("map");
		
		btnSubmit = (Button) findViewById(R.id.btn_Submit);
		btnSubmit.setOnClickListener(onClickListener);

		((ImageButton) findViewById(R.id.btn_BackSignup))
				.setOnClickListener(onClickListener);

		((Button) findViewById(R.id.btnbck2))
				.setOnClickListener(onClickListener);

		editTextOccupation = (EditText) findViewById(R.id.txtSOccupation);
		editTextOccupation.setText(i.getStringExtra("occupation"));
		occupation = editTextOccupation.getText().toString();

		editTextUniversity = (EditText) findViewById(R.id.txtSUniversity);
		editTextUniversity.setText(i.getStringExtra("universityUser"));
		university = editTextUniversity.getText().toString();

		editTextAmount = (EditText) findViewById(R.id.txtSAmount);
		editTextAmount.setText(i.getStringExtra("amount"));

		amount = editTextAmount.getText().toString();

		if (i.getExtras() != null) {
			Fname = i.getStringExtra("Fname");
			Lname = i.getStringExtra("Lname");
			Email = i.getStringExtra("Email");
			Password = i.getStringExtra("Password");
			Phone = i.getStringExtra("Phone");
			age = i.getStringExtra("Age");
			gender = i.getStringExtra("Gender");
			userType = i.getStringExtra("userType");
			imageByte = i.getByteArrayExtra("Photo");
			file = i.getStringExtra("file");
		} else {
			Fname = infoMap.get("fname");
			Lname = infoMap.get("lname");
			Email = infoMap.get("email");
			Password = "";
			Phone = infoMap.get("phone");
			age = infoMap.get("age");
			gender = infoMap.get("gender");
			userType = infoMap.get("usertype");
			file = infoMap.get("photo");
		}

	}

	private boolean isValidateField() {
		boolean isValidate = true;

		if (editTextOccupation.getText().toString().length() == 0) {
			isValidate = false;

		}

		if (editTextUniversity.getText().toString().length() == 0) {
			isValidate = false;
		}
		if (editTextAmount.getText().toString().length() == 0) {
			isValidate = false;
		}
		return isValidate;

	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackSignup: {
				Intent intent = new Intent(SignUpStep2UserActivity.this,
						SignInActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
			}

			case R.id.btnbck2: {
				onBackPressed();
				break;
			}
			case R.id.btn_Submit: {

				// if (!isValidateField())
				{
					if (editTextOccupation.getText().toString().trim().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty Occupation",
								"Please enter the Occupation");
						return;
					}

					if (editTextUniversity.getText().toString().trim().length() == 0) {
						Utility.ShowAlertWithMessage(mContext,
								"Empty University",
								"Please enter the University");
						return;
					}
					if (editTextAmount.getText().toString().trim().length() == 0) {
						Utility.ShowAlertWithMessage(mContext, "Empty Amount",
								"Please enter the Amount");
						return;
					}
					occupation = editTextOccupation.getText().toString();
					infoMap.put("occupation", occupation);
					university = editTextUniversity.getText().toString();
					infoMap.put("university", university);
					amount = editTextAmount.getText().toString();
					infoMap.put("investableamout", amount);
					Log.e("infoMap", infoMap.toString());
					startSignUp();
				}

			}
				break;
			}
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
	}

	private void startSignUp() {
		// Start to get current location

		new SignUpUser().execute("UserSignUp");


	}

	public class SignUpUser extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				if (mBProgressBar != null)
					mBProgressBar.cancel();

				mBProgressBar = ProgressDialog.show(
						SignUpStep2UserActivity.this, "Sign Up", "Please wait",
						true, true, new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								SignUpUser.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("In Lead", "execute asynctask");
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			listParams.add(new BasicNameValuePair("fname", Fname));
			listParams.add(new BasicNameValuePair("lname", Lname));
			listParams.add(new BasicNameValuePair("email", Email));
			listParams.add(new BasicNameValuePair("pwd", Password));
            listParams.add(new BasicNameValuePair("phone", Phone));
            listParams.add(new BasicNameValuePair("age", age));
			listParams.add(new BasicNameValuePair("university", university));
			listParams.add(new BasicNameValuePair("photo", file));
			listParams.add(new BasicNameValuePair("designation", occupation));
			listParams.add(new BasicNameValuePair("amount", amount));
			listParams.add(new BasicNameValuePair("lat", "" + userLat));
			listParams.add(new BasicNameValuePair("lng", "" + userLng));
			listParams.add(new BasicNameValuePair("devicetype", "android"));
			listParams.add(new BasicNameValuePair("gender", gender));

			if (Utility.getUserPrefernce(mContext, "token") != null)
				listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));
			Log.e("Response User step 1", "Towards step 2");
			String serverUrl = Utility.SERVERURL + params[0];
			Log.e("Fields into API", listParams.toString());
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);
			Log.e("Response User step 2", response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("{") && result.length() > 5) {
					HashMap<String, String> map = null;
					;
					try {
						map = Utility.toMap(new JSONObject(result));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (userType == "Lead") {
						Utility.setUserPrefernce(mContext, "id",
								map.get("userid"));
						Utility.setUserPrefernce(mContext, "usertype", "user");
					} else {
						Utility.setUserPrefernce(mContext, "id",
								map.get("advisorid"));
						Utility.setUserPrefernce(mContext, "usertype",
								"advisor");
					}

					map.put("gender",
							Utility.getUserPrefernce(mContext, "gender"));
					result = new JSONObject(map).toString();
					Utility.setBooleanPreferences(mContext, "login", true);
					Utility.setUserPrefernce(mContext, "userinfo", result);
					Intent intent = new Intent(mContext,
							SlideMenuActivityGroup.class);
					startActivityForResult(intent, LOGOUT);
					finish();

				} else if (result.contains("-")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.emailexist));
				} else if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert", getResources()
							.getString(R.string.noexist));
				}
			} else {
				Utility.showAlert(mContext, "Alert",
						getResources().getString(R.string.nonetwork));
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == BUY) {
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivity(intent);
				finish();
			}
		}

	}

	public void finish() {
		// Prepare data intent
		Intent userData = new Intent();
		userData.putExtra("occupation", editTextOccupation.getText().toString());
		userData.putExtra("universityUser", editTextUniversity.getText()
				.toString());
		userData.putExtra("amount", editTextAmount.getText().toString());

		// Activity finished ok, return the data
		setResult(RESULT_OK, userData);
		super.finish();
	}

}
