package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.aretha.slidemenu.SlideMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.linkites.retire.util.ImageDownloader;
import com.linkites.retire.util.RoundedImageView;
import com.linkites.retire.util.ImageDownloader.Mode;
import com.linkites.retire.utility.Base64Coder;
import com.linkites.retire.utility.Utility;
import com.linkites.retire.utility.ViewSwitcher;
import android.graphics.Canvas;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;

public class CenterHomeActivity extends Activity implements
		OnSeekBarChangeListener, OnTouchListener, OnGestureListener {

	// CustomDraggableView imgStackContainer=null;
	static final LatLng KIEL = new LatLng(53.551, 9.993);
	private Context mContext;
	private GoogleMap map;
	PointF DownPT = new PointF(); // Record Mouse Position When Pressed Down
	PointF StartPT = new PointF();
	private Animation show, hide, rightRotate, return_right_rotate;
	private double userLat, userLng;
	private boolean worldwide = false, markFav = false, isbuttonPressed,
			isblock, isLike;
	private RelativeLayout layoutProgressMap;
	public ArrayList<View> arrayViews;
	private ArrayList<HashMap<String, String>> listRecords;
	private ArrayList<HashMap<String, String>> WorldWidelistRecords;
	private FrameLayout layoutImages;
	private ProgressDialog mProgressDialog;
	private GestureDetector detectore;
	private LinearLayout layoutBottomLikeDislike, layoutBottomNorecord;
	private ViewSwitcher viewSwitcher;
	private static final String TAG = "Touch";
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();
	private boolean isLeftAnimationRunnning, isRightAnimationRunnning;
	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;

	SlideMenu mSlideMenu;
	private ImageButton btnDislike;
	RoundedImageView imageView;
	private HashMap<String, Bitmap> hasMarkerBitmap;

	@SuppressLint({ "NewApi", "CutPasteId" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_center_home);
		mContext = this;
		SlideMenuActivityGroup slide = (SlideMenuActivityGroup) getParent();
		mSlideMenu = slide.getSlideMenu();
		ViewGroup container = (ViewGroup) findViewById(R.id.container);
		viewSwitcher = new ViewSwitcher(container);
		arrayViews = new ArrayList<View>();
		hasMarkerBitmap = new HashMap<String, Bitmap>();

		AlphaAnimation blinkanimation = new AlphaAnimation(1, 0); // Change
																	// alpha
																	// from
																	// fully
																	// visible
																	// to
																	// invisible
		blinkanimation.setDuration(300); // duration - half a second
		blinkanimation.setInterpolator(new LinearInterpolator()); // do not
																	// alter
																	// animation
																	// rate
		blinkanimation.setRepeatCount(1); // Repeat animation infinitely
		blinkanimation.setRepeatMode(Animation.REVERSE);
		layoutBottomLikeDislike = (LinearLayout) findViewById(R.id.layoutHomeBottom);
		layoutBottomNorecord = (LinearLayout) findViewById(R.id.layoutBottomInvite);
		layoutBottomNorecord.setVisibility(View.GONE);
		layoutBottomLikeDislike.setVisibility(View.GONE);
		hide = AnimationUtils.loadAnimation(this, R.anim.alpha_animation_hide);
		show = AnimationUtils.loadAnimation(this, R.anim.alpha_animation_show);
		imageView = (RoundedImageView) findViewById(R.id.imageViewCenter);
		imageView.setScaleType(ScaleType.CENTER_CROP);
		imageView.setCornerRadius(50);
		imageView.setBorderWidth(2);
		imageView.setBorderColor(Color.DKGRAY);
		imageView.setRoundBackground(true);

		rightRotate = AnimationUtils.loadAnimation(this, R.anim.right_rotate);
		return_right_rotate = AnimationUtils.loadAnimation(this,
				R.anim.return_right_rotate);
		listRecords = new ArrayList<HashMap<String, String>>();
		WorldWidelistRecords = new ArrayList<HashMap<String, String>>();

		detectore = new GestureDetector(this, this);
		// Setup loading view
		new HashMap<String, Bitmap>();
		layoutProgressMap = (RelativeLayout) findViewById(R.id.layoutProgress);
		layoutImages = (FrameLayout) findViewById(R.id.layoutStackImages);

		if (Utility.getUserPrefernce(
				getApplicationContext(),
				"male_"
						+ Utility.getUserPrefernce(CenterHomeActivity.this,
								"id")) == null) {
			Utility.setUserPrefernce(
					getApplicationContext(),
					"male_"
							+ Utility.getUserPrefernce(CenterHomeActivity.this,
									"id"), "");
			Utility.setBooleanprefence(getApplicationContext(), "Male_"
					+ Utility.getUserPrefernce(CenterHomeActivity.this, "id"),
					true);
			if (Utility.getFloatPrefrence(CenterHomeActivity.this, "distance_"
					+ Utility.getUserPrefernce(CenterHomeActivity.this, "id")) == 0) {
				Utility.setFloatPrefrence(
						getApplicationContext(),
						"distance_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"), 100);
			}
			if (Utility.getIntegerPrefrence(CenterHomeActivity.this, "maxage_"
					+ Utility.getUserPrefernce(CenterHomeActivity.this, "id")) == 0)
				Utility.setIntegerPrefrence(
						getApplicationContext(),
						"maxage_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"), 50);
			if (Utility.getIntegerPrefrence(CenterHomeActivity.this, "maxage_"
					+ Utility.getUserPrefernce(CenterHomeActivity.this, "id")) == 0)
				Utility.setIntegerPrefrence(
						getApplicationContext(),
						"minage_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"), 18);
			if (Utility.getUserPrefernce(getApplicationContext(), "female_"
					+ Utility.getUserPrefernce(CenterHomeActivity.this, "id")) == null) {
				Utility.setUserPrefernce(
						getApplicationContext(),
						"female_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"), "");
				Utility.setBooleanprefence(
						getApplicationContext(),
						"Female_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"), true);
			}
		}

		((ImageView) findViewById(R.id.imageViewMatches))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						LocalBroadcastManager.getInstance(mContext)
								.sendBroadcast(
										new Intent("slide").putExtra("slide",
												"right"));
					}
				});
		((ImageView) findViewById(R.id.imageViewMap))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!worldwide) {
							worldwide = true;
							new GetUserOrAdvisor().execute("");
						} else {
							worldwide = false;
						}
						viewSwitcher.swap();
					}
				});

		((Button) findViewById(R.id.btnBottomNorecord))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("text/html");
						intent.putExtra(Intent.EXTRA_SUBJECT, "Invite friends");
						intent.putExtra(Intent.EXTRA_TEXT,
								"Hello friends, Join Retirely to get connect with best advisors in the world"
										+ "http://retire.ly");
						startActivity(Intent.createChooser(intent,
								"Join Retirely"));

					}
				});
		// Check google play service install or not install in device for map
		int status = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getBaseContext());
		if (status == ConnectionResult.SUCCESS) {
			// if (map != null)
			map = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.mapAll)).getMap();
			// if (supportMapFragment != null) {

			map.getUiSettings().setAllGesturesEnabled(true);
			map.getUiSettings().setScrollGesturesEnabled(true);
			map.getUiSettings().setZoomGesturesEnabled(true);
			map.getUiSettings().setRotateGesturesEnabled(false);
			map.getUiSettings().setZoomControlsEnabled(true);
			// }
		} else {
			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
					CenterHomeActivity.this, requestCode);
			try {
				dialog.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		((ImageButton) findViewById(R.id.btn_BackHome))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						LocalBroadcastManager.getInstance(mContext)
								.sendBroadcast(
										new Intent("slide").putExtra("slide",
												"left"));
					}
				});

		// Fav update is true (In the case of crashing on favorite)
		if (Utility.getBooleanPrefernce(mContext, "favupdate")) {
			Utility.setBooleanprefence(mContext, "favupdate", false);
		}

		userLat = 22.70450;// 34.197311;
		userLng = 75.87373;// -118.643982;
		new GetUserOrAdvisor().execute("");
		final Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.current_location);

		Timer tm = new Timer();
		new Timer();
		tm.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						((View) findViewById(R.id.viewCircle))
								.startAnimation(animation);
					}
				});
			}
		}, 0, 4000);

		((ImageButton) findViewById(R.id.btnFavorite))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						markFav = true;
						isbuttonPressed = true;
						if (arrayViews.size() > 0) {
							HashMap<String, String> mapdata = listRecords
									.get(arrayViews.size() - 1);
							if (Utility.getUserPrefernce(mContext, "usertype")
									.equalsIgnoreCase("advisor"))
								new MarkAsFavOrUnFavorite().execute(mapdata
										.get("userid"));
							else
								new MarkAsFavOrUnFavorite().execute(mapdata
										.get("advisorid"));

							// swapRigh();
						}

					}
				});
		btnDislike = ((ImageButton) findViewById(R.id.btnRemovePhoto));

		btnDislike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				markFav = false;
				isbuttonPressed = true;
				if (arrayViews.size() > 0) {
					HashMap<String, String> mapdata = listRecords
							.get(arrayViews.size() - 1);
					if (Utility.getUserPrefernce(mContext, "usertype")
							.equalsIgnoreCase("advisor"))
						new MarkAsFavOrUnFavorite().execute(mapdata
								.get("userid"));
					else
						new MarkAsFavOrUnFavorite().execute(mapdata
								.get("advisorid"));

				}
				// swapLeft();

			}
		});

		((ImageButton) findViewById(R.id.imageViewInfo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						final HashMap<String, String> map = listRecords
								.get(arrayViews.size() - 1);
						Intent intent = new Intent(mContext,
								OtherUserProfileActivityView.class);
						int requestfor = (map.get("userid") != null) ? Constants.USER
								: Constants.ADVISOR;
						intent.putExtra("requestFor", requestfor);
						intent.putExtra("broadcast", "updatsearch");
						intent.putExtra("detail", map);
						startActivity(intent);
					}
				});

		LocalBroadcastManager.getInstance(this).registerReceiver(likedReceiver,
				new IntentFilter("liked"));
		LocalBroadcastManager.getInstance(this).registerReceiver(nopeReceiver,
				new IntentFilter("nope"));
		LocalBroadcastManager.getInstance(this).registerReceiver(updateUsers,
				new IntentFilter("updateuser"));
		LocalBroadcastManager.getInstance(this).registerReceiver(updateSearch,
				new IntentFilter("updatsearch"));

		LocalBroadcastManager.getInstance(this).registerReceiver(updatePhoto,
				new IntentFilter("updatePhoto"));

		updatePhoto();

	}

	private BroadcastReceiver updatePhoto = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					updatePhoto();
				}
			});
		}
	};

	private void updatePhoto() {
		if (Utility.getUserPrefernce(this, "userimg") == null) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					String url = null;
					String Map = Utility.getUserPrefernce(mContext,
							"userprofileinfo");
					JSONObject json;
					HashMap<String, String> Infomap = null;
					try {
						json = new JSONObject(Map);
						Infomap = Utility.toMap(json);
						Log.e("Info Map data in profile view class", ""
								+ Infomap);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (Infomap.get("photopath") != null)
						if (Infomap.get("photopath").contains("http")
								|| Infomap.get("photopath").contains("https")) {
							url = Infomap.get("photopath");
						} else {
							url = Utility.IMAGEURL + Infomap.get("photopath");
						}

					final Bitmap bm = Utility.getBitmap(url);
					if (bm != null)
						Utility.setUserPrefernce(mContext, "userimg",
								Base64Coder.encodeTobase64(bm));

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (bm != null)
								imageView.setImageBitmap(bm);
							else
								imageView.setImageResource(R.drawable.dummy);

							LocalBroadcastManager.getInstance(mContext)
									.sendBroadcast(new Intent("upateimage"));
						}
					});
				}
			}).start();
		} else {
			imageView.setImageBitmap(Base64Coder.decodeBase64(Utility
					.getUserPrefernce(mContext, "userimg")));
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(
					new Intent("upateimage"));
		}
	}

	BroadcastReceiver updateUsers = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String userId = intent.getStringExtra("id");
			final boolean isBlock = intent.getBooleanExtra("block", false);
			for (int i = 0; i < listRecords.size(); i++) {
				HashMap<String, String> map = listRecords.get(i);
				String mapUserid = (map.get("usertype")
						.equalsIgnoreCase("user")) ? map.get("userid") : map
						.get("advisorid");
				if (mapUserid.equalsIgnoreCase(userId)) {
					listRecords.remove(i);
					break;
				}

			}
			runOnUiThread(new Runnable() {
				public void run() {
					if (isBlock)
						swapLeft();
					else
						swapRigh();
				}
			});
		}
	};
	// Liked receiver
	private BroadcastReceiver likedReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			final int tag = intent.getIntExtra("tag", -1);
			runOnUiThread(new Runnable() {
				public void run() {

					if (tag >= 0) {
						HashMap<String, String> mapdata = listRecords.get(tag);
						if (Utility.getUserPrefernce(mContext, "usertype")
								.equalsIgnoreCase("advisor"))
							new MarkAsFavOrUnFavorite().execute(mapdata
									.get("userid"));
						else
							new MarkAsFavOrUnFavorite().execute(mapdata
									.get("advisorid"));
					}

					if (tag == 0) {
						layoutImages.setVisibility(View.INVISIBLE);
						layoutProgressMap.setVisibility(View.VISIBLE);
						layoutBottomNorecord.setVisibility(View.GONE);
						layoutBottomLikeDislike.setVisibility(View.GONE);
					}
				}
			});
		}
	};

	// Nope swip receiver
	private BroadcastReceiver nopeReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			final int tag = intent.getIntExtra("tag", -1);
			markFav = false;
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(mContext, "Photo Disliked index " + tag,
							Toast.LENGTH_LONG).show();
					if (tag >= 0) {
						HashMap<String, String> mapdata = listRecords.get(tag);
						Toast.makeText(mContext,
								"Favorite " + mapdata.get("fname"),
								Toast.LENGTH_LONG).show();

						if (Utility.getUserPrefernce(mContext, "usertype")
								.equalsIgnoreCase("advisor"))
							new MarkAsFavOrUnFavorite().execute(mapdata
									.get("userid"));
						else
							new MarkAsFavOrUnFavorite().execute(mapdata
									.get("advisorid"));

					}

					if (tag == 0) {
						layoutImages.setVisibility(View.INVISIBLE);
						layoutProgressMap.setVisibility(View.VISIBLE);
						layoutBottomNorecord.setVisibility(View.GONE);
						layoutBottomLikeDislike.setVisibility(View.GONE);
					}
				}
			});
		}
	};

	private BroadcastReceiver updateSearch = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String type = intent.getStringExtra("type");
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) intent
					.getSerializableExtra("details");
			if (type.equalsIgnoreCase("detail")) {

				listRecords.set(arrayViews.size() - 1, map);

				View v = arrayViews.get(arrayViews.size() - 1);
				ImageView imageRate = (ImageView) v
						.findViewById(R.id.imageViewRate);

				if (!map.get("overallrating").equalsIgnoreCase("0")) {
					int rate = Math.round(Float.valueOf(map
							.get("overallrating")));
					String name = "review_" + rate + "_fill";
					int resId = getResources().getIdentifier(name, "drawable",
							getPackageName());
					imageRate.setImageResource(resId);
					imageRate.setVisibility(View.VISIBLE);
				}

			}
			if (type.equalsIgnoreCase("fav")) {
				swapRigh();
			}
			if (type.equalsIgnoreCase("block")) {
				swapRigh();
			}

		}
	};

	// @Override
	// public void onBackPressed (){
	// super.onBackPressed();
	// finish();
	// }
	@Override
	public void onResume() {
		super.onResume();
		if (this.getIntent().getIntExtra("requestFor", 0) == Constants.MYMATCHED) {
			Intent in = new Intent(Utility.SLIDEMENURBROADCAST);
			in.putExtra("request", Constants.RIGHT);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
		}
	}

	private ProgressDialog mProgressDialog2;

	// Start to get user near by of user
	private class GetUserOrAdvisor extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// layoutFrames.setVisibility(View.GONE);
			if (worldwide) {
				mProgressDialog2 = ProgressDialog.show(mContext,
						"Searching world wide", "Please wait...", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface arg0) {
								// TODO Auto-generated method stub
								GetUserOrAdvisor.this.cancel(true);
							}
						});
			}
			layoutImages.setVisibility(View.INVISIBLE);
			layoutProgressMap.setVisibility(View.VISIBLE);
			layoutImages.removeAllViews();
		}

		@Override
		protected String doInBackground(String... params) {
			String serverUrl = Utility.SERVERURL;
			String usertype = Utility.getUserPrefernce(mContext, "usertype");
			if (usertype != null) {

				String male = (Utility.getBooleanPrefernce(
						mContext,
						"Male_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"))) ? "male"
						: "";
				String female = (Utility.getBooleanPrefernce(
						mContext,
						"Female_"
								+ Utility.getUserPrefernce(
										CenterHomeActivity.this, "id"))) ? "female"
						: "";

				if (usertype.equalsIgnoreCase("user")) {

					// if get advisor by world wide
					if (worldwide) {
						serverUrl = serverUrl + "SearchAdvisors"
								+ "&keywords=worldwide" + "&userid="
								+ Utility.getUserPrefernce(mContext, "id");
					} else {
						serverUrl = Utility.SERVERURL + "SearchAdvisors"
								+ "&userid="
								+ Utility.getUserPrefernce(mContext, "id");

						serverUrl = serverUrl
								+ "&lat="
								+ Utility.getUserIdPrefernce(mContext, "latd")
								+ "&lng="
								+ Utility.getUserIdPrefernce(mContext, "long")
								+ "&male="
								+ male
								+ "&female="
								+ female
								+ "&miles="
								+ Utility
										.getFloatPrefrence(
												mContext,
												"distance_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"))
								+ "&minage="
								+ Utility
										.getIntegerPrefrence(
												mContext,
												"minage_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"))
								+ "&maxage="
								+ Utility
										.getIntegerPrefrence(
												mContext,
												"maxage_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"));

					}
				} else {
					// if get user by world wide
					if (worldwide) {
						serverUrl = serverUrl + "SearchUsers"
								+ "&keywords=worldwide" + "&advisorid="
								+ Utility.getUserPrefernce(mContext, "id");
					} else {
						serverUrl = Utility.SERVERURL + "SearchUsers"
								+ "&advisorid="
								+ Utility.getUserPrefernce(mContext, "id");

						serverUrl = serverUrl
								+ "&lat="
								+ Utility.getUserIdPrefernce(mContext, "latd")
								+ "&lng="
								+ Utility.getUserIdPrefernce(mContext, "long")
								+ "&male="
								+ male
								+ "&female="
								+ female
								+ "&miles="
								+ Utility
										.getFloatPrefrence(
												mContext,
												"distance_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"))
								+ "&minage="
								+ Utility
										.getIntegerPrefrence(
												mContext,
												"minage_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"))
								+ "&maxage="
								+ Utility
										.getIntegerPrefrence(
												mContext,
												"maxage_"
														+ Utility
																.getUserPrefernce(
																		CenterHomeActivity.this,
																		"id"));
					}
				}
			}
			String resposne = Utility.getJSONFromUrl(serverUrl);

			if (resposne != null) {
				try {
					String subKey = null;
					JSONObject jsonObject = new JSONObject(resposne);
					JSONArray jsonArray = null;
					if (usertype.equalsIgnoreCase("user")) {
						jsonArray = jsonObject.getJSONArray("advisors");
						subKey = "advisor";
					} else {
						jsonArray = jsonObject.getJSONArray("users");
						subKey = "user";
					}
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						object = object.getJSONObject(subKey);
						HashMap<String, String> map = Utility.toMap(object);
						listRecords.add(map);
						WorldWidelistRecords.add(map);
						System.out.println("This is world wide reocrds:"
								+ WorldWidelistRecords);

					}
				} catch (JSONException e) {
					e.printStackTrace();
					return "No Record found.";
				}
				if (listRecords.size() > 0)
					return "Success";
				else
					return "No Record found.";
			}
			return null;
		}

		protected void onPostExecute(String result) {
			if (mProgressDialog2 != null) {
				mProgressDialog2.cancel();
			}
			if (result != null) {
				if (result.equalsIgnoreCase("Success")) {

					if (!worldwide) {
						new Thread(downloadImages).start();
					}
					if (worldwide) {
						if (WorldWidelistRecords.size() > 0) {
							setUsersOnMap();
						} else {
							Toast.makeText(mContext,
									"There is no record to display in map",
									Toast.LENGTH_LONG).show();
						}

					}
				}
				if (result.equalsIgnoreCase("Failed")) {
					Toast.makeText(mContext, "Failed", Toast.LENGTH_SHORT)
							.show();

					layoutImages.setVisibility(View.INVISIBLE);
					layoutProgressMap.setVisibility(View.VISIBLE);
					layoutBottomNorecord.setVisibility(View.VISIBLE);
					layoutBottomLikeDislike.setVisibility(View.GONE);
					return;
				}
				if (result.equalsIgnoreCase("No Record found.")) {
					Toast.makeText(mContext, "No Record found.",
							Toast.LENGTH_SHORT).show();
					layoutImages.setVisibility(View.INVISIBLE);
					layoutProgressMap.setVisibility(View.VISIBLE);
					layoutBottomNorecord.setVisibility(View.VISIBLE);
					layoutBottomLikeDislike.setVisibility(View.GONE);

					return;
				}
			}
			GetUserOrAdvisor.this.cancel(true);
		}
	}

	private Runnable downloadImages = new Runnable() {
		@Override
		public void run() {
			final LayoutInflater inflater = LayoutInflater.from(mContext);

			for (int i = 0; i < listRecords.size(); i++) {
				final HashMap<String, String> map = listRecords.get(i);
				final View v = inflater.inflate(R.layout.layout_stack_image,
						null);
				v.setTag(i);
				v.setOnTouchListener(CenterHomeActivity.this);
				arrayViews.add(v);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						// PhotoStackImageView imageview = new
						// PhotoStackImageView(mContext);
						v.setLayoutParams(new LinearLayout.LayoutParams(
								android.widget.LinearLayout.LayoutParams.WRAP_CONTENT,
								android.widget.LinearLayout.LayoutParams.WRAP_CONTENT));
						final ImageView imageview = (ImageView) v
								.findViewById(R.id.imageViewStack);
						String imageUrl = "";
						if (map.get("photopath").contains("http")
								|| map.get("photopath").contains("https")) {
							imageUrl = map.get("photopath");
						} else {
							imageUrl = Utility.IMAGEURL + map.get("photopath");
						}
						if (imageUrl != null) {
							ImageDownloader imageLoaaderr = new ImageDownloader();
							imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
							imageLoaaderr.download(imageUrl, imageview,
									BitmapFactory.decodeResource(
											getResources(), R.drawable.dummy));
						} else
							imageview.setImageResource(R.drawable.dummy);

						imageview.setTag(v.getTag());

						String fullname = map.get("fname") + " "
								+ map.get("lname");
						fullname = fullname + ", " + map.get("age");
						((TextView) v.findViewById(R.id.txtVHomeUserName))
								.setText(fullname);
						ImageView imageRate = (ImageView) v
								.findViewById(R.id.imageViewRate);

						if (Utility.getUserPrefernce(mContext, "usertype") != null)

							if (Utility.getUserPrefernce(mContext, "usertype")
									.equalsIgnoreCase("user")) {

								{
									int rate = Math.round(Float.valueOf(map
											.get("overallrating")));
									String name = "review_" + rate + "_fill";
									int resId = getResources().getIdentifier(
											name, "drawable", getPackageName());
									imageRate.setImageResource(resId);
									imageRate.setVisibility(View.VISIBLE);
								}
							} else {
								imageRate.setVisibility(View.GONE);
							}

						imageview.setScaleType(ScaleType.FIT_XY);
						layoutImages.addView(v);

						layoutImages.setVisibility(View.VISIBLE);
						layoutBottomLikeDislike.setVisibility(View.VISIBLE);
						layoutBottomNorecord.setVisibility(View.GONE);

						layoutProgressMap.setVisibility(View.INVISIBLE);
						((RelativeLayout) findViewById(R.id.layoutImageFrames))
								.setVisibility(View.VISIBLE);
					}
				});

			}

			new Runnable() {
				@Override
				public void run() {

				}
			};

		}
	};

	private void setUsersOnMap() {

		final View markerMap = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.custom_marker_layout, null);
		final ImageView img = (ImageView) markerMap.findViewById(R.id.img_prof);

		final LatLngBounds.Builder builder = new LatLngBounds.Builder();

		for (int i = 0; i < WorldWidelistRecords.size(); i++) {
			final HashMap<String, String> profilemap = WorldWidelistRecords
					.get(i);

			String imageUrl = "";
			if (profilemap.get("photopath").contains("http")
					|| profilemap.get("photopath").contains("https")) {
				imageUrl = profilemap.get("photopath");
			} else {
				imageUrl = Utility.IMAGEURL + profilemap.get("photopath");
			}
			Bitmap bm = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
			bm = Utility.getBitmap(imageUrl);
			hasMarkerBitmap.put(imageUrl, bm);
			if (bm != null) {
				img.setImageBitmap(bm);
			} else {
				img.setImageDrawable(getResources().getDrawable(
						R.drawable.profile_default));
			}

			MarkerOptions markerOptions = new MarkerOptions();
			LatLng latLng = new LatLng(
					Double.parseDouble(profilemap.get("lat")),
					Double.parseDouble(profilemap.get("lng")));
			markerOptions.position(latLng);
			markerOptions.title(profilemap.get("fname") + "," + i);
			markerOptions.snippet(profilemap.get("location"));
			builder.include(latLng);
			markerOptions.icon(BitmapDescriptorFactory
					.fromBitmap(createDrawableFromView(this, markerMap)));
			markerOptions.anchor(0.5f, 1);
			this.map.addMarker(markerOptions);
		}

		map.setOnCameraChangeListener(new OnCameraChangeListener() {

			@Override
			public void onCameraChange(CameraPosition arg0) {
				// Move camera.
				map.moveCamera(CameraUpdateFactory.newLatLngBounds(
						builder.build(), 10));
				// Remove listener to prevent position reset on camera move.
				map.setOnCameraChangeListener(null);
			}
		});
		map.setInfoWindowAdapter(new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {

				return null;
			}

			@Override
			public View getInfoContents(Marker marker) {

				View myContentsView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
						.inflate(R.layout.custom_info_contents, null);
				ImageView imgProf = ((ImageView) myContentsView
						.findViewById(R.id.img_prof_info));
				TextView tvSnippet = ((TextView) myContentsView
						.findViewById(R.id.snippet));
				TextView tvTitle = ((TextView) myContentsView
						.findViewById(R.id.title));

				String[] str = marker.getTitle().split(",");
				tvTitle.setText(str[0]);
				HashMap<String, String> worldListRecordMap = WorldWidelistRecords
						.get(Integer.parseInt(str[1]));
				
				if (hasMarkerBitmap.get(worldListRecordMap.get("photopath")) != null) {
					if (worldListRecordMap.get("photopath").contains("http")
							|| worldListRecordMap.get("photopath").contains(
									"https")){
						imgProf.setImageBitmap(Utility
								.getBitmap(worldListRecordMap.get("photopath")));
					}else{
						imgProf.setImageBitmap(Utility
								.getBitmap(Utility.IMAGEURL+worldListRecordMap.get("photopath")));

					}
				} else {
					imgProf.setImageDrawable(getResources().getDrawable(
							R.drawable.profile_default));
				}

				tvSnippet.setText(marker.getSnippet());

				return myContentsView;
			}
		});

		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub
				try {
				String[] str = arg0.getTitle().split(",");
				HashMap<String, String> worldListRecordMap = WorldWidelistRecords
							.get(Integer.parseInt(str[1]));
 
					Intent intent = new Intent(mContext,
							OtherUserProfileActivityView.class);
					intent.putExtra("detail", worldListRecordMap);
					startActivity(intent);
				
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG)
							.show();
				}catch(Exception e)
				{
					Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG)
					.show();
				}
			}
			

		});

	}

	// Convert a view to bitmap
	public static Bitmap createDrawableFromView(Context context, View view) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(displayMetrics);
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels,
				displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
				view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);

		return bitmap;
	}

	public void drag(MotionEvent event, View v) {

		RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) v
				.getLayoutParams();

		switch (event.getAction()) {
		case MotionEvent.ACTION_MOVE: {
			params.topMargin = (int) event.getRawY() - (v.getHeight());
			params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
			v.setLayoutParams(params);
			break;
		}
		case MotionEvent.ACTION_UP: {
			params.topMargin = (int) event.getRawY() - (v.getHeight());
			params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
			v.setLayoutParams(params);
			break;
		}
		case MotionEvent.ACTION_DOWN: {
			v.setLayoutParams(params);
			break;
		}
		}
	}

	private class MarkAsFavOrUnFavorite extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				mProgressDialog = ProgressDialog.show(mContext, "",
						"Please wait", true, true,
						new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								MarkAsFavOrUnFavorite.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String url = null;
			if (markFav) {
				url = Utility.SERVERURL + "MarkAsFavorites";
				ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					listParams.add(new BasicNameValuePair("userid", params[0]));
					listParams.add(new BasicNameValuePair("advisorid", Utility
							.getUserPrefernce(mContext, "id")));
				} else {
					listParams.add(new BasicNameValuePair("advisorid",
							params[0]));
					listParams.add(new BasicNameValuePair("userid", Utility
							.getUserPrefernce(mContext, "id")));
				}
				listParams.add(new BasicNameValuePair("usertype", Utility
						.getUserPrefernce(mContext, "usertype")));
				String response = Utility
						.postParamsAndfindJSON(url, listParams);
				return response;

			} else if (!markFav) {
				url = Utility.SERVERURL + "MarkAsBlocked";
				url = url + "&senderid="
						+ Utility.getUserPrefernce(mContext, "id");
				if (Utility.getUserPrefernce(mContext, "usertype")
						.equalsIgnoreCase("advisor")) {
					url = url + "&senderusertype=advisor&blockeduserid="
							+ params[0];
				} else {
					url = url + "&senderusertype=user&blockeduserid="
							+ params[0];
				}
				String response = Utility.getJSONFromUrl(url);

				return response;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (mProgressDialog != null)
				mProgressDialog.cancel();

			if (result != null) {
				if (markFav) {
					if (result.contains("-")) {

						return;
					}

					if (result.equalsIgnoreCase("0")) {
						Toast.makeText(mContext, "Failed to favorite",
								Toast.LENGTH_SHORT).show();
						return;
					}

					if (markFav) {

						if (isbuttonPressed) {
							if (markFav) {
								swapRigh();
							}

							isbuttonPressed = false;
						} else {
							markFav = false;
							isblock = false;
							if (arrayViews.size() > 0)
								arrayViews.remove(arrayViews.size() - 1);
						}
						LocalBroadcastManager.getInstance(mContext)
								.sendBroadcast(new Intent("updateFav"));

						if (arrayViews.size() == 0) {
							layoutImages.setVisibility(View.INVISIBLE);
							layoutBottomLikeDislike.setVisibility(View.GONE);
							layoutBottomNorecord.setVisibility(View.VISIBLE);
							layoutProgressMap.setVisibility(View.VISIBLE);
						}

					} else if (!markFav) {
						if (isbuttonPressed) {
							swapLeft();
							isbuttonPressed = false;
						} else {
							isblock = false;
							if (arrayViews.size() > 0)
								arrayViews.remove(arrayViews.size() - 1);
						}
						if (arrayViews.size() == 0) {
							layoutImages.setVisibility(View.INVISIBLE);
							layoutBottomLikeDislike.setVisibility(View.GONE);
							layoutBottomNorecord.setVisibility(View.VISIBLE);
							layoutProgressMap.setVisibility(View.VISIBLE);
						}
					}
				}

			} else {
				Toast.makeText(mContext,
						"Please check your Internet connection",
						Toast.LENGTH_SHORT).show();

			}
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		detectore.onTouchEvent(event);
		int eid = event.getAction();
		LinearLayout ll = (LinearLayout) v;
		RelativeLayout rl = (RelativeLayout) ((LinearLayout) v).getChildAt(0);
		ImageView imgLike = (ImageView) rl.getChildAt(3);
		ImageView imgNope = (ImageView) rl.getChildAt(4);
		Matrix matrix = new Matrix();
		switch (eid) {
		case MotionEvent.ACTION_MOVE:
			PointF mv = new PointF(event.getX() - DownPT.x, event.getY()
					- DownPT.y);
			v.setX((int) (StartPT.x + mv.x));
			v.setY((int) (StartPT.y + mv.y));
			StartPT = new PointF(v.getX(), v.getY());

			if (v.getX() < 0) {

				matrix.postRotate(90);
				if (isRightAnimationRunnning && !isLeftAnimationRunnning) {
					isRightAnimationRunnning = false;
					isLeftAnimationRunnning = true;
					imgNope.startAnimation(show);
					imgLike.startAnimation(hide);
				}

				if (!isRightAnimationRunnning && !isLeftAnimationRunnning) {
					isRightAnimationRunnning = false;
					isLeftAnimationRunnning = true;
					imgLike.startAnimation(hide);
					imgNope.startAnimation(show);
				}

				//
				imgLike.setVisibility(View.VISIBLE);
				imgNope.setVisibility(View.INVISIBLE);
				//
			} else if (v.getX() > 0) {

				if (!isRightAnimationRunnning && isLeftAnimationRunnning) {
					isRightAnimationRunnning = true;
					isLeftAnimationRunnning = false;
					imgLike.startAnimation(show);
					imgNope.startAnimation(hide);

				}
				if (!isRightAnimationRunnning && !isLeftAnimationRunnning) {
					isRightAnimationRunnning = true;
					isLeftAnimationRunnning = false;
					imgLike.startAnimation(show);
					imgNope.startAnimation(hide);
				}

				imgLike.setVisibility(View.VISIBLE);
				imgNope.setVisibility(View.INVISIBLE);
			}
			break;
		case MotionEvent.ACTION_DOWN:
			DownPT.x = event.getX();
			DownPT.y = event.getY();
			StartPT = new PointF(v.getX(), v.getY());
			break;
		case MotionEvent.ACTION_UP:
			// Nothing have to do

			if (v.getX() > -100 && v.getX() < 200) {
				v.setX(0);
				v.setY(0);
				v.invalidate();
				imgLike.setVisibility(View.INVISIBLE);
				imgNope.setVisibility(View.INVISIBLE);
				imgLike.startAnimation(hide);
				imgNope.startAnimation(hide);

			} else {

				if (v.getX() < 20) {
					if (!isblock) {
						isblock = true;
						Intent in = new Intent("nope");
						in.putExtra("tag", (Integer) v.getTag());
						v.setX(-1000);
						v.setVisibility(View.GONE);
						LocalBroadcastManager.getInstance(
								CenterHomeActivity.this).sendBroadcast(in);
					}
				} else if (v.getX() > 200) {
					if (!markFav) {
						v.setX(1000);
						markFav = true;
						Intent in = new Intent("liked");
						in.putExtra("tag", (Integer) v.getTag());

						LocalBroadcastManager.getInstance(
								CenterHomeActivity.this).sendBroadcast(in);
					}

				}

			}

			break;
		default:
			break;
		}

		return true;
	}

	public void swapRigh() {

		final View v = arrayViews.get(arrayViews.size() - 1);

		RelativeLayout relativeLayout = (RelativeLayout) ((LinearLayout) v)
				.getChildAt(0);

		ImageView image1 = (ImageView) relativeLayout.getChildAt(3);
		image1.startAnimation(show);
		AnimationSet set = new AnimationSet(true);
		set.setFillAfter(true);
		TranslateAnimation moveLefttoRight = new TranslateAnimation(0, 1000, 0,
				0);
		moveLefttoRight.setStartOffset(100);
		moveLefttoRight.setDuration(1000);
		set.addAnimation(moveLefttoRight);
		set.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

				v.setVisibility(View.GONE);
				arrayViews.remove(arrayViews.size() - 1);

				if (arrayViews.size() == 0) {
					layoutBottomLikeDislike.setVisibility(View.GONE);
					layoutBottomNorecord.setVisibility(View.VISIBLE);
					layoutImages.setVisibility(View.INVISIBLE);
					layoutProgressMap.setVisibility(View.VISIBLE);
				}
			}
		});
		v.startAnimation(set);
		//
	}

	public void swapLeft() {
		final View v = arrayViews.get(arrayViews.size() - 1);

		RelativeLayout relativeLayout = (RelativeLayout) ((LinearLayout) v)
				.getChildAt(0);

		ImageView image2 = (ImageView) relativeLayout.getChildAt(4);

		image2.startAnimation(show);

		AnimationSet set = new AnimationSet(true);
		set.setFillAfter(true);
		TranslateAnimation moveLefttoRight = new TranslateAnimation(0, -1000,
				0, 0);
		moveLefttoRight.setStartOffset(100);
		moveLefttoRight.setDuration(1000);
		set.addAnimation(moveLefttoRight);
		v.startAnimation(set);
		set.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

				v.setVisibility(View.GONE);
				arrayViews.remove(arrayViews.size() - 1);
				if (arrayViews.size() == 0) {
					layoutBottomLikeDislike.setVisibility(View.GONE);
					layoutBottomNorecord.setVisibility(View.VISIBLE);
					layoutImages.setVisibility(View.INVISIBLE);
					layoutProgressMap.setVisibility(View.VISIBLE);
				}

			}
		});
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		final HashMap<String, String> map = listRecords
				.get(arrayViews.size() - 1);
		Intent intent = new Intent(mContext, OtherUserProfileActivityView.class);
		int requestfor = (map.get("userid") != null) ? Constants.USER
				: Constants.ADVISOR;
		intent.putExtra("requestFor", requestfor);
		intent.putExtra("broadcast", "updatsearch");
		intent.putExtra("detail", map);
		startActivity(intent);

		return false;
	}

}
