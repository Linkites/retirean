package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.SignInActivity.SignInUser;
import com.linkites.retire.utility.Utility;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.backup.RestoreObserver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ResetPassword extends Activity {

	Button btnLeadUser;
	Button btnFinancialAdvisor;
	TextView email;
	Button btnSend;
	String userType = null;
	private ProgressDialog mBProgressBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.reset_password);
		
		btnLeadUser = (Button)findViewById(R.id.btnLeadUser);
		btnFinancialAdvisor = (Button)findViewById(R.id.btnFinancialAdvisor);
		email = (TextView)findViewById(R.id.txtVEmail);
		btnSend = (Button)findViewById(R.id.btnSend);
		TextView txtVcancel = (TextView)findViewById(R.id.txtVCancel);
		txtVcancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		btnLeadUser.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userType="LeadUser";
				btnLeadUser.setBackgroundResource(R.drawable.blue_back_left_rounded_corner);
				btnLeadUser.setTextColor(Color.WHITE);
				btnFinancialAdvisor.setBackgroundResource(R.drawable.white_back_right_rounded_corner);
				btnFinancialAdvisor.setTextColor(Color.rgb(0, 122, 255));
			}
		});
		
		btnFinancialAdvisor.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				userType = "FinancialAdvisor";
				btnFinancialAdvisor.setBackgroundResource(R.drawable.blue_back_right_rounded_corner);
				btnFinancialAdvisor.setTextColor(Color.WHITE);
				btnLeadUser.setBackgroundResource(R.drawable.white_back_left_rounded_corner);
				btnLeadUser.setTextColor(Color.rgb(0, 122, 255));
			}
		});
		
		btnSend.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(userType==null){
					Utility.ShowAlertWithMessage(ResetPassword.this, "Alert", "Please Select UserType");					
					return;
				}else if(email.getText().toString().trim().equals("")){
					Utility.ShowAlertWithMessage(ResetPassword.this, "Alert", "Please Enter Email Address");					
					return;
				}else{
					new ForgatePassword().execute();
				}
			}
		});
	}
	
	public class ForgatePassword extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				mBProgressBar = ProgressDialog.show(ResetPassword.this,
						"Sending Request", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								ForgatePassword.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

			listParams.add(new BasicNameValuePair("email", email.getText().toString()));

			listParams.add(new BasicNameValuePair("usertype", userType));
			String serverUrl = Utility.SERVERURL + "ForgetPassword";
			String response = Utility.postParamsAndfindJSON(serverUrl, listParams);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();
			
			if (result != null) {
				if(result.trim().equals("\"1\"")){
					AlertDialog.Builder builder = new AlertDialog.Builder(ResetPassword.this);
					builder.setCancelable(false);
					builder.setTitle("Success");
					builder.setMessage("Your Password Has Been Reset Successfully, please check your email");
					builder.setNegativeButton("ok",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,	int which) {
									finish();
								}
							});					
					AlertDialog alert = builder.create();
					try {
						alert.show();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(result.equals("\"-2\"")){
					Utility.showAlert(ResetPassword.this, "Alert","There is no any account Associated with this Email");
				}
				else if(result.equals("\"0\"") || result.equals("\"-1\"")){
					Utility.showAlert(ResetPassword.this, "Alert","Some Error Occured, please try after some time.");
				}

			} else {
				Utility.showAlert(ResetPassword.this, "Alert",getResources().getString(R.string.nonetwork));
			}
		}
	}
}
