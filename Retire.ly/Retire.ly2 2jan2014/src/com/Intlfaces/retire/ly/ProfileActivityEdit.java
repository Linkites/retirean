package com.Intlfaces.retire.ly;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.util.ImageDownloader;
import com.linkites.retire.util.ImageDownloader.Mode;
import com.linkites.retire.utility.Base64Coder;

import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivityEdit extends FragmentActivity {
	private EditText editTextFname, editTextLname, editTextAge,
			editTextLocation, editTextFirm, editTextEmail, editTextPhone,
			editTextWebsitee, editTextOccupation, editTextAmount, editTextBio,
			editTextUniversity, editTextAdvisorOffice;
	private LinearLayout layoutmain;
	private Bitmap bitmap;
	private Animation shake;
	private boolean isServiceRunning;
	private ProgressDialog mProgressDialog;

	private Context mContext;
	private int requestFor;
	private double lat, lng;
	private ImageView imageView;
	private final int TAKE_PICTURE = 0x01;
	private final int PICK_PICTURE = 0x02;
	private Uri imageUri;
	private ProgressDialog progressDialog;
	private String gender;
	private Button btnUpdatePrfile, btnRemoveImage, btnContactMenu,
			btnProfessionalMenu, btnPicEdit;
	private TextView txtFnameText;
	private RelativeLayout rlayoutImage;
	private boolean isImageChange;
	private LinearLayout layoutTab1, layoutTab2;
	HashMap<String, String> Infomap = new HashMap<String, String>();
	LayoutInflater inflater;
	private View parentView;
	ImageButton btnuserMenuProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_user_new);
		requestFor = this.getIntent().getIntExtra("requestFor", 0);
		mContext = this;
		String Map = Utility.getUserPrefernce(mContext, "userprofileinfo");
		JSONObject json;
		try {
			json = new JSONObject(Map);
			Infomap = Utility.toMap(json);
			Log.e("Info Map data in profile edit class", "" + Infomap);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (Utility.getUserPrefernce(mContext, "usertype").equalsIgnoreCase(
				"advisor")) {
			setupAdvisor();
		} else {
			setupUser();
		}

		// hide default image of navigation bar
		((ImageView) findViewById(R.id.ImageViewNavTitle))
				.setVisibility(View.INVISIBLE);

		((ImageButton) findViewById(R.id.btn_BackHome))
				.setImageResource(R.drawable.btn_block_content);

		((ImageButton) findViewById(R.id.btn_BackHome))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						onBackPressed();
					}
				});

	btnuserMenuProfile = (ImageButton) findViewById(R.id.userMenuProfile);

		btnuserMenuProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnuserMenuProfile.setEnabled(false);
				updateUserProfile();
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void showAlert() {
		AlertDialog.Builder alert = new Builder(mContext);
		alert.setTitle("Pic Photo");
		alert.setPositiveButton("Camera",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						takeCameraPhoto();
					}
				});
		alert.setNeutralButton("Gallery",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						takeGalleryPhoto();
					}
				});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		alert.show();

	}

	private Runnable downloadpic = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String url = null;
			if (Infomap.get("photopath") != null) {
				if (Infomap.get("photopath").contains("http")
						|| Infomap.get("photopath").contains("https")) {
					url = Infomap.get("photopath");
				} else {
					url = Utility.IMAGEURL + Infomap.get("photopath");
				}
				bitmap = Utility.getBitmap(url);
			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					if (bitmap != null) {
						imageView.setImageBitmap(bitmap);
						// Utility.setUserPrefernce(
						// mContext,
						// "image_"
						// + Utility.getUserPrefernce(mContext,
						// "id"),
						// Base64Coder.encodeTobase64(bitmap));
					}
				}
			});
		}
	};

	private void takeCameraPhoto() {
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		File photo = new File(Environment.getExternalStorageDirectory(),
				"Pic.jpg");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
		imageUri = Uri.fromFile(photo);
		startActivityForResult(intent, TAKE_PICTURE);
	}

	private void takeGalleryPhoto() {

		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				PICK_PICTURE);
	}

	private void CheckChanges() {
		Infomap.clear();

		if (requestFor == Constants.ADVISOR) {
			Infomap.put("firm", editTextFirm.getText().toString());
			Infomap.put("website", editTextWebsitee.getText().toString());
			Infomap.put("professionalbio", editTextBio.getText().toString());

		} else {
			Infomap.put("amount",
					Utility.parseToString(editTextAmount.getText().toString()));
			Infomap.put("occupation", editTextOccupation.getText().toString());
		}

		Infomap.put("fname", editTextFname.getText().toString());
		Infomap.put("lname", editTextLname.getText().toString());
		Infomap.put("location", editTextLocation.getText().toString());
		Infomap.put("age", editTextAge.getText().toString());
		Infomap.put("phone", editTextPhone.getText().toString());
		Infomap.put("email", editTextEmail.getText().toString());
		Infomap.put("gender", gender);
		boolean isChanges = false;
		HashMap<String, String> oldMap = null;
		try {
			oldMap = Utility.toMap(new JSONObject(Utility.getUserPrefernce(
					mContext, "userinfo")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<?> setkey = oldMap.keySet();
		Iterator<?> keys = setkey.iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();

			if (!key.equalsIgnoreCase("advisorid")
					&& !key.equalsIgnoreCase("userid")
					&& !key.equalsIgnoreCase("id")) {
				if (oldMap.get(key) != null && Infomap.get(key) != null)
					if (!oldMap.get(key).equalsIgnoreCase(Infomap.get(key))) {
						isChanges = true;
					}
			}
		}

		if (isChanges || isImageChange) {
			updateUserProfile();
		}
	}

	private void updateUserProfile() {

		if (!isServiceRunning) {
			final UpdateProfile update = new UpdateProfile();
			try {
				mProgressDialog = ProgressDialog.show(mContext,
						"Updating profile", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								update.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
			update.execute("");
		}
		
		
	}

	private void setupAdvisor() {
		mContext = this;
		inflater = LayoutInflater.from(this);
		parentView = inflater.inflate(R.layout.activity_profile_advisor, null);

		((RelativeLayout) parentView.findViewById(R.id.layoutFileds))
				.setVisibility(View.GONE);
		((LinearLayout) parentView.findViewById(R.id.layoutChanegPhoto))
				.setVisibility(View.VISIBLE);

		Button btnTakePhoto = (Button) parentView
				.findViewById(R.id.btnChangePhoto);
		btnTakePhoto.setVisibility(View.VISIBLE);
		btnTakePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showAlert();
			}
		});
		((LinearLayout) findViewById(R.id.layoutProfile)).addView(parentView);
		imageView = (ImageView) parentView.findViewById(R.id.ImageHeaderPic);
		// if (Utility.getUserPrefernce(mContext,
		// "image_" + Utility.getUserPrefernce(mContext, "id")) == null) {
		// new Thread(downloadpic).start();
		//
		// } else {
		// bitmap = Base64Coder.decodeBase64(Utility.getUserPrefernce(
		// mContext,
		// "image_" + Utility.getUserPrefernce(mContext, "id")));
		// imageView.setImageBitmap(bitmap);
		// }
	
		LinearLayout layout = (LinearLayout) parentView
				.findViewById(R.id.editProfileFieldLayout);
		layout.setVisibility(View.VISIBLE);

		String url = null;
		if (Infomap.get("photopath") != null)
			if (Infomap.get("photopath").contains("http")
					|| Infomap.get("photopath").contains("https")) {
				url = Infomap.get("photopath");
			} else {
				url = Utility.IMAGEURL + Infomap.get("photopath");
			}

		if (url != null) {
			ImageDownloader imageLoaaderr = new ImageDownloader();
			imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
			imageLoaaderr.download(url, imageView, BitmapFactory
					.decodeResource(getResources(), R.drawable.dummy));
		}

		Typeface typface = Typeface.createFromAsset(getAssets(),
				"fonts/GeosansLight.ttf");

		try {
			Infomap = Utility.toMap(new JSONObject(Utility.getUserPrefernce(
					mContext, "userprofileinfo")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Infomap.remove("password");

		if ((Infomap.get("gender").equalsIgnoreCase("Male") || Infomap.get(
				"gender").equalsIgnoreCase("1"))) {
			gender = "Male";
		} else {
			gender = "Female";
		}
		// Advisor Basic info

		lat = Double.parseDouble(Infomap.get("lat"));
		lng = Double.parseDouble(Infomap.get("lat"));
		
		editTextFirm = (EditText)parentView.findViewById(R.id.editTextAdvisorFIRM);
		editTextFirm.setText((Infomap.get("firm").trim().length()==0)?"Not Available":Infomap.get("firm"));
		editTextFirm.setFocusable(true);
		editTextFirm.setClickable(true);
		
		
		
		(editTextFname = (EditText) parentView
				.findViewById(R.id.editextHeaderFname)).setText(Infomap
				.get("fname"));
		editTextFname.setTypeface(typface);
		editTextFname.setFocusable(true);
		editTextFname.setClickable(true);
		(editTextLname = (EditText) parentView
				.findViewById(R.id.editextHeaderLname)).setText(Infomap
				.get("lname"));

		editTextLname.setFocusable(true);
		editTextLname.setClickable(true);
		editTextLname.setTypeface(typface);

		(editTextLocation = (EditText) parentView
				.findViewById(R.id.editTextHeaderLocation)).setText(Infomap
				.get("location"));
		editTextLocation.setFocusable(true);
		editTextLocation.setClickable(true);
		editTextLocation.setTypeface(typface);
	
		(editTextAge = (EditText) parentView
				.findViewById(R.id.editTextHeaderAge)).setText(Infomap
				.get("age"));
		editTextAge.setFocusable(true);
		editTextAge.setClickable(true);
		editTextAge.setTypeface(typface);
	
		// Advisor Professional info
		(editTextAdvisorOffice = (EditText) parentView
				.findViewById(R.id.editTextAdvisorOfficeAddress))
				.setText(Infomap.get("currentoffice"));
		editTextAdvisorOffice.setFocusable(true);
		editTextAdvisorOffice.setClickable(true);
		editTextAdvisorOffice.setTypeface(typface);
	
		(editTextBio = (EditText) parentView
				.findViewById(R.id.editTextAdvisorProffInfo)).setText(Infomap
				.get("professionalbio"));
		editTextBio.setFocusable(true);
		editTextBio.setClickable(true);
		editTextBio.setTypeface(typface);
		editTextBio.setEnabled(true);

		// Advisor Contact info
		(editTextEmail = (EditText) parentView
				.findViewById(R.id.editTextAdvisorMail)).setText(Infomap
				.get("email"));
		editTextEmail.setFocusable(true);
		editTextEmail.setClickable(true);
		editTextEmail.setTypeface(typface);
	
		(editTextPhone = (EditText) parentView
				.findViewById(R.id.editTextAdvisorPhone)).setText(Infomap
				.get("phone"));
		editTextPhone.setFocusable(true);
		editTextPhone.setClickable(true);
		editTextPhone.setTypeface(typface);
	
		(editTextWebsitee = (EditText) parentView
				.findViewById(R.id.editTextAdvisorWebsite)).setText(Infomap
				.get("website"));
		editTextWebsitee.setFocusable(true);
		editTextWebsitee.setClickable(true);
		editTextWebsitee.setTypeface(typface);
	
		editTextOccupation = (EditText) parentView
				.findViewById(R.id.editTextAdvisorDesignation);
		editTextOccupation.setFocusable(true);
		editTextOccupation.setClickable(true);
		editTextOccupation.setTypeface(typface);
		editTextOccupation.setText(Infomap.get("registeredtitle"));

		editTextUniversity = (EditText) parentView
				.findViewById(R.id.editTextAdvisorUniversity);
		editTextUniversity.setFocusable(true);
		editTextUniversity.setClickable(true);
		editTextUniversity.setTypeface(typface);
		editTextUniversity.setText(Infomap.get("university"));

		editTextAdvisorOffice = (EditText) parentView
				.findViewById(R.id.editTextAdvisorOfficeAddress);
		editTextAdvisorOffice.setText(Infomap.get("currentoffice"));
		editTextAdvisorOffice.setFocusable(true);
		editTextAdvisorOffice.setClickable(true);
		editTextAdvisorOffice.setTypeface(typface);

		((ImageView) parentView.findViewById(R.id.ImageHeaderPic))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						showAlert();
					}
				});

	}

	private void setupUser() {
		mContext = this;
		inflater = LayoutInflater.from(this);

		parentView = inflater.inflate(R.layout.activity_user_detail, null);
		((LinearLayout) findViewById(R.id.layoutProfile)).addView(parentView);

		Button btnTakePhoto = (Button) parentView
				.findViewById(R.id.btnChangePhoto);
		btnTakePhoto.setVisibility(View.VISIBLE);
		btnTakePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showAlert();
			}
		});
		imageView = (ImageView) parentView
				.findViewById(R.id.ImageHeaderPicuser);

		String url = null;
		if (Infomap.get("photopath") != null)
			if (Infomap.get("photopath").contains("http")
					|| Infomap.get("photopath").contains("https")) {
				url = Infomap.get("photopath");
			} else {
				url = Utility.IMAGEURL + Infomap.get("photopath");
			}

		if (url != null) {
			ImageDownloader imageLoaaderr = new ImageDownloader();
			imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
			imageLoaaderr.download(url, imageView, BitmapFactory
					.decodeResource(getResources(), R.drawable.dummy));
		}

		// if (Utility.getUserPrefernce(mContext,
		// "image_" + Utility.getUserPrefernce(mContext, "id")) == null) {
		// new Thread(downloadpic).start();
		//
		// } else {
		// //bitmap = Base64Coder.decodeBase64(Utility.getUserPrefernce(
		// // mContext,
		// //"image_" + Utility.getUserPrefernce(mContext, "id")));
		// //imageView.setImageBitmap(bitmap);
		// }

		LinearLayout layout = new LinearLayout(mContext);
		layout = (LinearLayout) parentView
				.findViewById(R.id.editProfileFieldLayout);
		layout.setVisibility(View.VISIBLE);
		Typeface typface = Typeface.createFromAsset(getAssets(),
				"fonts/GeosansLight.ttf");

		if (Infomap.get("lat") != null)
			lat = Double.parseDouble(Infomap.get("lat"));
		if (Infomap.get("lng") != null)
			lng = Double.parseDouble(Infomap.get("lng"));

		if (Infomap.get("gender") == "1") {
			gender = "Male";
		} else {
			gender = "Female";
		}

		((RelativeLayout) parentView.findViewById(R.id.layoutFileds))
				.setVisibility(View.GONE);
		((LinearLayout) parentView.findViewById(R.id.layoutChanegPhoto))
				.setVisibility(View.VISIBLE);

		TextView txtvLocation = (TextView) parentView
				.findViewById(R.id.txtLocationUser);
		txtvLocation.setVisibility(View.GONE);
		((TextView) parentView.findViewById(R.id.txtAgeUser))
				.setVisibility(View.GONE);
		(editTextFname = (EditText) parentView
				.findViewById(R.id.editextHeaderFname)).setText(Infomap
				.get("fname"));
		editTextFname.setFocusable(true);
		editTextFname.setClickable(true);
		editTextFname.setTypeface(typface);

		(editTextLname = (EditText) parentView
				.findViewById(R.id.editextHeaderLname)).setText(Infomap
				.get("lname"));
		editTextLname.setTypeface(typface);
		editTextLname.setFocusable(true);
		editTextLname.setClickable(true);

		(editTextLocation = (EditText) parentView
				.findViewById(R.id.editTextHeaderLocation)).setText(Infomap
				.get("location"));
		editTextLocation.setFocusable(true);
		editTextLocation.setClickable(true);
		editTextLocation.setTypeface(typface);
		editTextLocation.setBackgroundResource(android.R.color.transparent);

		editTextAge = (EditText) parentView
				.findViewById(R.id.editTextHeaderAge);
		if (Infomap.get("age").trim().length() > 0)
			editTextAge.setText(Infomap.get("age"));

		editTextAge.setFocusable(true);
		editTextAge.setClickable(true);
		editTextAge.setTypeface(typface);
		editTextAge.setBackgroundResource(android.R.color.transparent);

		(editTextOccupation = (EditText) parentView
				.findViewById(R.id.editTextUserOccupation)).setText(Infomap
				.get("occupation"));
		editTextOccupation.setTypeface(typface);
		editTextOccupation.setBackgroundResource(android.R.color.transparent);
		editTextOccupation.setFocusable(true);
		editTextOccupation.setClickable(true);
		(editTextUniversity = (EditText) parentView
				.findViewById(R.id.editTextUserUniversity)).setText(Infomap
				.get("university"));
		editTextUniversity.setTypeface(typface);
		editTextUniversity.setBackgroundResource(android.R.color.transparent);
		editTextUniversity.setFocusable(true);
		editTextUniversity.setClickable(true);
		if (Infomap.get("investableamout") != null) {
			(editTextAmount = (EditText) parentView
					.findViewById(R.id.editTextUserAmount)).setText(Infomap
					.get("investableamout"));
			editTextAmount.setFocusable(true);
			editTextAmount.setClickable(true);
		} else if (Infomap.get("amount") != null) {
			(editTextAmount = (EditText) parentView
					.findViewById(R.id.editTextUserAmount)).setText(Infomap
					.get("amount"));
			
			editTextAmount.setFocusable(true);
			editTextAmount.setClickable(true);

		}

		// User Contact info
		(editTextEmail = (EditText) parentView
				.findViewById(R.id.editTextUserMailuser)).setText(Infomap
				.get("email"));
		editTextEmail.setTypeface(typface);
		editTextEmail.setFocusable(true);
		editTextEmail.setClickable(true);

		(editTextPhone = (EditText) parentView
				.findViewById(R.id.editTextUserPhoneuser)).setText(Infomap
				.get("phone"));
		editTextPhone.setTypeface(typface);
		editTextPhone.setFocusable(true);
		editTextPhone.setClickable(true);

		((ImageView) parentView.findViewById(R.id.ImageHeaderPicuser))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						showAlert();
					}
				});

	}

	private class UpdateProfile extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			isServiceRunning = true;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			// Store new value in has map for update
			if (Infomap.containsKey("email"))
				Infomap.remove("email");

			Log.d("", "params " + Infomap);
			String serverUrl = Utility.SERVERURL;
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			// "phone":"2575257","linkedinid":"","location":"","lng":"75.8790337","date":"2014-01-09 03:07:32","password":"e10adc3949ba59abbe56e057f20f883e","online":"1","socialpageurl":"","registeredtitle":"","age":"24","ratingcount":"0","gender":"Female","fbid":"","lat":"22.6982392","photopath":"","fname":"jacob","searchkeywords":"","university":"","lname":"luke","status":"10","website":"www.google.com","firm":"indore","overallrating":"0","currentoffice":"indore","devicetoken":"APA91bFUNVk41aZfho0NVe5ez3SsezZ-Fs3S3a-tMojeWzaGFwOUIRXjzBb06Fo62J7ChfPjrZEZU0FN0YeENw3AYel4Ez-H40UY","professionalbio":"test","purchasedate":"2014-01-09 03:07:32","email":"jacob@gmail.com","reviewCount":"0","devicetype":"","advisorid":"106"}
			if (Utility.getUserPrefernce(mContext, "usertype")
					.equalsIgnoreCase("advisor")) {
				serverUrl = serverUrl + "UpdateAdvisorProfile";
				listParams.add(new BasicNameValuePair("advisorid", Utility
						.getUserPrefernce(mContext, "id")));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				
				listParams.add(new BasicNameValuePair("firm", editTextFirm
						.getText().toString()));
				
				listParams.add(new BasicNameValuePair("website",
						editTextWebsitee.getText().toString()));
				
				listParams.add(new BasicNameValuePair("professionalbio",
						editTextBio.getText().toString()));
				
				listParams.add(new BasicNameValuePair("title",
						editTextOccupation.getText().toString()));
				
				listParams.add(new BasicNameValuePair("university",
						editTextUniversity.getText().toString()));
				
				listParams.add(new BasicNameValuePair("currentoffice",
						editTextAdvisorOffice.getText().toString()));
				
			} else {

				serverUrl = serverUrl + "UpdateUserProfile";
				
				listParams.add(new BasicNameValuePair("userid", Utility
						.getUserPrefernce(mContext, "id")));
				listParams.add(new BasicNameValuePair("lat", "" + Utility.getUserIdPrefernce(mContext, "latd")));
				listParams.add(new BasicNameValuePair("lng", "" + Utility.getUserIdPrefernce(mContext, "long")));
				listParams.add(new BasicNameValuePair("amount", Utility
						.parseToString(editTextAmount.getText().toString())));
			
				String occupation = editTextOccupation.getText().toString();
				if(occupation==null)
					occupation = "";
				
				listParams.add(new BasicNameValuePair("occupation",
						occupation));
			
				listParams.add(new BasicNameValuePair("university",
						editTextUniversity.getText().toString()));
			}

			listParams.add(new BasicNameValuePair("fname", editTextFname
					.getText().toString()));
			listParams.add(new BasicNameValuePair("lname", editTextLname
					.getText().toString()));
			listParams.add(new BasicNameValuePair("location", editTextLocation
					.getText().toString()));
			listParams.add(new BasicNameValuePair("age", editTextAge.getText()
					.toString()));
			listParams.add(new BasicNameValuePair("phone", editTextPhone
					.getText().toString()));
			listParams.add(new BasicNameValuePair("email", editTextEmail
					.getText().toString()));
			listParams.add(new BasicNameValuePair("gender", gender));
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);

			return response;
		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			UpdateProfile.this.cancel(true);
			btnuserMenuProfile.setEnabled(true);
			isServiceRunning = false;

			if (result != null) {
				if (result.contains("-")) {
					Utility.showAlert(mContext, "Alert", "Email Already exists");
					if (mProgressDialog != null)
						mProgressDialog.cancel();

					return;
				} else if (result.contains("1")) {
					Toast.makeText(mContext, "Profile Updated",
							Toast.LENGTH_LONG).show();
					Infomap.put("email", editTextEmail.getText().toString());
					Utility.setUserPrefernce(mContext, "userinfo",
							new JSONObject(Infomap).toString());

					Timer tm = new Timer();
					tm.schedule(new TimerTask() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									new Thread(updateProfile).start();
								}
							});
						}
					}, 100);
				}
			} else {
				if (mProgressDialog != null)
					mProgressDialog.cancel();

			}

		}

	}

	Runnable updateProfile = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			//http://retire.ly/mobile/api/v1/api.php?method=GetAdvisorDetail&advisorid=
			String serverUrl = Utility.SERVERURL;
			String key = null, subKey = null;
			if (Utility.getUserPrefernce(mContext, "usertype")
					.equalsIgnoreCase("advisor")) {
				serverUrl = serverUrl + "GetAdvisorDetail&advisorid="
						+ Utility.getUserPrefernce(mContext, "id");
				key = "advisors";
				subKey = "advisor";
			} else {

				serverUrl = serverUrl + "GetUserDetail&userid="
						+ Utility.getUserPrefernce(mContext, "id");
				key = "users";
				subKey = "user";
			}
			final String response = Utility.getJSONFromUrl(serverUrl);
			if (response != null) {
				JSONObject json;
				try {
					json = new JSONObject(response);
					JSONArray jarray = json.getJSONArray(key);
					json = jarray.getJSONObject(0);
					json = json.getJSONObject(subKey);
					Infomap = Utility.toMap(json);
					Utility.setUserPrefernce(mContext, "userprofileinfo",
							json.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (mProgressDialog != null) {
						mProgressDialog.cancel();
					}
					
					Utility.setUserPrefernce(mContext, "userimg", null);
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("updatePhoto"));
					
					Utility.setBooleanprefence(mContext, "profileupdate", true);
					if (response != null) {
						if (requestFor == Constants.ADVISOR) {
							setupAdvisor();
						} else {
							setupUser();
						}
					}
				}
			});
		}
	};

	private class UploadProfilePicture extends AsyncTask<Bitmap, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mProgressDialog = ProgressDialog.show(mContext,
					"Uploading Profile Photo", "Please wait...", true, true,
					new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							UploadProfilePicture.this.cancel(true);
						}
					});
		}

		@Override
		protected String doInBackground(Bitmap... params) {
			// TODO Auto-generated method stub
			String photo = Base64Coder.encodeTobase64(params[0]);
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

			if (Utility.getUserPrefernce(mContext, "usertype")
					.equalsIgnoreCase("user")) {
				listParams.add(new BasicNameValuePair("userid", Utility
						.getUserPrefernce(mContext, "id")));
				listParams.add(new BasicNameValuePair("usertype", "user"));
			} else {
				listParams.add(new BasicNameValuePair("advisorid", Utility
						.getUserPrefernce(mContext, "id")));
				listParams.add(new BasicNameValuePair("usertype", "advisor"));
			}
			listParams.add(new BasicNameValuePair("photo", photo));

			String url = Utility.SERVERURL + "UpdatePhoto";
			String response = Utility.postParamsAndfindJSON(url, listParams);
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result != null) {
				if (result.contains("0")) {
					Utility.ShowAlertWithMessage(mContext, "Alert",
							"Photo upload failed");
					if (mProgressDialog != null)
						mProgressDialog.cancel();

				}
				if (result.contains("1")) {
					Utility.ShowAlertWithMessage(mContext, "Alert",
							"Your Profile photo has been updated Successfully.");
					Timer tm = new Timer();
					tm.schedule(new TimerTask() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									new Thread(updateProfile).start();
									
								}
							});
						}
					}, 100);
				}
			} else {

				Utility.ShowAlertWithMessage(mContext, "Alert",
						"Please check your internet connection.");
				if (mProgressDialog != null)
					mProgressDialog.cancel();

			}

		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == TAKE_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {

				getContentResolver().notifyChange(imageUri, null);

				ContentResolver cr = getContentResolver();

				try {

					Bitmap mPhoto = android.provider.MediaStore.Images.Media
							.getBitmap(cr, imageUri);
					mPhoto = Bitmap.createScaledBitmap(mPhoto, 320, 320, false);
					imageView.setImageBitmap(mPhoto);
					new UploadProfilePicture().execute(mPhoto);

				} catch (Exception e) {

					Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT)
							.show();

				}

			}

		}

		if (requestCode == PICK_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {
				try {
					if (data.getData() != null) {
						Uri _uri = data.getData();
						Bitmap bmp = null;
						if (_uri != null) {
							Cursor cursor = getContentResolver()
									.query(_uri,
											new String[] { android.provider.MediaStore.Images.ImageColumns.DATA },
											null, null, null);
							cursor.moveToFirst();

							String imageFilePath = cursor.getString(0);
							File photo = new File(imageFilePath);
							if (photo.exists()) {
								bmp = BitmapFactory.decodeFile(photo
										.getAbsolutePath());
								bmp = Bitmap.createScaledBitmap(bmp, 320, 320,
										false);
							}
							imageView.setImageBitmap(bmp);
							if(bmp!=null)
							new UploadProfilePicture().execute(bmp);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.toString();
				}
			}
		}

	};

}
