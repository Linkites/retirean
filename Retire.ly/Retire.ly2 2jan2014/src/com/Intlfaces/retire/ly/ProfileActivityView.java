package com.Intlfaces.retire.ly;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.util.ImageDownloader;
import com.linkites.retire.util.ImageDownloader.Mode;
import com.linkites.retire.utility.TextViewCustom;

import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ProfileActivityView extends FragmentActivity {
	private int requestFor;
	@SuppressWarnings("unused")
	private TextViewCustom txtvFname, txtvLname, txtvAge, txtvLocation, txtvFirm,
			txtvTitle, txtReviewCount;
	private EditText txtvOffice, txtvEmail, txtvPhone,editTextFirm,
			txtvWebsitee, txtvOccupation, txtvAmount, txtvUniversity, txtFProfessionalBio;
	private ImageButton backIcon;
	@SuppressWarnings("unused")
	private ImageView imageRating, imageOnline, imageViewPic, imageFav,
			imageMenu;
	private ImageButton btnuserMenuProfile;
	private Bitmap bitmap;
	private Context mContext;
	private static final int REVIEWUPDATE = 101;
	private HashMap<String, String> Infomap = null;
	LayoutInflater inflater;
	private View parentView;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_profile_user_new);

		requestFor = this.getIntent().getIntExtra("requestFor", 0);
		mContext = this;
		Infomap = new HashMap<String, String>();
		String Map = Utility.getUserPrefernce(mContext, "userprofileinfo");
		JSONObject json;
		try {
			json = new JSONObject(Map);
			Infomap = Utility.toMap(json);
			Log.e("Info Map data in profile view class", "" + Infomap);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("infoMapOnProfileView", Map);
		System.out.println("Hash map " + Infomap);
		if (Utility.getUserPrefernce(mContext, "usertype").equalsIgnoreCase("advisor")) {
			initAdvisor();

		} else {
			inituser();
		}

		
		String userType = Utility.getUserPrefernce(mContext, "usertype");
		if (userType.equalsIgnoreCase("user")) {
		} else {
		}

		// hide default image of navigation bar
		((ImageView)findViewById(R.id.ImageViewNavTitle)).setVisibility(View.INVISIBLE);
		
		backIcon = (ImageButton) findViewById(R.id.btn_BackHome);
		backIcon.setOnClickListener(onClickListener);

		btnuserMenuProfile = (ImageButton) findViewById(R.id.userMenuProfile);
		btnuserMenuProfile.setImageResource(R.drawable.btn_edit);
		btnuserMenuProfile.setOnClickListener(onClickListener);

	}

	private void initAdvisor()
	{
		inflater = LayoutInflater.from(this); 
		parentView = inflater.inflate(R.layout.activity_profile_advisor, null);
		
		((LinearLayout)findViewById(R.id.layoutProfile)).addView(parentView);
	
		txtvFname = (TextViewCustom) parentView.findViewById(R.id.txtNameAdvisor);
		txtFProfessionalBio = (EditText)parentView.findViewById(R.id.editTextAdvisorProffInfo);
		txtvUniversity = (EditText)parentView.findViewById(R.id.editTextAdvisorUniversity);
		txtvOccupation = (EditText)parentView.findViewById(R.id.editTextAdvisorDesignation);
		txtvLocation = (TextViewCustom) parentView.findViewById(R.id.txtLocationAdvisor);
		txtvAge = (TextViewCustom) parentView.findViewById(R.id.txtAgeAdvisor);
		txtReviewCount = (TextViewCustom) parentView.findViewById(R.id.txtReviewCount);
		txtvEmail = (EditText) parentView.findViewById(R.id.editTextAdvisorMail);
		txtvPhone = (EditText) parentView.findViewById(R.id.editTextAdvisorPhone);
		txtvWebsitee = (EditText) parentView.findViewById(R.id.editTextAdvisorWebsite);
		imageRating = (ImageView) parentView.findViewById(R.id.userMenuProfileRating);
		imageViewPic = (ImageView) parentView.findViewById(R.id.ImageHeaderPic);
		txtvOffice = (EditText)parentView.findViewById(R.id.editTextAdvisorOfficeAddress);
		editTextFirm = (EditText)parentView.findViewById(R.id.editTextAdvisorFIRM);
		setupAdvisor(Infomap);
	}
	
	private void setupAdvisor(HashMap<String, String> info) {
	
		txtFProfessionalBio.setText(info.get("professionalbio"));
		txtFProfessionalBio.setFocusable(false);
		txtFProfessionalBio.setClickable(false);
		
		txtvUniversity.setText(info.get("university"));
		txtvUniversity.setFocusable(false);
		txtvUniversity.setClickable(false);
		
		editTextFirm.setText((Infomap.get("firm").trim().length()==0)?"Not Available":Infomap.get("firm"));
		editTextFirm.setFocusable(false);
		editTextFirm.setClickable(false);
		
		txtvOccupation.setText(info.get("registeredtitle"));
		txtvOccupation.setFocusable(false);
		txtvOccupation.setClickable(false);
		
		if (Infomap != null && info.get("lname") != null) {

			txtvFname
					.setText(info.get("fname") + " " + info.get("lname"));

		} else {
			txtvFname.setText(info.get("fname"));
		}
		txtvLocation.setText(info.get("location"));
		txtvAge.setText(Infomap.get("age"));
		// Advisor Professional info
		if(info.get("reviewCount")!=null)
		txtReviewCount.setText("Reviews(" + info.get("reviewCount") + ")");

		// Advisor Contact info
		
		txtvEmail.setText(info.get("email"));
		txtvEmail.setFocusable(false);
		txtvEmail.setClickable(false);
		
		//txtvEmail.setOnClickListener(onClickListener);
		txtvPhone.setText((info.get("phone") != null) ? info.get("phone")
						: "");
		txtvPhone.setFocusable(false);
		txtvPhone.setClickable(false);

		txtvWebsitee.setText((info.get("website") != null ? info
						.get("website") : ""));
		txtvWebsitee.setFocusable(false);
		txtvWebsitee.setClickable(false);
		
		txtvOffice.setText(info.get("currentoffice"));
		txtvOffice.setFocusable(false);
		txtvOffice.setClickable(false);
		
		int rate = Math.round(Float.valueOf(info.get("overallrating")));;
		int resid = R.drawable.review_blank_fill;
		if (rate > 0){
			resid = getResources().getIdentifier("review_" + rate + "_fill",
					"drawable", getPackageName());
			imageRating.setImageResource(resid);
		}



		String url = null;
		if (info.get("photopath") != null)
			if (info.get("photopath").contains("http")
					|| info.get("photopath").contains("https")) {
				url = info.get("photopath");
			} else {
				url = Utility.IMAGEURL + info.get("photopath");
			}

		if (url != null) {
			ImageDownloader imageLoaaderr = new ImageDownloader();
			imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
			imageLoaaderr.download(url, imageViewPic, BitmapFactory
					.decodeResource(getResources(), R.drawable.dummy));

		}
	}

	private void inituser()
	{
		inflater = LayoutInflater.from(this);
		parentView = inflater.inflate(R.layout.activity_user_detail, null);
		((LinearLayout)findViewById(R.id.layoutProfile)).addView(parentView);
		txtvFname = (TextViewCustom) parentView.findViewById(R.id.txtNameUser);
		txtvLocation = (TextViewCustom) parentView.findViewById(R.id.txtLocationUser);
		txtvAge = (TextViewCustom) parentView.findViewById(R.id.txtAgeUser);
		txtvOccupation = (EditText) parentView.findViewById(R.id.editTextUserOccupation);
		txtvAmount = (EditText) parentView.findViewById(R.id.editTextUserAmount);
		txtvUniversity = (EditText) parentView.findViewById(R.id.editTextUserUniversity);
		txtvEmail = (EditText) parentView.findViewById(R.id.editTextUserMailuser);
		txtvPhone = (EditText) parentView.findViewById(R.id.editTextUserPhoneuser);
		imageViewPic = (ImageView) parentView.findViewById(R.id.ImageHeaderPicuser);
		
		setupUser(Infomap);
	}

	private void setupUser(HashMap<String, String> info) {
		
		if (info.get("lname") != null) {

			txtvFname
					.setText(info.get("fname") + " " + info.get("lname"));

		} else {
			txtvFname.setText(info.get("fname"));
		}

				
		if(info.get("location").trim().length()>0){
			txtvLocation.setText(info.get("location"));
		}else
		{
			txtvLocation.setVisibility(View.GONE);
		}
		
		txtvAge.setText(info.get("age"));
//		txtReviewCount = (TextView) parentView.findViewById(R.id.txtReviewCount);
//		txtReviewCount.setVisibility(View.GONE);
//		(imageRating = (ImageView) parentView.findViewById(R.id.userMenuProfileRating))
//				.setVisibility(View.GONE);

		txtvOccupation.setText((Infomap.get("occupation").trim().length()==0)?"Not Available":Infomap.get("occupation"));
		txtvOccupation.setFocusable(false);
		txtvOccupation.setClickable(false);
		txtvAmount.setText((Infomap.get("investableamout").trim().length()==0)?"Not Available":"$"+Infomap.get("investableamout"));
		txtvAmount.setFocusable(false);
		txtvAmount.setClickable(false);
		
		txtvUniversity.setText((Infomap.get("university").trim().length()==0)?"Not Available":Infomap.get("university"));
		txtvUniversity.setFocusable(false);
		txtvUniversity.setClickable(false);
		// User Contact info
		txtvEmail.setText(info.get("email"));

		txtvEmail.setFocusable(false);
		txtvEmail.setClickable(false);
		
		//txtvEmail.setOnClickListener(onClickListener);
		txtvPhone.setText((Infomap.get("phone").trim().length()==0)?"Not Available":Infomap.get("phone"));
		txtvPhone.setFocusable(false);
		txtvPhone.setClickable(false);

		String url = null;
		if (info.get("photopath") != null)
			if (info.get("photopath").contains("http")
					|| info.get("photopath").contains("https")) {
				url = info.get("photopath");
			} else {
				url = Utility.IMAGEURL + Infomap.get("photopath");
			}

	
		if (url != null) {
			ImageDownloader imageLoaaderr = new ImageDownloader();
			imageLoaaderr.setMode(Mode.NO_DOWNLOADED_DRAWABLE);
			imageLoaaderr.download(url, imageViewPic, BitmapFactory
					.decodeResource(getResources(), R.drawable.dummy));
		}

//		imageViewPic = (ImageView) parentView.findViewById(R.id.ImageHeaderPicuser);
//		if (bitmap != null) {
//			imageViewPic.setImageBitmap(bitmap);
//		} else {
//			new Thread(downloadpic).start();
//		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(Utility.getBooleanPrefernce(mContext, "profileupdate"))
		{
			Utility.setBooleanprefence(mContext, "profileupdate", false);
			String Map = Utility.getUserPrefernce(mContext, "userprofileinfo");
			JSONObject json;
			HashMap<String, String> info=null;
			try {
				json = new JSONObject(Map);
				info = Utility.toMap(json);
				Infomap.putAll(info);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("Hash map " + Infomap);
			if (Utility.getUserPrefernce(mContext, "usertype").equalsIgnoreCase("advisor")) {
				
				setupAdvisor(info);

			} else {
				setupUser(info);
			}
		}
	}

	// Click listener for favorite image
	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.btn_BackHome: {
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(
						new Intent("slide").putExtra("slide", "left"));
			}
				break;
			case R.id.userMenuProfile: {
				Intent intent = new Intent(mContext, ProfileActivityEdit.class);
				
				startActivityForResult(intent, 2);

			}
				break;
			}

		}
	};

	@Override
	public void onBackPressed() {
		super.onBackPressed();

	};

	// Download user image
	private Runnable downloadpic = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String url = null;

			if (Infomap.get("photopath") != null) {
				if (Infomap.get("photopath").contains("http")
						|| Infomap.get("photopath").contains("https")) {
					url = Infomap.get("photopath");
				} else {
					url = Utility.IMAGEURL + Infomap.get("photopath");
				}

				bitmap = Utility.getBitmap(url);
			}
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (bitmap != null)
						imageViewPic.setImageBitmap(bitmap);
					else
						imageViewPic.setImageResource(R.drawable.dummy);
					bitmap = null;
				}
			});
		}
	};

	// Mark as favorite

	Runnable updateProfile = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String serverUrl = Utility.SERVERURL + "GetAdvisorDetail";
			serverUrl = serverUrl + "&advisorid=" + Infomap.get("advisorid");
			final String response = Utility.getJSONFromUrl(serverUrl);
			if (response != null) {
				JSONObject json;
				try {
					json = new JSONObject(response);
					JSONArray jarray = json.getJSONArray("advisors");
					json = jarray.getJSONObject(0);
					json = json.getJSONObject("advisor");
					Infomap.put("overallrating",
							json.getString("overallrating"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (response != null) {
						int rate = Integer.parseInt(Infomap
								.get("overallrating"));

						int resid = R.drawable.review_blank_fill;
						if (rate > 0)
							resid = getResources().getIdentifier(
									"review_" + rate + "_fill", "drawable",
									getPackageName());
						imageRating.setImageResource(resid);

					}
				}
			});
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REVIEWUPDATE) {
			if (resultCode == Activity.RESULT_OK) {
				new Thread(updateProfile).start();
			}
		}
	}

}
