package com.Intlfaces.retire.ly;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFormatException;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Intlfaces.retire.ly.WebSocketClient.Listener;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.internal.fa;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.google.code.linkedinapi.schema.DateOfBirth;
import com.google.code.linkedinapi.schema.Person;
import com.linkedinn.Config;
import com.linkedinn.LinkedinDialog;
import com.linkedinn.LinkedinDialog.OnVerifyListener;
import com.linkites.retire.utility.FlipAnimation;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.RetirelyDbAdapter;

import facebook.BaseRequestListener;
import facebook.SessionEvents;
import facebook.SessionEvents.AuthListener;
import facebook.SessionEvents.LogoutListener;

@SuppressWarnings("deprecation")
public class SignInActivity extends Activity implements Listener{
	private Context mContext;
	private ProgressDialog mBProgressBar;
	private String SignIn = "SignIn";
	private Facebook facebook;
	private static String APP_ID = "415089575266346";// Replace it with your app id
	private AsyncFacebookRunner mAsyncRunner;
	private static final int FACEBOOK = 1;
	private static final int LINKEDIN = 2;
	private static final int LOGOUT = 3;
	private static final int SOCIALSIGNUP = 4;
	private static final int NORMALSIGNUP = 5;
	
	final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
            .getInstance().createLinkedInOAuthService(
                    Config.LINKEDIN_CONSUMER_KEY,Config.LINKEDIN_CONSUMER_SECRET);
	final LinkedInApiClientFactory factory = LinkedInApiClientFactory
			.newInstance(Config.LINKEDIN_CONSUMER_KEY,
					Config.LINKEDIN_CONSUMER_SECRET);
	private RetirelyDbAdapter dbAdapter;
	LinkedInRequestToken liToken;
	LinkedInApiClient client;
	LinkedInAccessToken accessToken = null;
	LinearLayout imageContainer;
	EditText editFemail;
	EditText editFpassword;
	FlipAnimation flipAnimation;
	Button btnFbLogin,btnLinkedin;
	ImageView imgAdvisor1,imgAdvisor2;
	public boolean running = true;
	ImageView img;
	int currentImage1;
	int currentImage2;
	private WebSocketClient socketclient;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext = this;
		Intent i = new Intent(mContext,LocationService.class);
		startService(i);
		editFemail = (EditText) findViewById(R.id.txtEmail);
		editFpassword = (EditText) findViewById(R.id.txtPass);
		this.getIntent().getIntExtra("requestFor", Constants.USER);
		dbAdapter = new RetirelyDbAdapter(this);
		imgAdvisor1=(ImageView)findViewById(R.id.imgAdvisor1);
		imgAdvisor2=(ImageView)findViewById(R.id.imgAdvisor2);
		
		 if( Build.VERSION.SDK_INT >= 9){
	            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	            StrictMode.setThreadPolicy(policy); 
	     }
		 
		 if (!isEmulator()) {

				checkNotNull(Utility.SENDER_ID, "SENDER_ID");
				// Make sure the device has the proper dependencies.
				GCMRegistrar.checkDevice(this);
				// Make sure the manifest was properly set - comment out this line
				// while developing the app, then uncomment it when it's ready.
				GCMRegistrar.checkManifest(this);

				Utility.mContext = getApplicationContext();

				final String regId = GCMRegistrar.getRegistrationId(this);
				if (regId.equals("")) {
					// Automatically registers application on startup.
					GCMRegistrar.register(getApplicationContext(),
							Utility.SENDER_ID);
					Log.e("Registeration Id", "RegisterIng Divice");
				} else {
					Log.e("Registeration Id", regId);
				}
			}
		 
		// Threads for animation Image flip.
//		new Thread(){
//			public void run() {
//				try {
//					while(running){
//						Thread.sleep(2000);
//						runOnUiThread(animate);
//					}
//					System.out.println("Exiting Bye!!!");
//				} catch (InterruptedException e) {
//				}				
//			}
//		}.start();		
//			
//		new Thread(){
//			public void run() {
//				try {
//					while(running){
//						Thread.sleep(2000);
//						runOnUiThread(animate2);
//					}
//					System.out.println("Exiting Bye!!!");
//				} catch (InterruptedException e) {
//				}				
//			}
//		}.start();			
			 
		
		
		facebook = new Facebook(APP_ID);
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		SessionEvents.addAuthListener(new SampleAuthListener());		
		SessionEvents.addLogoutListener(new SampleLogoutListener());
		

		btnFbLogin = (Button) findViewById(R.id.btn_Fb);

		btnFbLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				loginToFacebook();
			}
		});	
		btnLinkedin = (Button)findViewById(R.id.btn_Link);
		btnLinkedin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				btnLinkedin.setEnabled(false);
				linkedInLogin();
			}
		});
		((Button) findViewById(R.id.btn_Login)).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								mContext);
						builder.setCancelable(false);
						builder.setTitle("Sign In");
						builder.setMessage("Please Select account type");
						// Set behavior of negative button
						builder.setNegativeButton("cancel",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.cancel();

									}
								});
						builder.setPositiveButton("User",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,int arg1) {
										new SignInUser().execute("user");

									}
								});
						builder.setNeutralButton("Advisor",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										new SignInUser().execute("advisor");
									}
								});
						AlertDialog alert = builder.create();
						try {
							alert.show();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
		((Button) findViewById(R.id.btn_signup)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						
										Intent intent = new Intent(
												SignInActivity.this,
												SignUpActivity.class);
										intent.putExtra("requestFor",
												Constants.USER);
										startActivityForResult(intent,NORMALSIGNUP);
						
					}
				});
		((TextView)findViewById(R.id.txtVPasswordReset)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SignInActivity.this, ResetPassword.class);
				startActivity(intent);
			}
		});
		List<BasicNameValuePair> extraHeaders = Arrays.asList(
			    new BasicNameValuePair("Cookie", "session=abcd")
			);
		socketclient = new WebSocketClient(URI.create("ws://162.222.227.147:10000"), this, extraHeaders);	//162.222.227.147:3001
		
		LocalBroadcastManager.getInstance(this).registerReceiver(sendMsgReceiver, new IntentFilter("sendmsg"));
		
		LocalBroadcastManager.getInstance(this).registerReceiver(
				logoutReceiver, new IntentFilter("logout"));
		if(Utility.getBooleanPrefernce(mContext, "login"))
		
		{
			registeredsocet();
			
			Intent intent = new Intent(mContext,
					SlideMenuActivityGroup.class);
			startActivityForResult(intent, LOGOUT);
		}
	}

	private void registeredsocet()
	{
		socketclient.connect();
		socketclient.send(""+Utility.getUserPrefernce(mContext, "id")+"ΔtestΔ0ΔhelloΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δadvisor");

	}
	
private BroadcastReceiver sendMsgReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			final String msg = intent.getStringExtra("msg");
			Log.d("Socket msg", ""+msg);
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					socketclient.send(msg);
				}
			});
		}
	};
	
	
	// 
	private BroadcastReceiver logoutReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Utility.setBooleanPreferences(mContext, "login", false);
			Utility.setUserPrefernce(mContext, "userprofileinfo", null);
			removeProfileInfo(null);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("applogout"));
			//finish();

		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	// Facebook Aut Listener 
	private class SampleAuthListener implements AuthListener {
		public void onAuthSucceed() {
			
			requestUserData();
		}

		public void onAuthFail(String error) {
			Toast.makeText(mContext, "Login Failed: " + error,
					Toast.LENGTH_LONG).show();

		}

	}

	public class SampleLogoutListener implements LogoutListener {
		public void onLogoutBegin() {

		}

		public void onLogoutFinish() {

		}
	}

	
	private void linkedInLogin() {
		ProgressDialog progressDialog = new ProgressDialog(
				SignInActivity.this);

		LinkedinDialog d = new LinkedinDialog(SignInActivity.this,
				progressDialog);
		d.show();
		
		
		// set call back listener to get oauth_verifier value
		d.setVerifierListener(new OnVerifyListener() {
			@Override
			public void onVerify(String verifier) {
				try {
					Log.i("LinkedinSample", "verifier: " + verifier);

					accessToken = LinkedinDialog.oAuthService
							.getOAuthAccessToken(LinkedinDialog.liToken,
									verifier);
					LinkedinDialog.factory.createLinkedInApiClient(accessToken);
					client = factory.createLinkedInApiClient(accessToken);
					// client.postNetworkUpdate("Testing by Mukesh!!! LinkedIn wall post from Android app");
					Log.i("LinkedinSample",
							"ln_access_token: " + accessToken.getToken());
					Log.i("LinkedinSample",
							"ln_access_token: " + accessToken.getTokenSecret());
				
					//Person p = client.getProfileForCurrentUser();
					Person p = client.getProfileForCurrentUser(EnumSet.of(
			                ProfileField.ID, ProfileField.FIRST_NAME,
			                ProfileField.LAST_NAME, ProfileField.HEADLINE,
			                ProfileField.INDUSTRY, ProfileField.PICTURE_URL,
			                ProfileField.DATE_OF_BIRTH, ProfileField.LOCATION_NAME,
			                ProfileField.MAIN_ADDRESS, ProfileField.LOCATION_COUNTRY,ProfileField.EMAIL_ADDRESS));
					
					
					DateOfBirth dob = p.getDateOfBirth();
					int age = 18;
					if(dob!=null)
					 age = Utility.getAge(dob.getYear().intValue(), dob.getMonth().intValue(), dob.getDay().intValue());
					// Store image path 
					String picurl = p.getPictureUrl();//"http://graph.facebook.com/"+p.getId()+"/picture?width=250&height=250";
					if(picurl!=null)
					Log.e("PICTURE URL", picurl);
					Utility.setUserPrefernce(mContext, "linkedinnid", p.getId());
					Utility.setUserPrefernce(mContext, "imagepath", picurl);
					
					Utility.setUserPrefernce(mContext, "age", ""+age);
					// Store first and last name of user
					Utility.setUserPrefernce(mContext, "fname", p.getFirstName());
					Utility.setUserPrefernce(mContext, "lname", p.getLastName());
			
					// store id as password
					final String socialPassword = p.getId();
					Utility.setUserPrefernce(mContext, "pwd", socialPassword);
					//Utility.setUserPrefernce(mContext, "gender", p.get);
					// store email id
					final String socialemail = p.getEmailAddress();
					Utility.setUserPrefernce(mContext, "email", socialemail);
					Log.d("", "socialemail "+socialemail);
					
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							showAlert(LINKEDIN);
						}
					});
			
				} catch (Exception e) {
					Log.i("LinkedinSample", "error to get verifier");
					e.printStackTrace();
				}
			}
		});

		// set progress dialog
				progressDialog.setMessage("Loading...");
				progressDialog.setCancelable(true);
			
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.show();
				btnLinkedin.setEnabled(true);
	}
	
	// Social Sign In
	public class SocialSignIn extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				mBProgressBar = ProgressDialog.show(SignInActivity.this,
						"Login in", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								SocialSignIn.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			
			listParams.add(new BasicNameValuePair("fname", Utility
					.getUserPrefernce(mContext, "fname")));
			listParams.add(new BasicNameValuePair("lname", Utility
					.getUserPrefernce(mContext, "lname")));
			
			listParams.add(new BasicNameValuePair("email", Utility
					.getUserPrefernce(mContext, "email")));
			
			listParams.add(new BasicNameValuePair("pwd", Utility
					.getUserPrefernce(mContext, "pwd")));
			if(Utility
					.getUserPrefernce(mContext, "gender")!=null)
			listParams.add(new BasicNameValuePair("gender", Utility
					.getUserPrefernce(mContext, "gender")));
			
			listParams.add(new BasicNameValuePair("age", Utility
					.getUserPrefernce(mContext, "age")));
			
			listParams.add(new BasicNameValuePair(params[1], Utility
					.getUserPrefernce(mContext, "pwd")));
			
			
			listParams.add(new BasicNameValuePair("imagepath", Utility
					.getUserPrefernce(mContext, "imagepath")));
			listParams.add(new BasicNameValuePair("usertype", params[0]));
			
			listParams.add(new BasicNameValuePair("devicetype","android"));

			if (Utility.getUserPrefernce(mContext, "token") != null)
				listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));

			String serverUrl = Utility.SERVERURL + "SocialSignIn";
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("{") && result.length() > 5) {
					try {
						HashMap<String, String> map = Utility
								.toMap(new JSONObject(result));
						map.put("gender", Utility.getUserPrefernce(mContext, "gender"));
					
						result = new JSONObject(map).toString();
						
										
						socketclient.connect();
						

						
						if(map.get("usertype").contains("user")&&map.get("phone").trim().length()==0){
						Intent intent = new Intent(mContext,
								SocialSignUpActivity.class);
						
						intent.putExtra("map", map);
						startActivity(intent);
						}else if(map.get("usertype").contains("advisor")&&map.get("phone").trim().length()==0){
							Intent intent = new Intent(mContext,
									SocialSignUpActivity.class);
							intent.putExtra("map", result);
							startActivityForResult(intent,SOCIALSIGNUP);
						}else
						{
							Utility.setUserPrefernce(mContext, "usertype",map.get("usertype")
									);
							if(map.get("usertype").equalsIgnoreCase("user")){
								map.put("userid", map.get("id"));
							Utility.setUserPrefernce(mContext, "id",map.get("id")
									);
							}
							else{
								map.put("advisorid", map.get("id"));
								
								Utility.setUserPrefernce(mContext, "id",map.get("id")
										);
								
								map.remove("id");
							}
					
							Utility.setUserPrefernce(mContext, "userprofileinfo", new JSONObject(map).toString());
							Utility.setBooleanPreferences(mContext, "login", true);
							Intent intent = new Intent(mContext,
									SlideMenuActivityGroup.class);
							startActivityForResult(intent,SOCIALSIGNUP);
						}
						//finish();

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

			} else {
				Utility.showAlert(mContext, "Alert",
						getResources().getString(R.string.nonetwork));
			}
		}

	}

	// Sign In
	public class SignInUser extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				mBProgressBar = ProgressDialog.show(SignInActivity.this,
						"Login in", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								SignInUser.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();

			listParams.add(new BasicNameValuePair("email", editFemail.getText()
					.toString()));

			listParams.add(new BasicNameValuePair("pwd", editFpassword
					.getText().toString()));
			listParams.add(new BasicNameValuePair("usertype", params[0]));

			if (Utility.getUserPrefernce(mContext, "token") != null)
			listParams.add(new BasicNameValuePair("devicetoken", Utility
						.getUserPrefernce(mContext, "token")));
			
			listParams.add(new BasicNameValuePair("devicetype", "android"));

			String serverUrl = Utility.SERVERURL + SignIn;
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("" + result);
			if (mBProgressBar != null)
				mBProgressBar.cancel();

			if (result != null) {
				if (result.contains("{") && result.length() > 5) {
					try {
						HashMap<String, String> map = Utility
								.toMap(new JSONObject(result));
						Utility.setBooleanPreferences(mContext, "login", true);
						Utility.setUserPrefernce(mContext, "userprofileinfo", result);
						Utility.setUserPrefernce(mContext, "usertype",map.get("usertype")
								);
						saveProfileInfo(map);
						socketclient.connect();
						socketclient.send(""+Utility.getUserPrefernce(mContext, "id")+"ΔtestΔ0ΔhelloΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δadvisor");

						Intent intent = new Intent(mContext,
								SlideMenuActivityGroup.class);
						startActivityForResult(intent, LOGOUT);
						//finish();

					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else if (result.contains("0")) {
					Utility.showAlert(mContext, "Alert",
							"User not registered");
				}

			} else {
				Utility.showAlert(mContext, "Alert",
						getResources().getString(R.string.nonetwork));
			}
		}

	}

	private void saveProfileInfo(HashMap<String, String> map) {
		Set<?> setkey = map.keySet();
		Iterator<?> keys = setkey.iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (key.equalsIgnoreCase("advisorid")) {
				Utility.setUserPrefernce(mContext, "id", map.get("advisorid"));
			} else if (key.equalsIgnoreCase("userid")) {
				Utility.setUserPrefernce(mContext, "id", map.get("userid"));
			}
			Utility.setUserPrefernce(mContext, key, map.get(key));
		}

	}
	
	private void removeProfileInfo(HashMap<String, String> map) {
	
		if(facebook!=null)
		{
			if(facebook.isSessionValid())
			{
				try {
					facebook.logout(this);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}


	public void loginToFacebook() {

		if (!facebook.isSessionValid()) {
			facebook.authorize(SignInActivity.this, new String[] {"email", "publish_stream","user_birthday","user_education_history", "user_work_history","user_location", "user_friends", "manage_friendlists"}, new DialogListener() {

				@Override
				public void onCancel() {
					// Function to handle cancel event

				}

				@Override
				public void onComplete(Bundle values) {
					// Function to handle complete event
					// Edit Preferences and update facebook acess_token

					SessionEvents.onLoginSuccess();
				}

				@Override
				public void onError(DialogError error) {
					// Function to handle error
					Toast.makeText(SignInActivity.this,
							"Internet Connection Error", Toast.LENGTH_LONG)
							.show();

				}

				@Override
				public void onFacebookError(FacebookError fberror) {
					// Function to handle Facebook errors
					Toast.makeText(SignInActivity.this,
							"Fb Error :" + fberror.toString(),
							Toast.LENGTH_LONG).show();
				}

			});
		}

	}

	public void requestUserData() {
		
		Bundle params = new Bundle();
		//params.putString("fields", "name, picture, sex,email,first_name,last_name,birthday,work,education");
		//mAsyncRunner.request("me", params, new UserRequestListener());
		String fqlQuery = "SELECT uid,email,current_location,first_name,last_name,sex,birthday,work FROM  user WHERE uid=me()";
		params.putString("q", fqlQuery);
		mAsyncRunner.request("me", params, new UserRequestListener());
	}

	private class UserRequestListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(response);
				Log.i("Facebook test ", response);
//				JSONObject data = new JSONObject(
//						jsonObject.getString("picture"));
//				data = data.getJSONObject("data");
// {"id":"100001584344083","name":"Amit Mohite","first_name":"Amit","last_name":"Mohite","link":"https:\/\/www.facebook.com\/ajmohite1","username":"ajmohite1","birthday":"04\/02\/1984",
				//"hometown":{"id":"112027728823762","name":"Indore, India"},
				//"location":{"id":"112027728823762","name":"Indore, India"},
				//"work":[{"employer":{"id":"302124889826138","name":"LinkITes"}}],
				//"favorite_teams":[{"id":"283267038392614","name":"Bhagat Singh : A Timeless Revolution"}],
				//"education":[{"school":{"id":"109579315764443","name":"Garima Vidhya Vihar H.S. School"},"type":"High School"},
				//{"school":{"id":"112069525485912","name":"IGNOU"},"type":"College"}],
				//"gender":"male","email":"ajmohite1\u0040gmail.com","timezone":5.5,"locale":"en_US","verified":true,"updated_time":"2013-10-14T14:12:25+0000"}

//		
				String dob = jsonObject.getString("birthday");
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				 Date convertedDate = new Date();
				    try {
				        convertedDate = dateFormat.parse(dob);
				    } catch (ParcelFormatException e) {
				        e.printStackTrace();
				    } catch (ParseException e) {
						e.printStackTrace();
					}
				    Calendar thatDay = Calendar.getInstance();
				    thatDay.setTime(convertedDate);
				    int year = thatDay.get(Calendar.YEAR);
				    int month = thatDay.get(Calendar.MONTH)+1;
				    int day = thatDay.get(Calendar.DAY_OF_MONTH);
				 int age = Utility.getAge(year, month, day);
				// Store image path 
				String picurl = "http://graph.facebook.com/"+jsonObject.getString("id")+"/picture?width=250&height=250";
				Utility.setUserPrefernce(mContext, "imagepath", picurl);
				Utility.setUserPrefernce(mContext, "age", ""+age);
				// Store first and last name of user
				Utility.setUserPrefernce(mContext, "fname", jsonObject.getString("first_name"));
				Utility.setUserPrefernce(mContext, "lname", jsonObject.getString("last_name"));
		
				// store id as password
				final String socialPassword = jsonObject.getString("id");
				Utility.setUserPrefernce(mContext, "pwd", socialPassword);
				Utility.setUserPrefernce(mContext, "gender", jsonObject.getString("gender"));
				// store email id
				final String socialemail = jsonObject.getString("email");
				Utility.setUserPrefernce(mContext, "email", socialemail);
				
				
				
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						showAlert(FACEBOOK);
					}
				});

			} catch (JSONException e) {
				if (mBProgressBar != null)
					mBProgressBar.cancel();
				e.printStackTrace();
			}
		}

	}
	
	
	private void showAlert(final int request)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setCancelable(false);
		builder.setTitle("Socail Sign In");
		builder.setMessage("Select account type");
		// Set behavior of negative button
		builder.setNegativeButton("cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		builder.setPositiveButton("User",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						if(request==FACEBOOK)
						new SocialSignIn().execute("user","fbid");
						else
							new SocialSignIn().execute("user","linkedinid");

					}
				});
		builder.setNeutralButton("Advisor",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(request==FACEBOOK)
							new SocialSignIn().execute("advisor","fbid");
							else
								new SocialSignIn().execute("advisor","linkedinid");

					}
				});
		AlertDialog alert = builder.create();
		try {
			alert.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Runnable animate = new Runnable() {		
		public void run() {
			
			imageContainer = (LinearLayout)findViewById(R.id.flip_layout1);
			imgAdvisor1 = (ImageView)findViewById(R.id.imgAdvisor1);
			imgAdvisor2 = (ImageView)findViewById(R.id.imgAdvisor2);		
			
			flipAnimation = new FlipAnimation(imgAdvisor1, imgAdvisor2);
			if (imgAdvisor1.getVisibility()==View.GONE)
		    {
		        flipAnimation.reverse();
		    }
		    imageContainer.startAnimation(flipAnimation);
		    
		    runOnUiThread(setImages);
		}
	};
	
	public Runnable animate2 = new Runnable() {		
		public void run() {
			
			imageContainer = (LinearLayout)findViewById(R.id.flip_layout2);
			imgAdvisor1 = (ImageView)findViewById(R.id.imgAdvisor3);
			imgAdvisor2 = (ImageView)findViewById(R.id.imgAdvisor4);		
			
			flipAnimation = new FlipAnimation(imgAdvisor1, imgAdvisor2);
			if (imgAdvisor1.getVisibility()==View.GONE)
		    {
		        flipAnimation.antiReverse();
		    }
		    imageContainer.startAnimation(flipAnimation);
		    
		    runOnUiThread(setImages2);
		}
	};
	
	public Runnable setImages = new Runnable() {
		public void run() {
			Random r = new Random();
			int imgResource;
			int i;
			do{
			i= r.nextInt(4);
			if(i==0)
				imgResource = R.drawable.advisor1;
			else if(i==1)
				imgResource = R.drawable.advisor2;
			else if(i==2)
				imgResource = R.drawable.advisor3;
			else 
				imgResource = R.drawable.advisor4;
			
			
			}
			while(currentImage1 == imgResource || currentImage2 == imgResource);
			if(imgAdvisor1.getVisibility()==View.VISIBLE){
				imgAdvisor2.setImageResource(imgResource);					
				currentImage2 = imgResource;					
			}
			else{
				imgAdvisor1.setImageResource(imgResource);
				currentImage1 = imgResource;
			}
			
		}
	};
	
	public Runnable setImages2 = new Runnable(){
		public void run (){
			Random r = new Random();
			int imgResource;
			int i;
			do{
				i=r.nextInt(4);
				if(i==0)
					imgResource = R.drawable.advisor1;
				else if(i==1)
					imgResource = R.drawable.advisor2;
				else if(i==2)
					imgResource = R.drawable.advisor3;
				else 
					imgResource = R.drawable.advisor4;
				
			}
			while(currentImage1 == imgResource || currentImage2 == imgResource);
			if(imgAdvisor2.getVisibility()==View.VISIBLE){
				imgAdvisor1.setImageResource(imgResource);					
				currentImage1 = imgResource;					
			}
			else{
				imgAdvisor2.setImageResource(imgResource);
				currentImage2 = imgResource;
			}
		}
	};
	
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode==LOGOUT)
		{
			if(resultCode==Activity.RESULT_CANCELED)
			{
				finish();
				
			}
		}
		if (requestCode==SOCIALSIGNUP) {
			
			if (resultCode==Activity.RESULT_OK) {
				
				registeredsocet();
				
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivityForResult(intent, LOGOUT);
			}
		}
		if (requestCode==NORMALSIGNUP) {
			
			if (resultCode==Activity.RESULT_OK) {
				
				registeredsocet();
				
				Intent intent = new Intent(mContext,
						SlideMenuActivityGroup.class);
				startActivityForResult(intent, LOGOUT);
			}
		}
	};
	
	protected void onDestroy() {
		running=false;
		super.onDestroy();
		socketclient.disconnect();
	}

	@Override
	public void onConnect() {
		// TODO Auto-generated method stub
		Log.d("Socket Connected", "Connected...");
		socketclient.send(""+Utility.getUserPrefernce(mContext, "id")+"ΔtestΔ0ΔhelloΔ04:34:49 +PMΔ2013-12-02 00:00:00 +0000Δadvisor");
	}

	@Override
	public void onMessage(String message) {
		// TODO Auto-generated method stub
		Log.d("Socket msg", ""+message);
		if(message.contains("said")){
			String []strMsg = message.split("said");
			String [] strsubmg = strMsg[1].split("Δ");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("userid", strsubmg[0].replace("\"", "").trim());
			map.put("message", strMsg[1]);
			dbAdapter.insertChatRecord(map);
	
			if(!Utility.getBooleanPrefernce(mContext, "chatwindow_"+strsubmg[0].replace("\"", "").trim()))
			{
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("updateFav"));
			}
			LocalBroadcastManager.getInstance(mContext).sendBroadcastSync(new Intent("receivemsg").putExtra("msg", message));
		}
	}

	@Override
	public void onMessage(byte[] data) {
		// TODO Auto-generated method stub
		Log.d("Socket data msg", ""+data);
	}

	
	@Override
	public void onDisconnect(int code, String reason) {
		// TODO Auto-generated method stub
		Log.d("Socket Disconnected", ""+reason);
	}

	@Override
	public void onError(Exception error) {
		// TODO Auto-generated method stub
		Log.d("Socket error", ""+error);
	};
	
	public boolean isEmulator() {
		return Build.MANUFACTURER.equals("unknown");
	}
	
	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("SenderId is Null");
		}
	}
	
}
