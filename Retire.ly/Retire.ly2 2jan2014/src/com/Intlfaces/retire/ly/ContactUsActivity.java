package com.Intlfaces.retire.ly;


import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.Intlfaces.retire.ly.SignInActivity.SignInUser;
import com.linkites.retire.utility.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class ContactUsActivity extends Activity {

	private EditText txtEmail, txtSubject, txtMessage;
    private Button btnSend;
    private ImageButton btnBack;
	private ProgressDialog mBProgressBar;
    Context mContext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);
        mContext= this;
		((ImageView) findViewById(R.id.ImageViewNavTitle))
				.setImageResource(R.drawable.title_contact_us);

		txtEmail=(EditText)findViewById(R.id.txtSEmail);
		txtSubject=(EditText)findViewById(R.id.txtSSubject);
		txtMessage=(EditText)findViewById(R.id.txtSMessage);
		
		btnSend=(Button)findViewById(R.id.btn_SendUs);
		btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (!Utility.isValidEmail(txtEmail.getText()
						.toString())
						|| txtEmail.getText().toString().length() == 0) {
					Utility.ShowAlertWithMessage(mContext, "Invalid Email",
							"Please check the email address");
					return;
				}
				if (txtSubject.getText().toString().length() == 0) {
					Utility.ShowAlertWithMessage(mContext,
							"Empty Subject",
							"Please check the Subject field");
					return;
				}
				if (txtMessage.getText().toString().length() == 0) {
					Utility.ShowAlertWithMessage(mContext,
							"Empty Message",
							"Please check the Message field");
					return;
				}			
				
				btnSend.setEnabled(false);
				ContactUs contactus = new ContactUs();
				contactus.execute();
			}
		});
		
		btnBack = (ImageButton) findViewById(R.id.btn_BackHome);
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocalBroadcastManager.getInstance(ContactUsActivity.this)
				.sendBroadcast(new Intent("slide").putExtra("slide","left"));
			}
		});
		
	}
	
	private class ContactUs extends AsyncTask<String, Void, String>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			try {
				mBProgressBar = ProgressDialog.show(ContactUsActivity.this,
						"Sending Message", "Please wait", true, true,
						new DialogInterface.OnCancelListener() {

							@Override
							public void onCancel(DialogInterface dialog) {
								ContactUs.this.cancel(true);
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}	
		
		
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			
			listParams.add(new BasicNameValuePair("email", txtEmail.getText().toString()));
			listParams.add(new BasicNameValuePair("usertype", "user"));
			listParams.add(new BasicNameValuePair("message", txtMessage.getText().toString()));
			listParams.add(new BasicNameValuePair("subject", txtSubject.getText().toString()));
			
			
			String serverUrl = Utility.SERVERURL+"ContactUs";
			String response = Utility.postParamsAndfindJSON(serverUrl,
					listParams);
			System.out.println("this is response:"+response);
			return response;
		}
		
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			mBProgressBar.cancel();
			btnSend.setEnabled(true);
			
			if(result!=null){
				System.out.println("This is result: "+result);
				Toast.makeText(mContext, "Message Sent", Toast.LENGTH_SHORT).show();
			}
			if(result==null){
				Utility.showAlert(mContext, "No Network", "Please check your internet connection");
			}
		}
		
	}
	
}
