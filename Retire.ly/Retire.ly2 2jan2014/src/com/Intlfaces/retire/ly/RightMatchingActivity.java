package com.Intlfaces.retire.ly;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.fortysevendeg.swipelistview.SwipeListView;
import com.fortysevendeg.swipelistview.SwipeListViewListener;
import com.google.android.gms.internal.al;
import com.linkites.retire.utility.SettingsManager;
import com.linkites.retire.utility.Utility;
import com.retirely.adapters.ApplicationAdapter;
import com.retirely.adapters.RetirelyDbAdapter;

public class RightMatchingActivity extends Activity implements SwipeListViewListener{

private String url = "http://retire.ly/mobile/api.php?method=SearchUsers&lat=34.197311&lng=-118.643982&male=male&female=female&miles=10&minage=18&maxage=25&keywords=&advisorid=4";
public ArrayList<HashMap<String, String>> listMatchesRecord, listFilterRecord;
private SwipeListView listViewRecord;
private ApplicationAdapter adapter;
private Context mContext;
private HashMap<String, Bitmap> mapBitmap;
private ProgressDialog mProgressBar;
public boolean isLoaded, isAdvisor, isServiceRuning;
private EditText editSearchBox;
private int selectedIndex;


private RetirelyDbAdapter dbadapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_right_layout);
		mContext = this;
		dbadapter = new RetirelyDbAdapter(this);
		// Check login user type
		String userType = Utility.getUserPrefernce(mContext, "usertype");
		if(userType!=null)
		if(userType.equalsIgnoreCase("user"))
			isAdvisor = false;
		else
			isAdvisor = true;
		String searchhint = "";
		if(isAdvisor){
			searchhint = "Search in 0 Leads";
		((ImageView)findViewById(R.id.ImageViewNavTitle)).setImageResource(R.drawable.title_my_leads);
		}
		else{
			searchhint = "Search in 0 Advisors";
		((ImageView)findViewById(R.id.ImageViewNavTitle)).setImageResource(R.drawable.title_my_advisors);
		}
			
		// list for store all record
		listMatchesRecord = new  ArrayList<HashMap<String,String>>();
		listFilterRecord = new ArrayList<HashMap<String,String>>();
		// list view for show all record 
		listViewRecord = (SwipeListView)findViewById(R.id.listViewMatch);
		listViewRecord.setOffsetLeft(convertDpToPixel(160));
		listViewRecord.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
		listViewRecord.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
		listViewRecord.setAnimationTime(0);
		listViewRecord.setSwipeListViewListener(this);
		// map for store images
		mapBitmap = new HashMap<String, Bitmap>();
		
		editSearchBox = (EditText)findViewById(R.id.editTextSearch);
		
		editSearchBox.addTextChangedListener(editTextWather);
		editSearchBox.setOnEditorActionListener(editTextActionListenr);
		editSearchBox.setHint(searchhint);
		
		// adapter to show record on cell
		adapter = new ApplicationAdapter(mContext, 1, R.layout.cell_match_user, listFilterRecord, mapBitmap);
	
		listViewRecord.setAdapter(adapter);
		editSearchBox.setEnabled(false);
		LocalBroadcastManager.getInstance(mContext).registerReceiver(updateFavorite, new IntentFilter("updateFav"));
		LocalBroadcastManager.getInstance(mContext).registerReceiver(receiverBlock, new IntentFilter("block"));
		LocalBroadcastManager.getInstance(mContext).registerReceiver(receiverLike, new IntentFilter("likefavorite"));
		
		if(!isLoaded)
		{
			isLoaded = true;
			if(!isServiceRuning)
			new GetMyMatches().execute("");
			// Start to get current location
		}
	}
	
	
	
	
	
	private BroadcastReceiver receiverBlock = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			@SuppressWarnings("unchecked")
			final HashMap<String, String> map = (HashMap<String, String>)intent.getSerializableExtra("detail");
			final String userid = (isAdvisor)?map.get("userid"):map.get("advisorid");
			boolean isBlock = intent.getBooleanExtra("isblock", false);
			selectedIndex = Integer.parseInt(map.get("index"));
			runOnUiThread(new Runnable() {
				public void run() {
					 AlertDialog.Builder alert = new Builder(mContext);
					 alert.setMessage("Do you want to Block this user?");
					 alert.setPositiveButton("Yes", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							new MarkAndUnmarkAsFavorite().execute(userid);
						}
					});
					 alert.setNegativeButton("No", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
						}
					});
					 try{
						 alert.show();
					 }catch(Exception e)
					 {
						 e.printStackTrace();
					 }
					 
					Toast.makeText(mContext, map.get("fname"), Toast.LENGTH_LONG).show();
				}
			});
		}
	};
	
	   public int convertDpToPixel(float dp) {
	        DisplayMetrics metrics = getResources().getDisplayMetrics();
	        float px = dp * (metrics.densityDpi / 160f);
	        return (int) px;
	    }
	private BroadcastReceiver receiverLike = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			@SuppressWarnings("unchecked")
			final HashMap<String, String> map = (HashMap<String, String>)intent.getSerializableExtra("detail");
			final String favoriteid = map.get("favoriteid");
			selectedIndex = Integer.parseInt(map.get("index"));
			runOnUiThread(new Runnable() {
				public void run() {
					 new LikeUser().execute(favoriteid);
					Toast.makeText(mContext, map.get("fname")+ " index "+map.get("index"), Toast.LENGTH_LONG).show();
				}
			});
		}
	};
	
	private class LikeUser extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mProgressBar = ProgressDialog.show(mContext, "", "Please wait...", true, true, new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					LikeUser.this.cancel(true);
				}
			});
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String favoriteid = params[0];
			String response = null;
			String url = Utility.SERVERURL + "ApproveFavoriteRequest";
			ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
			listParams.add(new BasicNameValuePair("favoriteid", favoriteid));
			response = Utility
					.postParamsAndfindJSON(url, listParams);;
		
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (mProgressBar!=null) {
				mProgressBar.cancel();
			}
			if(result!=null)
			{
				if (result.contains("0")) {
				}else if(result.contains("1"))
				{
					HashMap<String, String> map = listMatchesRecord.get(selectedIndex);
					map.put("favoriteid", "10");
					listMatchesRecord.set(selectedIndex, map);
					adapter.notifyDataSetChanged();
					selectedIndex = -1;
				}
			}
			
			
		}
		
	}
	
	// MarkAndUnmark as favorite
		private class MarkAndUnmarkAsFavorite extends AsyncTask<String, Void, String>
		{

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				mProgressBar = ProgressDialog.show(mContext, "", "Please wait...", true, true, new DialogInterface.OnCancelListener() {
					
					@Override
					public void onCancel(DialogInterface dialog) {
						// TODO Auto-generated method stub
						MarkAndUnmarkAsFavorite.this.cancel(true);
					}
				});
			}
			
			@Override
			protected String doInBackground(String... userid) {
				// TODO Auto-generated method stub
				String response = null;
				{
					String url = Utility.SERVERURL + "MarkAsBlocked";
					//url = url + "&senderid="
						//	+ Utility.getUserPrefernce(mContext, "id");
					
					ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
					listParams.add(new BasicNameValuePair("senderid", Utility.getUserPrefernce(mContext, "id")));
					if (Utility.getUserPrefernce(mContext, "usertype")
							.equalsIgnoreCase("advisor")) {
						listParams.add(new BasicNameValuePair("senderusertype", "advisor"));
						listParams.add(new BasicNameValuePair("blockeduserid", userid[0]));
					} else {
						
						//url = url + "&senderusertype=user&blockeduserid="
							//	+ hashUser.get("advisorid");
						
						listParams.add(new BasicNameValuePair("senderusertype", "user"));
						listParams.add(new BasicNameValuePair("blockeduserid",userid[0]));
					}
					response = Utility
							.postParamsAndfindJSON(url, listParams);;
				}
				return response;//http://projects.linkites.com/retirely/api/v1/api.php?method=MarkAsBlocked&senderid=124&senderusertype=user&blockeduserid=76
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				if (mProgressBar!=null) {
					mProgressBar.cancel();
				}
				
				if (result!=null) {
					Toast.makeText(mContext, ""+result, Toast.LENGTH_LONG).show();
					if(result.contains("0"))
					{
						Utility.showAlert(mContext, "Alert", "Unable to process, please try again.");
					}else
					{
						listMatchesRecord.remove(selectedIndex);
						adapter.notifyDataSetChanged();
						selectedIndex = -1;
						//LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("updateFav"));
					}
				}else
				{
					Utility.showAlert(mContext, "Alert", "Please check your internet connection.");
				}
			}
		}

	
	
	private OnEditorActionListener editTextActionListenr = new OnEditorActionListener() {
		
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if(actionId ==  KeyEvent.KEYCODE_ENTER)
			{
				if(editSearchBox.getText().toString().trim().length()==0)
				{
					listFilterRecord.clear();
					listFilterRecord.addAll(listMatchesRecord);
					adapter.notifyDataSetChanged();
				}
			}
			return false;
		}
	};
	
	
	
	private TextWatcher editTextWather = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			listFilterRecord.clear();
			if(editSearchBox.getText().toString().trim().length()>0){
			for (HashMap<String, String> favorites : listMatchesRecord) {
				if(favorites.get("fname").toLowerCase().contains(editSearchBox.getText().toString().toLowerCase()))
				{
					listFilterRecord.add(favorites);
				}
			 }
			}else
			{
				
				listFilterRecord.addAll(listMatchesRecord);
			}
			adapter.notifyDataSetChanged();
			
		
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			
		}
	};
	BroadcastReceiver updateFavorite = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if(!isServiceRuning)
					new GetMyMatches().execute("");
				}
			});
		}
	};
	
	
	@Override
	protected void onResume() {
			super.onResume();
			if(Utility.getBooleanPrefernce(mContext, "favupdate"))
			{
				Utility.setBooleanprefence(mContext, "favupdate", false);
				if(!isServiceRuning)
				new GetMyMatches().execute("");
			}

	}
	
	@Override
		protected void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
	
		}
	// Asyn Task for get my matches
	private class GetMyMatches extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			listMatchesRecord.clear();
			listFilterRecord.clear();
			//mProgressBar.setVisibility(View.VISIBLE);
			editSearchBox.setEnabled(false);
			isServiceRuning = true;
		}
		@Override
		protected String doInBackground(String... params) {
			String response = null;
			String serverUrl = Utility.SERVERURL + "GetFavorites";
			
			if (isAdvisor) {
				serverUrl = serverUrl + "&userid=0" + "&advisorid="
						+ Utility.getUserPrefernce(mContext, "id");
			} else {
				serverUrl = serverUrl + "&advisorid=0" + "&userid="
						+ Utility.getUserPrefernce(mContext, "id");
			}
			Log.d("Server url", ""+serverUrl);
			response = Utility.getJSONFromUrl(serverUrl);
			if(response!=null){
			try
			{
			 JSONObject jObj = new JSONObject(response);
			 JSONArray aArray = jObj.getJSONArray("favorites");
			 for (int i = 0; i < aArray.length(); i++) {
				JSONObject json = aArray.getJSONObject(i);
				json = json.getJSONObject("favorite");
				HashMap<String, String> map = Utility.toMap(json);
				String userid = (map.containsKey("userid"))?map.get("userid"):map.get("advisorid");
				ArrayList<HashMap<String, String>> mapchats = dbadapter.getChatsOfuser(userid);
				map.put("count", ""+mapchats.size());
				listMatchesRecord.add(map);
			}
			 listFilterRecord.addAll(listMatchesRecord);
			}catch(JSONException e)
			{
				e.printStackTrace();
				return null;
			}
			if(listMatchesRecord.size()>0)
				response = "Success";
			else
				response = "No Record";
			}
			
			return response;
	}
		
	
		protected void onPostExecute(String result) {
			
			isServiceRuning = false;
			if(listFilterRecord.size()>0)
			{
				String searchhint = "Search in 0 Advisors";
				if(isAdvisor)
				{
					searchhint = "Search in "+listFilterRecord.size()+" Leads";
				}else
				{
					searchhint = "Search in "+listFilterRecord.size()+" Advisors";
				}
				editSearchBox.setHint(searchhint);
				editSearchBox.setEnabled(true);
				
			}
			if (result!=null) {
				if(result.contains("Success"))
				{
					
					adapter.notifyDataSetChanged();
				 new Thread(downloadImage).start();	
				}else
				{
					
				}
			}else
			{
				Utility.showAlert(getApplicationContext(), "Alert", "Please check your internet connection.");
			}
		}
	}
	
	private Runnable downloadImage = new Runnable() {
		
		@Override
		public void run() {
			String userType = Utility.getUserPrefernce(mContext, "usertype");
			for (int i = 0; i < listMatchesRecord.size(); i++) {
				HashMap<String, String> map = listMatchesRecord.get(i);
				String url =null;
				if(map.get("photopath").contains("http")||map.get("photopath").contains("https"))
				{
					url = map.get("photopath");
				}else
				{
					url = Utility.IMAGEURL;
					url = url + map.get("photopath");
				}
				if(url!=null)
				{
					Bitmap bm = Utility.getBitmap(url);
					if(userType.equalsIgnoreCase("user"))
						mapBitmap.put(map.get("advisorid"), bm);
					else
						mapBitmap.put(map.get("userid"), bm);
				}
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						adapter.notifyDataSetChanged();
					}
				});
			}
		}
	};
	@Override
	public void onOpened(int position, boolean toRight) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClosed(int position, boolean fromRight) {
		
	}

	@Override
	public void onListChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMove(int position, float x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartOpen(int position, int action, boolean right) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartClose(int position, boolean right) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClickFrontView(int position) {
		// TODO Auto-generated method stub
		if(!listMatchesRecord.isEmpty()){
		HashMap<String, String> map = listMatchesRecord.get(position);
		if(Integer.parseInt(map.get("favoriteid"))>0){
		selectedIndex = position;
		Intent in = new Intent(mContext, ChatActivity.class);
		if(isAdvisor){
			Bitmap bm = mapBitmap.get(map.get("userid"));
			in.putExtra("requestFor", Constants.USER);
			Utility.setBitmap(bm);
		}
		else{
			Bitmap bm = mapBitmap.get(map.get("advisorid"));
			Utility.setBitmap(bm);
			in.putExtra("requestFor", Constants.ADVISOR);
		}
		in.putExtra("srcview", Constants.FAVOURITE);
		in.putExtra("detail", map);
		startActivity(in);
		}else
		{
			
			listViewRecord.openAnimate(position);
		}
		}else{
			Utility.showAlert(mContext, "Alert",
					getResources().getString(R.string.nonetwork));
		}
			
	}

	@Override
	public void onClickBackView(int position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDismiss(int[] reverseSortedPositions) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int onChangeSwipeMode(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onChoiceChanged(int position, boolean selected) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChoiceStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChoiceEnded() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFirstListItem() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLastListItem() {
		// TODO Auto-generated method stub
		
	}
}
