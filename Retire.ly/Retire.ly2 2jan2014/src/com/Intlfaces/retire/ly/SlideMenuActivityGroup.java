package com.Intlfaces.retire.ly;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.aretha.slidemenu.SlideMenu;
import com.aretha.slidemenu.SlideMenu.LayoutParams;
import com.linkites.retire.utility.Base64Coder;
import com.linkites.retire.utility.Utility;

@SuppressWarnings("deprecation")
public class SlideMenuActivityGroup extends ActivityGroup{
	public static SlideMenu mSlideMenu;
	LocalActivityManager activityManager;
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_menu);
		mContext = getApplicationContext();
	}

	public SlideMenu getSlideMenu() {
		return mSlideMenu;
	}
	@Override
	public void onContentChanged() {
		super.onContentChanged();
		mSlideMenu = (SlideMenu) findViewById(R.id.slideMenu);
	
		activityManager = getLocalActivityManager();
		View primary = activityManager.startActivity("PrimaryActivity",	new Intent(this, LeftUserAccountActivity.class)).getDecorView();
		mSlideMenu.addView(primary, new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_PRIMARY_MENU));

		View secondary = activityManager.startActivity("SecondaryActivity",	new Intent(this, RightMatchingActivity.class)).getDecorView();
		mSlideMenu.addView(secondary, new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_SECONDARY_MENU));

		View content = activityManager.startActivity("ContentActivity",
				new Intent(this, CenterHomeActivity.class)).getDecorView();
		mSlideMenu.addView(content, new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_CONTENT));
		
		
		
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
				new IntentFilter(Utility.SLIDEMENURBROADCAST));
		
		LocalBroadcastManager.getInstance(this).registerReceiver(slideReceiver,
				new IntentFilter("slide"));
		
		LocalBroadcastManager.getInstance(this).registerReceiver(appLogout,
				new IntentFilter("applogout"));
		LocalBroadcastManager.getInstance(this).registerReceiver(contactus,
				new IntentFilter("contactus"));
	}
	
	private BroadcastReceiver appLogout = new BroadcastReceiver() {		
		@Override
		public void onReceive(Context context, Intent intent) {
			logout();
		}
	};
	
	private BroadcastReceiver contactus = new BroadcastReceiver() {		
		
		@Override
		public void onReceive(Context context, Intent intent) {
			View content;
			mSlideMenu.removeViewAt(2);
			content = activityManager.startActivity("ContactUsActivity",
					new Intent(mContext, ContactUsActivity.class))
					.getDecorView();
			mSlideMenu.addView(content, new LayoutParams(
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					android.view.ViewGroup.LayoutParams.MATCH_PARENT,
					LayoutParams.ROLE_CONTENT));
			mSlideMenu.close(true);
		}
	};
	
	private void logout()
	{
		Utility.setUserPrefernce(mContext, "userimg", null);
		setResult(Activity.RESULT_OK);
		
		finish();
	}

	private BroadcastReceiver slideReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String slide = intent.getStringExtra("slide");
			if(slide.equalsIgnoreCase("left"))
			{
				mSlideMenu.open(false, true);
			}else
			{
				mSlideMenu.open(true, true);
			}
		}
	};
	
	public void onBackPressed(){
		super.onBackPressed();
		Activity activity=activityManager.getActivity("ContentActivity");
		activity.finish();
	};
	
	protected void onResume() {
		super.onResume();
		Activity activity = activityManager.getActivity("MatchedActivity");
		if(activity!=null)
			((CenterHomeActivity)activity).onResume();
	};

	public static void SwitchToRight() {
		//mSlideMenu.open(false, true);
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			int request = intent.getIntExtra("request", 0);
			int requestfor = 0;
			String userType = Utility.getUserPrefernce(mContext, "usertype");
			if (userType.equalsIgnoreCase("user"))
				requestfor = Constants.USER;
			else
				requestfor = Constants.ADVISOR;
			if(request<0)
				request = Constants.HOME;
			
			View content;
			switch (request) {
			case Constants.HOME: {
				mSlideMenu.removeViewAt(2);
				content = activityManager.startActivity("HomeActivity",
						new Intent(mContext, CenterHomeActivity.class))
						.getDecorView();
				mSlideMenu.addView(content, new LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						LayoutParams.ROLE_CONTENT));
				
				mSlideMenu.close(true);
			}
				break;
			
			case Constants.MYMATCHED:
			{
				mSlideMenu.removeViewAt(2);
				content = activityManager.startActivity(
						"MatchedActivity",
						new Intent(mContext, CenterHomeActivity.class)
								.putExtra("requestFor", Constants.MYMATCHED))
						.getDecorView();
				mSlideMenu.addView(content, new LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						LayoutParams.ROLE_CONTENT));
				mSlideMenu.close(true);
				Timer time = new Timer();
				time.schedule(new TimerTask() {
					
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								mSlideMenu.open(true, true);
							}
						});
						
					}
				}, 100);
			}
				break;
			case Constants.MATCHPREFRENCE: {
				mSlideMenu.removeViewAt(2);
				content = activityManager.startActivity("PrefrenceActivity",
						new Intent(mContext, MatchPrefrenceActivity.class))
						.getDecorView();
				mSlideMenu.addView(content, new LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						LayoutParams.ROLE_CONTENT));
				mSlideMenu.close(true);
			}
				break;
			case Constants.USERPROFILE: {
		
				mSlideMenu.removeViewAt(2);
			
				content = activityManager.startActivity("ProfileActivityView",
						new Intent(mContext, ProfileActivityView.class).putExtra("requestFor",requestfor))
						.getDecorView();
				mSlideMenu.addView(content, new LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						LayoutParams.ROLE_CONTENT));
				mSlideMenu.close(true);
			}
				break;
			case Constants.RIGHT: {
				SwitchToRight();
			}
				break;

			default:
				break;
			}

		}
	};
	
	
	

}
