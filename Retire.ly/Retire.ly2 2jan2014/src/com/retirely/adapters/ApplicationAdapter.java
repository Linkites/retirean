package com.retirely.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.Intlfaces.retire.ly.R;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.linkites.retire.util.RoundedImageView;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;



import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ApplicationAdapter extends ArrayAdapter<HashMap<String, String>> {
	ArrayList<HashMap<String, String>> applist;
	Context mContext;
	int count = -1;
	LayoutInflater inflater;
	int position;
	boolean isArrowShow;
	private HashMap<String, Bitmap> mapImages;
	public ApplicationAdapter(Context context, int resource,
			int textViewResourceId, ArrayList<HashMap<String, String>> objects, HashMap<String, Bitmap> images) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		this.mapImages = images;
	}

	@Override
	public int getCount() {
		return applist.size();
	}
	
	public void setPosition(int pos)
	{
		position = pos+1;
	}
	public void displayArrow(boolean isdisplay)
	{
		isArrowShow = isdisplay;
	}

	public View getView(final int aPosition, View viewHolder, ViewGroup parent) {
		ViewHolder holder;
	
		HashMap<String, String> map = getItem(aPosition);
		map.put("index", ""+aPosition);
		String loogedInuserType = Utility.getUserPrefernce(mContext, "usertype");
		count = getCount()-1;
	
		if (viewHolder == null) {
			//R.layout.cell_rating_user,parent, null);
			LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			viewHolder = li.inflate(R.layout.cell_rating_user, parent, false);
			holder = new ViewHolder();
			
			holder.imageRating  = (ImageView)viewHolder.findViewById(R.id.imageViewRating);
			holder.imageUserPic = (RoundedImageView)viewHolder.findViewById(R.id.imageViewPic);
			holder.imageUserPic.setScaleType(ScaleType.CENTER_CROP);
			holder.imageUserPic.setCornerRadius(50);
			holder.imageUserPic.setBorderWidth(2);
			holder.imageUserPic.setBorderColor(Color.DKGRAY);
			holder.imageUserPic.setRoundBackground(true);
			holder.txtUserName = (TextViewCustom)viewHolder.findViewById(R.id.textViewName);
			holder.txtOnline = (TextViewCustom)viewHolder.findViewById(R.id.textViewOnline);
			holder.btnBlock = (Button)viewHolder.findViewById(R.id.btnBlock);
			holder.btnLike = (Button)viewHolder.findViewById(R.id.btnLike);
			holder.btnLike.setTag(map);
			holder.btnBlock.setTag(map);
			viewHolder.setTag(holder);
			} 
		else {
			holder = (ViewHolder) viewHolder.getTag();
			holder.btnLike.setTag(holder.btnLike.getTag());
			holder.btnBlock.setTag(holder.btnBlock.getTag());
		}
		
		 ((SwipeListView)parent).recycle(viewHolder, aPosition);

		 Log.e("favourate id status at adapter","Here null:-"+map.get("favoriteid"));
		 
		if(map.get("favoriteid")!=null && Integer.parseInt(map.get("favoriteid").trim())==0)
		{
			holder.btnLike.setVisibility(View.VISIBLE);
		}else
		{
			holder.btnLike.setVisibility(View.GONE);
		}
		holder.btnLike.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//likefavorite
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>)v.getTag();
				Intent in = new Intent("likefavorite");
				in.putExtra("detail", map);
				
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			}
		});
		
		holder.btnBlock.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>)v.getTag();
				Intent in = new Intent("block");
				in.putExtra("detail", map);
				in.putExtra("isblock", true);
				LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
			}
		});
		// getItem(aPosition);
		String lname =(map.get("lname")!=null)?map.get("lname"):"";
		holder.txtUserName.setText(map.get("fname")+" "+lname);
		//holder.txtOnline.setText((Integer.parseInt(map.get("online"))==0)?"Offline":"Online");
		if(Integer.parseInt(map.get("count"))>0)
		{
			holder.txtOnline.setVisibility(View.VISIBLE);
			holder.txtOnline.setText(map.get("count"));
		}else
		{
			holder.txtOnline.setVisibility(View.INVISIBLE);
			holder.txtOnline.setText("0");
		}
			
		if(loogedInuserType.equals("user"))
		{
			if (!map.get("overallrating").equalsIgnoreCase(
					"0")) {
				String name = "review_"
						+ map.get("overallrating")
						+ "_fill";
				int resId = mContext.getResources().getIdentifier(
						name, "drawable", mContext.getPackageName());
				holder.imageRating.setImageResource(resId);
			}
		}else
		{
			holder.imageRating.setVisibility(View.INVISIBLE);
		}
		if(mapImages!=null)
		{
			if(mapImages.containsKey(map.get("userid")))
			{
				if(mapImages.get(map.get("userid"))!=null)
				{
					holder.imageUserPic.setImageBitmap(mapImages.get(map.get("userid")));
				}else
				{
					holder.imageUserPic.setImageResource(R.drawable.profile_default);
				}
			}
			if(mapImages.containsKey(map.get("advisorid")))
			{
				if(mapImages.get(map.get("advisorid"))!=null)
				{
					holder.imageUserPic.setImageBitmap(mapImages.get(map.get("advisorid")));
				}else
				{
					holder.imageUserPic.setImageResource(R.drawable.profile_default);
				}
				
				
			}
		}
		
		
		return viewHolder;

	}

	class ViewHolder {
		TextViewCustom txtUserName, txtLocation, txtOnline;
		ImageView imageRating;
		RoundedImageView imageUserPic;
		Button btnLike, btnBlock;
	}
}
