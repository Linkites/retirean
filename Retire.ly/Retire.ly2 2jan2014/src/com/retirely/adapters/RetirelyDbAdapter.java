package com.retirely.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RetirelyDbAdapter extends SQLiteOpenHelper {

	private Context mContext;
	private static final String DBNAME = "Retirely";
	private static final String TABEL = "chats";
	private static final String ID = "_id";
	private static final String USERID = "userid";
	private static final String MESSAGE = "message";
	public RetirelyDbAdapter(Context context) {
		super(context, DBNAME, null, 1);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_TABLE = ("CREATE TABLE "+TABEL+"("+ID+" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,"+" "+USERID+" VARCHAR,"+" "+MESSAGE+" VARCHAR "+")");
		db.execSQL(CREATE_TABLE);
	}

	// Insert record in database
	public long insertChatRecord(HashMap<String, String> map)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		Set keySet = map.keySet();
		Iterator<?> keys = keySet.iterator();
		//
		while (keys.hasNext()) {
			final String key = (String) keys.next();
			values.put(key, map.get(key));
		}
		int row = (int) db.insert(TABEL, null, values);
		db.close();
		return row;
	}
	
	// Get Record of specify user 
	public ArrayList<HashMap<String, String>> getChatsOfuser(String userid)
	{
		ArrayList<HashMap<String, String>> biblelist = new ArrayList<HashMap<String, String>>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(TABEL, new String[] { ID, USERID, MESSAGE }, "userid=?", new String[] { userid },
				null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()){
				
				do {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(ID, String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(ID))));
					map.put(USERID, cursor.getString(cursor
							.getColumnIndex(USERID)));
					map.put(MESSAGE,
							cursor.getString(cursor.getColumnIndex(MESSAGE)));
					biblelist.add(map);
					map = null;

				} while (cursor.moveToNext());
			}
			
			cursor.close();
			db.close(); 
		}
	
		return biblelist;
	}
	
	
	public int deleteChatsOfuser(String userid)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		 int deleted = db.delete(TABEL, "userid=?", new String[] { userid });
		 db.close();
		return deleted;	
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
