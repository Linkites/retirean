package com.retirely.adapters;

import android.graphics.Bitmap;

public class ContactDetails {
	public String name;
	public String photoUri;
	public String id;
	public boolean isChecked;
	public Bitmap bmp;
}
