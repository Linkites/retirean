package com.retirely.adapters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ChatListAdapter extends ArrayAdapter<String> {
	ArrayList<String> applist;
	Context mContext;
	int count = 0;
	LayoutInflater inflater;
	

	public ChatListAdapter(Context context, int resource,
			int textViewResourceId, ArrayList<String> objects,
			HashMap<String, Bitmap> hashBitmap, String userId) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return applist.size();
	}

	public String getItem(int aPosition) {

		return applist.get(aPosition);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(final int aPosition, View convertView, ViewGroup parent) {
		ViewHolder holder;
		String object = getItem(aPosition);
		String [] seprated = object.split("Δ");
		String userid = Utility.getUserPrefernce(mContext, "id");
		if (convertView == null) {
			holder = new ViewHolder();
			
			convertView = inflater.inflate(R.layout.cell_chats_user, null);
			holder.layoutLeftChat = (RelativeLayout)convertView.findViewById(R.id.layoutLeftbubble);
			holder.layoutRightChat = (RelativeLayout)convertView.findViewById(R.id.layoutRighttbubble);
			holder.txtViewChatRight = (TextViewCustom) convertView
					.findViewById(R.id.textViewNameRight);
			//holder.txtViewSenderinfo = (TextViewCustom) convertView
					//.findViewById(R.id.textViewtimeRight);
			holder.layoutBgChatRight = (RelativeLayout) convertView
					.findViewById(R.id.chatbglayoutRight);
			holder.imageChaticonRight = (ImageView) convertView
					.findViewById(R.id.imageViewPicRight);
			holder.txtViewChat = (TextViewCustom) convertView
						.findViewById(R.id.textViewName);
		   holder.layoutBgChat = (RelativeLayout) convertView
						.findViewById(R.id.chatbglayout);
		   holder.imageChaticon = (ImageView) convertView
						.findViewById(R.id.imageViewPic);
			
		  
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		 if(seprated[0].equalsIgnoreCase(userid))
			{
				holder.layoutLeftChat.setVisibility(View.GONE);
				holder.layoutRightChat.setVisibility(View.VISIBLE);
			}else
			{
				holder.layoutLeftChat.setVisibility(View.VISIBLE);
				holder.layoutRightChat.setVisibility(View.GONE);
			}
		 
		if (seprated[6].equalsIgnoreCase("user")) {
			holder.imageChaticonRight.setImageResource(R.drawable.chat_ic);
			//holder.layoutBgChat.setBackgroundResource(R.drawable.bubble_yellow);

			if (seprated[3] != null)
				holder.txtViewChatRight.setText(seprated[3]);
			else
				holder.txtViewChatRight.setText("Annonymous");

//			if (seprated[4] != null)
//				holder.txtViewSenderinfo.setText(seprated[1] + ", "
//						+ seprated[4]);
//			else
//				holder.txtViewSenderinfo.setText("Sending");
		}
		else {
			
			holder.imageChaticon.setImageResource(R.drawable.chat_gray_ic);
			//holder.layoutBgChat.setBackgroundResource(R.drawable.bubble_green);
			
			if (seprated[3] != null)
				holder.txtViewChat.setText(seprated[3]);
			else
				holder.txtViewChat.setText("Annonymous");

//			if (seprated[4] != null)
//				holder.txtViewSenderinfo.setText(seprated[1] + ", "
//						+ seprated[4]);
//			else
//				holder.txtViewSenderinfo.setText("Sending");
		}

		return convertView;

	}

	class ViewHolder {

		TextViewCustom txtViewChat, txtViewChatRight;
		RelativeLayout layoutBgChat,layoutBgChatRight, layoutRightChat, layoutLeftChat;
		ImageView imageChaticon, imageChaticonRight;

	}
}
