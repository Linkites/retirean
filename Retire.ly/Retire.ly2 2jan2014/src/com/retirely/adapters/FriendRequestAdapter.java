package com.retirely.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.retirely.adapters.ContactDetails;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.Intlfaces.retire.ly.R;
import com.retirely.adapters.ApplicationAdapter.ViewHolder;
public class FriendRequestAdapter extends ArrayAdapter {
	ArrayList<ContactDetails> applist;
	Context mContext;
	int count = -1;
	LayoutInflater inflater;
	int position;
	boolean isArrowShow;
	int requestFor;
	ImageView thumb;
	String phptouri;
	HashMap<String, Bitmap> picMap;
	
	public FriendRequestAdapter(Context context, int resource, ArrayList<ContactDetails> objects, int requestFor, HashMap<String, Bitmap> picMap) {
		super(context, resource, objects);
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		this.requestFor = requestFor;
		this.picMap = picMap; 
	}
	
	public FriendRequestAdapter(Context context, int resource, ArrayList<ContactDetails> objects, int requestFor) {
		super(context, resource, objects);
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		this.requestFor = requestFor;
	}
	
	public View getView(final int aPosition, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View viewHolder = convertView;	
		if (viewHolder == null) {			
			viewHolder = inflater.inflate(R.layout.friend_request_design, null);			
		}
		
		if(requestFor==1){
			//setImageURI(Uri.parse(photoUrl));
			ContactDetails contactDetail= applist.get(aPosition);
			if(aPosition==applist.size()-1){
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}
			else{
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}

			thumb = (ImageView)viewHolder.findViewById(R.id.thumb);
			if(contactDetail.photoUri!=null && !contactDetail.photoUri.equals(""))
				thumb.setImageURI(Uri.parse(contactDetail.photoUri));
			else
				thumb.setImageResource(R.drawable.profile_default);
			TextView name = (TextView)viewHolder.findViewById(R.id.name);
			name.setText(contactDetail.name);
			
			CheckBox checkbox = (CheckBox)viewHolder.findViewById(R.id.checkbox);
			checkbox.setChecked(contactDetail.isChecked);
			
			checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {					
					Intent intent = new Intent("doThisWork");
					intent.putExtra("RequestFor", "contacts");
					intent.putExtra("position", aPosition+"");
					intent.putExtra("isChecked", isChecked+"");
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
				}
			});
		}
		
		else if(requestFor==2){
			//setImageURI(Uri.parse(photoUrl));
			ContactDetails contactDetail= applist.get(aPosition);
			if(aPosition==applist.size()-1){
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}
			else{
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}
			
			thumb = (ImageView)viewHolder.findViewById(R.id.thumb);
			if(picMap.get(contactDetail.id)!=null){
					thumb.setImageBitmap(picMap.get(contactDetail.id));
			}
			else
				thumb.setImageResource(R.drawable.profile_default);
			
			TextView name = (TextView)viewHolder.findViewById(R.id.name);
			name.setText(contactDetail.name);

			CheckBox checkbox = (CheckBox)viewHolder.findViewById(R.id.checkbox);
			checkbox.setChecked(contactDetail.isChecked);
			checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Intent intent = new Intent("doThisWork");
					intent.putExtra("RequestFor", "facebook");
					intent.putExtra("position", aPosition+"");
					intent.putExtra("isChecked", isChecked+"");
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);				
				}
			});
		}
		
		else if(requestFor==3){
			//setImageURI(Uri.parse(photoUrl));
			ContactDetails contactDetail= applist.get(aPosition);
			if(aPosition==applist.size()-1){
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}
			else{
				viewHolder.findViewById(R.id.list_divider).setVisibility(View.GONE);
			}
			
			thumb = (ImageView)viewHolder.findViewById(R.id.thumb);
			if(picMap.get(contactDetail.id)!=null){
				thumb.setImageBitmap(picMap.get(contactDetail.id));
			}
			else
				thumb.setImageResource(R.drawable.profile_default);
			
			TextView name = (TextView)viewHolder.findViewById(R.id.name);
			name.setText(contactDetail.name);
			
			
			CheckBox checkbox = (CheckBox)viewHolder.findViewById(R.id.checkbox);
			checkbox.setChecked(contactDetail.isChecked);
			checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {					
					Intent intent = new Intent("doThisWork");
					intent.putExtra("RequestFor", "linkedin");
					intent.putExtra("position", aPosition+"");
					intent.putExtra("isChecked", isChecked+"");
					LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);					
				}
			});
		}
		return viewHolder;
	}
}
