package com.retirely.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.Intlfaces.retire.ly.R;
import com.linkites.retire.utility.TextViewCustom;
import com.linkites.retire.utility.Utility;



import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class ApplicationAdapterForRating extends ArrayAdapter<HashMap<String, String>> {
	ArrayList<HashMap<String, String>> applist;
	Context mContext;
	int count = -1;
	LayoutInflater inflater;
	int position;
	boolean isArrowShow;
	private HashMap<String, Bitmap> mapImages;
	public ApplicationAdapterForRating(Context context, int resource,
			int textViewResourceId, ArrayList<HashMap<String, String>> objects, HashMap<String, Bitmap> images) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		this.mapImages = images;
	}

	@Override
	public int getCount() {
		return applist.size();
	}
	
	public void setPosition(int pos)
	{
		position = pos+1;
	}
	public void displayArrow(boolean isdisplay)
	{
		isArrowShow = isdisplay;
	}

	@SuppressLint("SimpleDateFormat")
	public View getView(final int aPosition, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View viewHolder = convertView;
		HashMap<String, String> map = getItem(aPosition);
		
		String userType = Utility.getUserPrefernce(mContext, "usertype");
		count = getCount()-1;
		
		if (viewHolder == null) {
			holder = new ViewHolder();
			viewHolder = inflater.inflate(R.layout.cell_rating_user, null);
			holder.imageUserPic = (ImageView)viewHolder.findViewById(R.id.imageViewPic);
			holder.txtUserName = (TextViewCustom)viewHolder.findViewById(R.id.textViewName);
			holder.txtRatingDate = (TextViewCustom)viewHolder.findViewById(R.id.textViewLocation);
			holder.txtReiew = (TextViewCustom)viewHolder.findViewById(R.id.textViewOnline);
			holder.imageRating = (ImageView)viewHolder.findViewById(R.id.imageViewRating);
			if(userType.equalsIgnoreCase("user"))
			{
				holder.imageRating.setVisibility(View.VISIBLE);
			}else
			{
				holder.imageRating.setVisibility(View.GONE);
			}
			viewHolder.setTag(holder);
			} else {
			holder = (ViewHolder) viewHolder.getTag();
		}
		// getItem(aPosition);
		String lname =(map.get("lname")!=null)?map.get("lname"):"";
		holder.txtUserName.setText(map.get("fname")+" "+lname);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
		
		 Date date = new Date();
		 try {
			 date =  sdf.parse(map.get("date"));
			 
		} catch (Exception e) {
			// TODO: handle exception
		}
		//holder.txtRatingDate.setText(sdf1.format(date));
		holder.txtReiew.setText((map.get("comment")));
		
		if(holder.imageRating.getVisibility()==View.VISIBLE)
		{
			if(Integer.parseInt(map.get("rating"))>0)
			{
				int rate = Integer.parseInt(map.get("rating"));
				String rateImage = "review_"+rate+"_fill";
				int resid = mContext.getResources().getIdentifier(rateImage, "drawable",mContext.getPackageName());
				holder.imageRating.setImageResource(resid);
			}else
			{
				holder.imageRating.setImageResource(R.drawable.review_blank_fill);
			}
		}
		if(mapImages!=null)
		{
			if(mapImages.containsKey(map.get("userid")))
			{
				if(mapImages.get(map.get("userid"))!=null)
				{
					holder.imageUserPic.setImageBitmap(mapImages.get(map.get("userid")));
				}else
				{
					holder.imageUserPic.setImageResource(R.drawable.profile_default);
				}
			}
			if(mapImages.containsKey(map.get("advisorid")))
			{
				if(mapImages.get(map.get("advisorid"))!=null)
				{
					holder.imageUserPic.setImageBitmap(mapImages.get(map.get("advisorid")));
				}else
				{
					holder.imageUserPic.setImageResource(R.drawable.profile_default);
				}
				
				
			}
		}
		
		
		return viewHolder;

	}

	class ViewHolder {
		TextViewCustom txtUserName, txtRatingDate, txtReiew;
		ImageView imageUserPic,  imageRating;
	}
}
